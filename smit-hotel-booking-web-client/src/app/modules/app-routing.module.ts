import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../components/login/login.component';
import { ReservationComponent } from 'app/components/reservation/reservation.component';
import { NewReservationComponent } from '../components/reservation/new-reservation/new-reservation.component';
import { AdminHomeComponent } from '../components/admin/admin-home/admin-home.component';
import { AuthGuard } from '../services/auth-guard.service';
import { NoAuthGuard } from '../services/no-auth-guard.service';
import { EditReservationComponent } from 'app/components/reservation/edit-reservation/edit-reservation.component';

const routes: Routes = [
  { path: '', redirectTo: '/broneeringud', pathMatch: 'full' },
  { path: 'login', component: LoginComponent, canActivate: [NoAuthGuard] },
  { path: 'broneeringud', component: ReservationComponent },
  { path: 'uus-broneering/:data', component: NewReservationComponent },
  { path: 'admin', component: AdminHomeComponent, canActivate: [AuthGuard] },
  { path: 'admin/broneering/:number', component: EditReservationComponent, canActivate: [AuthGuard] },
  { path: '**', redirectTo: '/broneeringud' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
