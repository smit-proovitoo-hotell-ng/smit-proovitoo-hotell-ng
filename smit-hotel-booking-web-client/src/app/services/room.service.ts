import { Injectable } from '@angular/core';
import { LocationDto } from 'app/models/location-dto';
import { Observable, of } from 'rxjs';
import { ReservationDto } from '../models/reservation-dto';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class RoomService {
  constructor(private http: HttpClient, private api: ApiService) {}

  getAvailableRooms(from: Date, to: Date): Observable<LocationDto[]> {
    return this.http.get<LocationDto[]>(
      this.api.apiUrl +
        '/Room/Available?from=' +
        from.toISOString() +
        '&to=' +
        to.toISOString()
    );
  }

  getAvailableRoomsAdmin(from: Date, to: Date, bookingCode: string): Observable<LocationDto[]> {
    return this.http.get<LocationDto[]>(
      this.api.apiUrl +
        '/admin/HotelManagement/Room/Available?from=' +
        from.toISOString() +
        '&to=' +
        to.toISOString() +
        '&bookingCode=' +
        encodeURIComponent(bookingCode), {
          withCredentials: true
        }
    );
  }

  getAllRooms(from?: Date | null, to?: Date | null): Observable<LocationDto[]> {
    let params = new HttpParams();
    if (!!from) {
      params = params.set('from', from?.toISOString())
    }
    if (!!to) {
      params = params.set('to', to?.toISOString())
    }
    return this.http.get<LocationDto[]>(this.api.apiUrl + '/admin/HotelManagement/Room/All', { params: params, withCredentials: true });
  }
}
