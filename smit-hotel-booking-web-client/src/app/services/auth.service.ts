import { HttpClient } from '@angular/common/http';
import { ApiService } from './api.service';
import { Observable, catchError, map, of } from 'rxjs';
import { LoginDto } from '../models/login-dto';
import { Injectable } from '@angular/core';
import { UserModel } from '../models/user-model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient, private api: ApiService) {}

  isLoggedIn = false;
  user?: UserModel;

  logIn(username: string, password: string) {
    const body: LoginDto = {
      username,
      password,
    };

    return this.http.post(this.api.apiUrl + '/Auth/LogIn', body, {
      withCredentials: true,
    });
  }

  logOut() {
    return this.http.post(this.api.apiUrl + '/Auth/LogOut', undefined, {
      withCredentials: true,
    });
  }

  isAuthenticated(): Observable<boolean> {
    return this.http.get<boolean>(this.api.apiUrl + '/Auth/IsAuthenticated', {
      withCredentials: true,
    });
  }

  getUserData(): Observable<UserModel> {
    return this.http.get<UserModel>(this.api.apiUrl + '/Auth/UserData', {
      withCredentials: true,
    });
  }
}
