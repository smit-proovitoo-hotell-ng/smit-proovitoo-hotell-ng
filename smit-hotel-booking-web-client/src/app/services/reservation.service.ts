import { Injectable } from "@angular/core";
import { LocationDto } from "app/models/location-dto";
import { Observable, of } from "rxjs";
import { ReservationDto } from "../models/reservation-dto";
import { HttpClient, HttpParams, HttpResponse } from "@angular/common/http";
import { ApiService } from "./api.service";
import { map } from 'rxjs/operators';
import { ReservationCreateDto } from "app/models/reservation-create-dto";
import { ReservationUpdateDto } from "app/models/reservation-update-dto";

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor(private http: HttpClient, private api: ApiService) {}

  searchReservations(dateFrom: Date | null, dateTo: Date | null): Observable<ReservationDto[]> {
    let params = new HttpParams();
    if (!!dateFrom) {
      params = params.set('from', dateFrom?.toISOString())
    }
    if (!!dateTo) {
      params = params.set('to', dateTo?.toISOString())
    }
    return this.http.get<ReservationDto[]>(this.api.apiUrl + '/admin/HotelManagement/Booking/All', {
      params: params,
      withCredentials: true,
    });
  };

  createBooking(reservationDto: ReservationCreateDto): Observable<ReservationDto> {
    return this.http.post<ReservationDto>(this.api.apiUrl + '/Booking/Create', reservationDto);
  }

  updateBooking(reservationDto: ReservationUpdateDto): Observable<ReservationDto> {
    return this.http.post<ReservationDto>(this.api.apiUrl + '/admin/HotelManagement/Booking/Update', reservationDto, {
      withCredentials: true,
    }).pipe(
      map(result => this.mapApiResultToReservationDto(result))
    );
  }

  getBookingByCode(code: string, fullname: string): Observable<ReservationDto> {
    return this.http.post<any>(this.api.apiUrl + '/Booking/Search', { BookingCode: code, FullName: fullname })
      .pipe(
        map(result => this.mapApiResultToReservationDto(result))
      );
  }

  cancelReservation(code: string, fullname: string): Observable<boolean> {
    return this.http.post<any>(this.api.apiUrl + '/Booking/Cancel', { BookingCode: code, FullName: fullname });  
  }

  cancelReservationAdmin(code: string): Observable<any> {
    return this.http.post<any>(this.api.apiUrl + '/admin/HotelManagement/Booking/Cancel', { BookingCode: code}, {
      withCredentials: true,
    });
  }

  getReservation(number: string): Observable<ReservationDto> {
    return this.http.get<ReservationDto>(`${this.api.apiUrl}/admin/HotelManagement/Booking/${encodeURIComponent(number)}`, {
      withCredentials: true,
    })
    .pipe(
      map(result => this.mapApiResultToReservationDto(result))
    );
  }

  private mapApiResultToReservationDto(result: any): ReservationDto {
    const reservationDto: ReservationDto = {
      bookingCode: result.bookingCode, 
      fullName: result.fullName,
      email: result.email,
      nationalIdentificationNumber: result.nationalIdentificationNumber,
      price: result.bookingRoom.price.toString(),
      room: {
        id: result.bookingRoom.id,
        price: result.bookingRoom.price.toString(),
        maxOccupants: result.bookingRoom.maxOccupants.toString(),
        type: result.bookingRoom.type,
        typeId: result.bookingRoom.typeId,
        roomNumber: result.bookingRoom.roomNumber,
      },
      roomId: result.bookingRoom.id,
      checkInStartUtc: new Date(result.checkInStartUtc),
      checkOutUtc: new Date(result.checkOutUtc),
    };
  
    return reservationDto;  
  }
}
