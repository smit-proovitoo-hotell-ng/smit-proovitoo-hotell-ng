import { Injectable, isDevMode } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private devUrl = 'https://localhost:7294/api';
  private prodUrl = '/api';

  constructor() {}

  get apiUrl(): string {
    if (isDevMode()) {
      return this.devUrl;
    }
    return this.prodUrl;
  }
}
