import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { AuthService } from './auth.service';
import { firstValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NoAuthGuard {
  constructor(private router: Router, private authService: AuthService) {}

  async canActivate(): Promise<boolean> {
    const isAuthenticated = await firstValueFrom(
      this.authService.isAuthenticated()
    );
    if (isAuthenticated) {
      this.router.navigate(['/admin']);
      return false;
    }

    return true;
  }
}
