import { FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';

export function pairRequiredValidation(controlName1: string, controlName2: string): ValidatorFn {
  return (control: FormGroup): ValidationErrors | null => {
    const control1 = control.get(controlName1);
    const control2 = control.get(controlName2);

    return (control1.value === null && control2.value === null) || (control1.value !== null && control2.value !== null) ? null : { 'pairRequired': true };
  };
};
