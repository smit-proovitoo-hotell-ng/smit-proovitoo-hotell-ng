import { AbstractControl, ValidationErrors } from "@angular/forms";
import { Observable, of } from "rxjs";

export function notEmptyOrWhitespaceValidation(control: AbstractControl): Observable<ValidationErrors | null> {
    const isOnlyWhitespace = (control.value || '').trim().length === 0;
    return of(isOnlyWhitespace ? { onlyWhitespace: true } : null);
}