import { AbstractControl, ValidationErrors } from '@angular/forms';
import { Observable, of } from 'rxjs';

/**
 * Validates a booking code to ensure it starts with "#" and only contains alphanumeric characters.
 * @param control The AbstractControl to validate.
 * @returns A ValidationErrors object if the booking code is invalid, otherwise null.
 */
export function bookingCodeValidation(
  control: AbstractControl
): Observable<ValidationErrors | null> {
  const value = control.value || '';
  const regex = /^#[a-zA-Z0-9]+$/;
  const isValid = regex.test(value);
  return of(
    isValid
      ? null
      : {
          invalidBookingCode: {
            message:
              'Broneeringu number peab algama "#" märgiga ning sisaldama ainult tähti ja numbreid.',
          },
        }
  );
}
