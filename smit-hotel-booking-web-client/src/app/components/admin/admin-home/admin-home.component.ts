import {
  Component,
  ChangeDetectionStrategy,
  AfterViewInit,
  ChangeDetectorRef,
} from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { ReservationService } from 'app/services/reservation.service';
import { BehaviorSubject, Subject } from 'rxjs';
import { ReservationDto } from '../../../models/reservation-dto';
import { LocationDto } from 'app/models/location-dto';
import { RoomService } from 'app/services/room.service';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminHomeComponent implements AfterViewInit {
  searchHotelForm = this.fb.group({
    dateFrom: new FormControl<Date>(null),
    dateTo: new FormControl<Date>(null),
  });
  searchRoomForm = this.fb.group({
    dateFrom: new FormControl<Date>(null),
    dateTo: new FormControl<Date>(null),
  });

  reservationDisplayedColumns: string[] = [
    'bookingCode',
    'fullName',
    'checkInStartUtc',
    'checkOutUtc',
    'room-type',
    'room-number',
    'cancel',
    'edit'
  ];
  roomDisplayedColumns: string[] = [
    'roomNumber',
    'type',
    'maxOccupants',
    'isAvailable',
    'expand'
  ];
  reservationDataSource$: Subject<MatTableDataSource<ReservationDto>> =
    new Subject<MatTableDataSource<ReservationDto>>();
  roomDataSource$: Subject<MatTableDataSource<LocationDto>> =
    new Subject<MatTableDataSource<LocationDto>>();

  constructor(
    private fb: FormBuilder,
    private reservationService: ReservationService,
    private roomService: RoomService,
    private changeDetector: ChangeDetectorRef
  ) { }

  ngAfterViewInit(): void {
    this.reservationService
      .searchReservations(null, null)
      .subscribe((result) => {
        this.reservationDataSource$.next(new MatTableDataSource(result));
        this.changeDetector.detectChanges();
      });
    this.searchRooms();
  }

  searchAvailability() {
    if (!this.searchHotelForm.valid) {
      this.searchHotelForm.markAllAsTouched();
      return;
    }
    let dateFrom = this.searchHotelForm.get('dateFrom')?.value ?? null;
    let dateTo = this.searchHotelForm.get('dateTo')?.value ?? null;

    this.reservationService
      .searchReservations(dateFrom, dateTo)
      .subscribe((result) => {
        this.reservationDataSource$.next(new MatTableDataSource(result));
      });
  }

  searchRooms() {
    if (!this.searchRoomForm.valid) {
      this.searchRoomForm.markAllAsTouched();
      return;
    }
    let dateFrom = this.searchRoomForm.get('dateFrom')?.value ?? null;
    let dateTo = this.searchRoomForm.get('dateTo')?.value ?? null;
    
    this.roomService.getAllRooms(dateFrom, dateTo).subscribe((result) => {
      this.roomDataSource$.next(new MatTableDataSource(result));
    });
  }

  getBookedRooms(dataSource: MatTableDataSource<LocationDto>): number {
    if (!dataSource || !dataSource.data) {
      return 0;
    }
    return dataSource.data.filter(location => location.bookings && location.bookings.length > 0).length;
  }

  clearHotelForm() {
    this.searchHotelForm.reset();
    this.searchHotelForm.markAllAsTouched();
    this.searchAvailability();
  }
  clearRoomForm() {
    this.searchRoomForm.reset();
    this.searchRoomForm.markAllAsTouched();
    this.searchRooms()
  }
}
