import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  Component,
  ContentChild,
  ContentChildren,
  Input,
  QueryList,
  ViewChild,
} from '@angular/core';
import {
  MatColumnDef,
  MatHeaderRowDef,
  MatNoDataRow,
  MatRowDef,
  MatTable,
  MatTableDataSource,
} from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { LocationDto } from 'app/models/location-dto';
import { ReservationDto } from '../../../models/reservation-dto';
import { ReservationService } from 'app/services/reservation.service';
import { DialogData, PopupDialogComponent } from 'app/components/common/popup/popup-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-all-reservations-table',
  templateUrl: './all-reservations-table.component.html',
  styleUrls: ['./all-reservations-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class AllReservationsTableComponent {
  @ContentChildren(MatHeaderRowDef) headerRowDefs!: QueryList<MatHeaderRowDef>;
  @ContentChildren(MatRowDef) rowDefs!: QueryList<MatRowDef<ReservationDto>>;
  @ContentChildren(MatColumnDef) columnDefs!: QueryList<MatColumnDef>;
  @ContentChild(MatNoDataRow) noDataRow!: MatNoDataRow;

  @ViewChild(MatTable, { static: true }) table!: MatTable<ReservationDto>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  @Input() columns!: string[];

  private _dataSource!: MatTableDataSource<ReservationDto>;
  @Input()
  set dataSource(dataSource: MatTableDataSource<ReservationDto>) {
    this._dataSource = dataSource;
    this.resetSortPagination();
  }
  get dataSource(): MatTableDataSource<ReservationDto> {
    return this._dataSource;
  }

  constructor(private router: Router, private reservationService: ReservationService, private toastr: ToastrService, public dialog: MatDialog) { }

  ngAfterViewInit(): void {
    this.resetSortPagination(2);
  }

  ngAfterContentInit() {
    this.columnDefs.forEach((columnDef) => this.table.addColumnDef(columnDef));
    this.rowDefs.forEach((rowDef) => this.table.addRowDef(rowDef));
    this.headerRowDefs.forEach((headerRowDef) =>
      this.table.addHeaderRowDef(headerRowDef)
    );
    this.table.setNoDataRow(this.noDataRow);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  cancelReservation(reservation: ReservationDto): void {
    const data: DialogData = {
      title: `Tühista broneering ${reservation.bookingCode}`,
      message: `Kas olete kindel, et soovite tühistada broneeringu?`,
      isCancelDisabled: false,
      okAction: () => {
        this.reservationService.cancelReservationAdmin(reservation.bookingCode).subscribe(result => {
          if (!!result) {
            this.toastr.success("Broneering edukalt tühistatud.");
          }
        });
      },
    };

    this.dialog.open(PopupDialogComponent, { data: data });
  }

  editReservation(reservation: ReservationDto) {
    this.router.navigate([`/admin/broneering/${reservation.bookingCode}`])
  }

  resetSortPagination(forceSortColIndex?: number) {
    if (this.paginator != null) {
      this.dataSource.paginator = this.paginator;
    }
    if (this.sort != null) {
      if (forceSortColIndex != null) {
        this.sort.active = this.columns[forceSortColIndex];
        this.sort.direction = 'asc';
      }

      this.sort.sortChange.emit();
      this.dataSource.sort = this.sort;
    }
  }
}
