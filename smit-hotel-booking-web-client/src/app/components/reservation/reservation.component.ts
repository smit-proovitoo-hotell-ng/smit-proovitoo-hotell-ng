import { Component, ChangeDetectionStrategy, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { LocationsTableComponent } from '../../components/reservation/locations-table/locations-table.component';
import { LocationDto } from 'app/models/location-dto';
import { notEmptyOrWhitespaceValidation } from 'app/validation/not-empty-or-whitespace-validation';
import { BehaviorSubject, Subject } from 'rxjs';
import { RoomService } from '../../services/room.service';
import { MatTabGroup } from '@angular/material/tabs';
import { ActivatedRoute, Router } from '@angular/router';
import { ReservationDto } from 'app/models/reservation-dto';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReservationComponent {
  @ViewChild(LocationsTableComponent) locationsTable!: LocationsTableComponent;
  @ViewChild('optionsTabs') optionsTabs!: MatTabGroup;
  searchHotelForm = this.fb.group({
    dateFrom: new FormControl<Date>(new Date(new Date().setUTCHours(0,0,0,0)), Validators.required),
    dateTo: new FormControl<Date>(
      new Date(new Date (new Date().setUTCHours(0,0,0,0)).getTime() + 86400000),
      Validators.required
    ),
  });

  searchReservationForm = this.fb.group({
    number: ['', Validators.required, notEmptyOrWhitespaceValidation],
    fullname: ['', Validators.required, notEmptyOrWhitespaceValidation],
  });
  searchedReservation?: ReservationDto;

  locationDisplayedColumns: string[] = [
    'price',
    'maxOccupants',
    'type',
    'reserve',
  ];
  locationDataSource$?: Subject<MatTableDataSource<LocationDto>> =
    new BehaviorSubject<MatTableDataSource<LocationDto>>(null);

  constructor(
    private fb: FormBuilder,
    private roomService: RoomService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  selectedTab : number | 0;

  ngOnInit(): void {
    this.route.fragment.subscribe((fragment: string) => {
      if (fragment) {
        this.selectedTab = +fragment;
      }
    });
  }

  ngAfterViewInit() {
    if (this.route.snapshot.fragment === 'minu-broneeringud') {
      this.optionsTabs.selectedIndex = 1;

      if (this.route.snapshot.queryParams['query'] != null) {
      }
    }

    if (
      this.route.snapshot.queryParams['from'] != null ||
      this.route.snapshot.queryParams['to'] != null
    ) {
      this.searchHotelForm.setValue({
        dateFrom: new Date(this.route.snapshot.queryParams['from']),
        dateTo: new Date(this.route.snapshot.queryParams['to']),
      });

      this.searchAvailableRooms();
    }
  }

  searchAvailableRooms() {
    if (!this.searchHotelForm.valid) {
      this.searchHotelForm.markAllAsTouched();
      return;
    }
    let dateFrom = this.searchHotelForm.get('dateFrom')?.value ?? null;
    let dateTo = this.searchHotelForm.get('dateTo')?.value ?? null;

    this.roomService.getAvailableRooms(dateFrom, dateTo).subscribe((result) => {
      this.locationDataSource$.next(new MatTableDataSource(result));
    });

    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: { from: dateFrom.toISOString(), to: dateTo.toISOString() },
    });
  }

  clearFields() {
    this.searchHotelForm.reset();
    this.searchHotelForm.markAllAsTouched();
    if (this.locationsTable) {
      this.locationsTable.clearFilter();
    }
    this.locationDataSource$?.next(null);
  }
}
