import { AfterViewInit, ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LocationDto } from 'app/models/location-dto';
import { ReservationDto } from 'app/models/reservation-dto';
import { ReservationUpdateDto } from 'app/models/reservation-update-dto';
import { ReservationService } from 'app/services/reservation.service';
import { RoomService } from 'app/services/room.service';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Subject, merge, skip } from 'rxjs';
import { DialogData, PopupDialogComponent } from 'app/components/common/popup/popup-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-edit-reservation',
  templateUrl: './edit-reservation.component.html',
  styleUrls: ['./edit-reservation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditReservationComponent implements OnInit, AfterViewInit {
  room = new FormControl(null, Validators.required);
  editReservationForm = this.fb.group({
    name: ['', [Validators.required]],
    personalCode: ['', [Validators.required]],
    email: ['', [Validators.required, Validators.email]],
    dateFrom: new FormControl<Date>(null, Validators.required),
    dateTo: new FormControl<Date>(null, Validators.required),
    room: this.room
  });

  reservation$: Subject<ReservationDto> = new BehaviorSubject<ReservationDto>(null);
  availableRooms$: Subject<LocationDto[]> = new BehaviorSubject<LocationDto[]>(null);
  reservationTitle$: Subject<string> = new BehaviorSubject<string>("Broneeringu info");
  bookingNumber: string;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private reservationService: ReservationService,
    private roomService: RoomService,
    private toastr: ToastrService,
    private router: Router,
    private location: Location,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getReservation();
  }

  ngAfterViewInit(): void {
    merge(this.editReservationForm.get("dateFrom").valueChanges, this.editReservationForm.get("dateTo").valueChanges).pipe(skip(2)).subscribe(() => {
      this.getAvailableRooms();
    });
  }

  getReservation(): void {
    const number = this.route.snapshot.paramMap.get('number');
    if (number) {
      this.bookingNumber = number;
      this.reservationService.getReservation(number).subscribe(result => {
        this.reservation$.next(result);
        this.fillForm(result);
      })
    }
  }

  fillForm(reservation: ReservationDto): void {
    this.editReservationForm.get("name")?.setValue(reservation.fullName);
    this.editReservationForm.get("personalCode")?.setValue(reservation.nationalIdentificationNumber);
    this.editReservationForm.get("email")?.setValue(reservation.email);
    this.editReservationForm.get("dateFrom")?.setValue(reservation.checkInStartUtc);
    this.editReservationForm.get("dateTo")?.setValue(reservation.checkOutUtc);
    this.availableRooms$.next([reservation.room]);
    this.reservationTitle$.next(`Info broneeringu ${reservation.bookingCode} kohta`);
    this.room.setValue(reservation.room.id);
    this.getAvailableRooms(reservation.bookingCode);
  }

  getAvailableRooms(bookingCode?: string): void {
    this.roomService.getAvailableRoomsAdmin(this.editReservationForm.get('dateFrom')?.value, this.editReservationForm.get('dateTo')?.value, bookingCode ?? this.bookingNumber)
      .subscribe(result => {
        this.availableRooms$.next(result);
      });
  }

  goBack(): void {
    this.router.navigateByUrl("/admin");
  }

  cancelReservation(): void {
    const data: DialogData = {
      title: `Tühista broneering ${this.bookingNumber}`,
      message: `Kas olete kindel, et soovite tühistada broneeringu?`,
      isCancelDisabled: false,
      okAction: () => {
        this.reservationService.cancelReservationAdmin(this.bookingNumber).subscribe((result) => {
          if (!!result) {
            this.toastr.success("Broneering edukalt tühistatud.");
            this.router.navigateByUrl("/admin");
          }
        })
      },
    };

    this.dialog.open(PopupDialogComponent, { data: data });
  }

  saveChanges(): void {
    if (!this.editReservationForm.valid) {
      this.editReservationForm.markAllAsTouched();
      return;
    }

    const updateBookingFunction = () => {
      const updatedBooking = {
        bookingCode: this.bookingNumber,
        fullName: this.editReservationForm.get('name')?.value ?? '',
        nationalIdentificationNumber:
          this.editReservationForm.get('personalCode')?.value ?? '',
        email: this.editReservationForm.get('email')?.value ?? '',
        checkInUtc: this.editReservationForm.get('dateFrom').value,
        checkOutUtc: this.editReservationForm.get('dateTo').value,
        roomId: this.room.value,
      } as ReservationUpdateDto;
  
      this.reservationService.updateBooking(updatedBooking).subscribe((result: ReservationDto) => {
        if (!!result.bookingCode) {
          this.getAvailableRooms();
          this.reservation$.next(result);
          this.toastr.success("Broneering on edukalt muudetud.");
        }
      });
    }

    const data: DialogData = {
      title: `Uuenda broneering ${this.bookingNumber}`,
      message: `Kas olete kindel, et soovite salvestada muudatused?`,
      isCancelDisabled: false,
      okAction: updateBookingFunction,
    };
    this.dialog.open(PopupDialogComponent, { data: data });
  }
}
