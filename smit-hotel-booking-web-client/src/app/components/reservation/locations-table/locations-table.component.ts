import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  Component,
  ContentChild,
  ContentChildren,
  Input,
  QueryList,
  ViewChild,
} from '@angular/core';
import {
  MatColumnDef,
  MatHeaderRowDef,
  MatNoDataRow,
  MatRowDef,
  MatTable,
  MatTableDataSource,
} from '@angular/material/table';
import { MatSort, SortDirection } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { LocationDto } from 'app/models/location-dto';

@Component({
  selector: 'app-locations-table',
  templateUrl: './locations-table.component.html',
  styleUrls: ['./locations-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class LocationsTableComponent {
  @ContentChildren(MatHeaderRowDef) headerRowDefs!: QueryList<MatHeaderRowDef>;
  @ContentChildren(MatRowDef) rowDefs!: QueryList<MatRowDef<LocationDto>>;
  @ContentChildren(MatColumnDef) columnDefs!: QueryList<MatColumnDef>;
  @ContentChild(MatNoDataRow) noDataRow!: MatNoDataRow;

  @ViewChild(MatTable, { static: true }) table!: MatTable<LocationDto>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  @Input() columns!: string[];

  private _dataSource!: MatTableDataSource<LocationDto>;

  @Input()
  set dataSource(dataSource: MatTableDataSource<LocationDto>) {
    this._dataSource = dataSource;
    this.resetSortPagination();
  }
  get dataSource(): MatTableDataSource<LocationDto> {
    return this._dataSource;
  }

  @Input() from?: Date;
  @Input() to?: Date;

  constructor(private router: Router) {
  }

  ngAfterViewInit(): void {
    this.resetSortPagination();
  }

  ngAfterContentInit() {
    this.columnDefs.forEach((columnDef) => this.table.addColumnDef(columnDef));
    this.rowDefs.forEach((rowDef) => this.table.addRowDef(rowDef));
    this.headerRowDefs.forEach((headerRowDef) =>
      this.table.addHeaderRowDef(headerRowDef)
    );
    this.table.setNoDataRow(this.noDataRow);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  clearFilter() {
    this.dataSource.filter = '';
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  resetSortPagination(forceSortColIndex? : number) {
    if (this.paginator != null) {
      this.dataSource.paginator = this.paginator;
    }
    if (this.sort != null) {
      if (forceSortColIndex != null) {
        this.sort.active = this.columns[forceSortColIndex];
        this.sort.direction = 'asc';    
      }

      this.sort.sortChange.emit();
      this.dataSource.sort = this.sort;
    }
  }

  reserve(location: LocationDto) {
    var json = encodeURIComponent(btoa(JSON.stringify({location, from: this.from, to: this.to})));
    this.router.navigate([`/uus-broneering/${json}`]);
  }
}
