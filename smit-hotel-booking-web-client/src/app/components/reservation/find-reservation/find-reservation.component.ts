import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {
  DialogData,
  PopupDialogComponent,
} from 'app/components/common/popup/popup-dialog.component';
import { ReservationDto } from 'app/models/reservation-dto';
import { ReservationService } from 'app/services/reservation.service';
import { notEmptyOrWhitespaceValidation } from 'app/validation/not-empty-or-whitespace-validation';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Location } from '@angular/common';

@Component({
  selector: 'app-find-reservation',
  templateUrl: './find-reservation.component.html',
  styleUrls: ['./find-reservation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FindReservationComponent {
  searchReservationForm = this.fb.group({
    reservationCode: [
      '',
      [Validators.required],
      [notEmptyOrWhitespaceValidation],
    ],
    fullName: ['', Validators.required, notEmptyOrWhitespaceValidation],
  });

  reservation$: BehaviorSubject<ReservationDto> =
    new BehaviorSubject<ReservationDto>(null);

  constructor(
    private fb: FormBuilder,
    private reservationService: ReservationService,
    private router: Router,
    private location: Location,
    public dialog: MatDialog
  ) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation?.extras?.state;

    if (state) {
      this.searchReservationForm.setValue({
        reservationCode: state['reservationCode'],
        fullName: state['fullName'],
      });
      this.searchReservation();
      this.clearSelectedTab();
    }
  }

  clearFields() {
    this.searchReservationForm.reset();
    this.searchReservationForm.markAllAsTouched();
    this.reservation$.next(null);
  }

  clearSelectedTab() {
    const currentUrl = this.location.path();
    const updatedUrl = currentUrl.split('#')[0];

    this.location.replaceState(updatedUrl);
  }

  searchReservation() {
    if (!this.searchReservationForm.valid) {
      this.searchReservationForm.markAllAsTouched();
      return;
    }

    let reservationCode =
      this.searchReservationForm.get('reservationCode')?.value;
    let fullName = this.searchReservationForm.get('fullName')?.value;

    this.reservationService
      .getBookingByCode(reservationCode, fullName)
      .subscribe((result) => {
        this.reservation$.next(result);
      });
  }

  cancelReservation() {
    if (this.reservation$.value == null) {
      return;
    }

    let reservationCode = this.reservation$.value.bookingCode;
    let fullName = this.reservation$.value.fullName;

    const cancelReservationCallback = () => {
      this.reservationService
        .cancelReservation(reservationCode, fullName)
        .subscribe((result) => {
          if (result) {
            const data: DialogData = {
              title: 'Broneering tühistatud',
              message: 'Teie broneering on edukalt tühistatud.',
              okAction: () => {
                location.reload();
              },
              isCancelDisabled: true
            };
            const dialogRef = this.dialog.open(PopupDialogComponent, {
              data: data,
            });
          }
        });
    };

    this.promptCancelReservationConfirmation(reservationCode, cancelReservationCallback);
  }

  promptCancelReservationConfirmation(reservationCode: string, callBack?: () => void) {
    const data: DialogData = {
      title: `Tühista broneering ${reservationCode}`,
      message: 'Kas olete kindel, et soovite oma broneeringu tühistada?',
      isCancelDisabled: false,
      okAction: () => {
        if (callBack) {
          callBack();
        }
      },
    };
    const dialogRef = this.dialog.open(PopupDialogComponent, {
      data: data,
    });
  }
}
