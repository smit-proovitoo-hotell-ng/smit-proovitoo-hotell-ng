import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  Validators,
} from '@angular/forms';
import { ReservationService } from '../../../services/reservation.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LocationDto } from '../../../models/location-dto';
import { BehaviorSubject } from 'rxjs';
import * as moment from 'moment';
import { ReservationCreateDto } from 'app/models/reservation-create-dto';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-new-reservation',
  templateUrl: './new-reservation.component.html',
  styleUrls: ['./new-reservation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NewReservationComponent implements AfterViewInit {
  
  private _isLoading = new BehaviorSubject<boolean>(false);
  isLoading$ = this._isLoading.asObservable();
  
  newReservationForm = this.fb.group({
    name: new FormControl<string>('', Validators.required),
    personalCode: new FormControl<string>('', [
      Validators.required
    ]),
    email: new FormControl<string>('', Validators.required),
  });

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private reservationService: ReservationService
  ) {}
  
  ngAfterViewInit(): void {
    try {
      const data = JSON.parse(atob(decodeURIComponent(this.route.snapshot.params['data'])));

      this.checkIn.next(data.from);
      this.checkOut.next(data.to);
      this.dates.next(
        moment(data.from).format('DD.MM.YYYY') +
          ' - ' +
          moment(data.to).format('DD.MM.YYYY')
      );
      this.location.next(data.location);
    } catch {
      this.router.navigate(['/']);
    }
  }

  @Input() title: string | undefined;
  location: BehaviorSubject<LocationDto> = new BehaviorSubject(
    {} as LocationDto
  );
  dates: BehaviorSubject<string> = new BehaviorSubject(null);
  checkIn: BehaviorSubject<Date> = new BehaviorSubject(null);
  checkOut: BehaviorSubject<Date> = new BehaviorSubject(null);

  goBack(): void {
    this.router.navigate(['/broneeringud'], {
      queryParams: { from: this.checkIn.value, to: this.checkOut.value },
    });
  }

  createBooking() {
    if (!this.newReservationForm.valid) {
      this.newReservationForm.markAllAsTouched();
      return;
    }

    this._isLoading.next(true);

    const newBooking = {
      fullName: this.newReservationForm.get('name')?.value ?? '',
      nationalIdentificationNumber:
        this.newReservationForm.get('personalCode')?.value ?? '',
      email: this.newReservationForm.get('email')?.value ?? '',
      checkInUtc: this.checkIn.value,
      checkOutUtc: this.checkOut.value,
      roomId: this.location.value.id,
    } as ReservationCreateDto;

    this.reservationService
      .createBooking(newBooking)
      .pipe(finalize(() => this._isLoading.next(false)))
      .subscribe((savedBooking) => {
        if (savedBooking?.bookingCode != null) {
          this.router.navigate(['/broneeringud'], {
            fragment: '1',
            state: {
              reservationCode: savedBooking.bookingCode,
              fullName: savedBooking.fullName
            },
          });
        }
      });
  }
}
