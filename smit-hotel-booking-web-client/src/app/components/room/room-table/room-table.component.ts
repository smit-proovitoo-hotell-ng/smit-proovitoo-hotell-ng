import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, ContentChild, ContentChildren, Input, QueryList, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatColumnDef, MatHeaderRowDef, MatNoDataRow, MatRowDef, MatTable, MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { LocationDto } from 'app/models/location-dto';
import { RoomService } from 'app/services/room.service';

@Component({
  selector: 'app-room-table',
  templateUrl: './room-table.component.html',
  styleUrls: ['./room-table.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class RoomTableComponent {
  @ContentChildren(MatHeaderRowDef) headerRowDefs!: QueryList<MatHeaderRowDef>;
  @ContentChildren(MatRowDef) rowDefs!: QueryList<MatRowDef<LocationDto>>;
  @ContentChildren(MatColumnDef) columnDefs!: QueryList<MatColumnDef>;
  @ContentChild(MatNoDataRow) noDataRow!: MatNoDataRow;

  @ViewChild(MatTable, { static: true }) table!: MatTable<LocationDto>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  @Input() columns!: string[];

  private _dataSource!: MatTableDataSource<LocationDto>;
  @Input()
  set dataSource(dataSource: MatTableDataSource<LocationDto>) {
    this._dataSource = dataSource;
    this.resetSortPagination();
  }
  get dataSource(): MatTableDataSource<LocationDto> {
    return this._dataSource;
  }

  expandedElement?: LocationDto | null;

  constructor(private router: Router, private roomService: RoomService) { }

  ngAfterViewInit(): void {
    this.resetSortPagination(0);
  }

  ngAfterContentInit() {
    this.columnDefs.forEach((columnDef) => this.table.addColumnDef(columnDef));
    this.rowDefs.forEach((rowDef) => this.table.addRowDef(rowDef));
    this.headerRowDefs.forEach((headerRowDef) =>
      this.table.addHeaderRowDef(headerRowDef)
    );
    this.table.setNoDataRow(this.noDataRow);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  resetSortPagination(forceSortColIndex?: number) {
    if (this.paginator != null) {
      this.dataSource.paginator = this.paginator;
    }
    if (this.sort != null) {
      if (forceSortColIndex != null) {
        this.sort.active = this.columns[forceSortColIndex];
        this.sort.direction = 'asc';
      }

      this.sort.sortChange.emit();
      this.dataSource.sort = this.sort;
    }
  }
}
