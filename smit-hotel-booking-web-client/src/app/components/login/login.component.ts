import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent {
  loginForm = this.fb.group({
    username: ['test-admin@netgroup.com', Validators.required],
    password: ['test-password', Validators.required],
  });

  constructor(private fb: FormBuilder, private authService: AuthService) {}

  onSubmit() {
    if (!this.loginForm.valid) {
      return;
    }
    let username = this.loginForm.get('username')?.value ?? '';
    let password = this.loginForm.get('password')?.value ?? '';
    this.authService.logIn(username, password).subscribe(() => {
      window.location.reload();
    });
  }
}
