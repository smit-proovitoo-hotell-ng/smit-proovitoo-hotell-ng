import { Component, Inject, ChangeDetectionStrategy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  title: string;
  message: string;
  isCancelDisabled?: boolean
  okAction: () => void;
  cancelAction?: () => void;
}

@Component({
  selector: 'app-popup-dialog',
  templateUrl: './popup-dialog.component.html',
  styleUrls: ['./popup-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<PopupDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,

  ) {
    dialogRef.disableClose = true; 
  }

  onOkClick(): void {
    if (this.data.okAction) {
      this.data.okAction();
    }
    this.dialogRef.close();
  }

  onCancelClick(): void {
    if (this.data.cancelAction) {
      this.data.cancelAction();
    }
    this.dialogRef.close();
  }
}
