import {
  ChangeDetectionStrategy,
  Component,
} from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import {
  ActivatedRoute,
  ActivatedRouteSnapshot,
  NavigationEnd,
  Route,
  Router,
} from '@angular/router';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Component({
  selector: 'toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolbarComponent {
  showLogOut$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private authService: AuthService, private router: Router) {
    this.showLogOut$.next(this.authService.isLoggedIn && /^\/admin(\/.*)?$/.test(router.url))
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.showLogOut$.next(
          this.authService.isLoggedIn && /^\/admin(\/.*)?$/.test(event.url)
        );
      }
    });
  }

  logOut() {
    this.authService.logOut().subscribe((_) => {
      this.router.navigate(['/login']);
    });
  }
}
