import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './modules/app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule, MatPaginatorIntl } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatListModule } from '@angular/material/list';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDialogModule } from '@angular/material/dialog';
import { ToastrModule } from 'ngx-toastr';
import { HttpErrorInterceptor } from './interceptors/http-error-interceptor';
import { EstonianMatPaginatorIntl } from './common/estonian-mat-paginator-intl';
import { LoaderComponent } from './components/common/loader/loader.component';

import {
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_FORMATS,
} from '@angular/material-moment-adapter';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
} from '@angular/material/core';
import { ToolbarComponent } from './components/common/toolbar/toolbar.component';
import { PageComponent } from './components/common/page/page.component';
import { PopupDialogComponent } from './components/common/popup/popup-dialog.component';
import { ReservationComponent } from './components/reservation/reservation.component';
import { LocationsTableComponent } from './components/reservation/locations-table/locations-table.component';
import { FindReservationComponent } from './components/reservation/find-reservation/find-reservation.component';
import { NewReservationComponent } from './components/reservation/new-reservation/new-reservation.component';
import { MatCardModule } from '@angular/material/card';
import { AdminHomeComponent } from './components/admin/admin-home/admin-home.component';
import { AllReservationsTableComponent } from './components/admin/all-reservations-table/all-reservations-table.component';
import { EditReservationComponent } from './components/reservation/edit-reservation/edit-reservation.component';
import { RoomTableComponent } from './components/room/room-table/room-table.component';


@NgModule({
  declarations: [
    AppComponent,
    PageComponent,
    ToolbarComponent,
    PopupDialogComponent,
    LoginComponent,
    AdminHomeComponent,
    AllReservationsTableComponent,
    ReservationComponent,
    LocationsTableComponent,
    FindReservationComponent,
    NewReservationComponent,
    EditReservationComponent,
    LoaderComponent,
    RoomTableComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDividerModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatTabsModule,
    MatDatepickerModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatListModule,
    MatChipsModule,
    MatProgressBarModule,
    MatMomentDateModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatCardModule,
    MatDialogModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    }),
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'et-EE' },
    { provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: { useUtc: true } },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    },
    { provide: MatPaginatorIntl, useClass: EstonianMatPaginatorIntl }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
