import { MatPaginatorIntl } from '@angular/material/paginator';

export class EstonianMatPaginatorIntl extends MatPaginatorIntl {
  override itemsPerPageLabel = 'Kirjeid lehel';
  override nextPageLabel = 'Järgmine leht';
  override previousPageLabel = 'Eelmine leht';
  override firstPageLabel = 'Esimene leht';
  override lastPageLabel = 'Viimane leht';

  override getRangeLabel = function (
    page: number,
    pageSize: number,
    length: number
  ) {
    if (length == 0 || pageSize == 0) {
      return `0 / ${length}`;
    }

    length = Math.max(length, 0);

    const startIndex = page * pageSize;
    const endIndex =
      startIndex < length
        ? Math.min(startIndex + pageSize, length)
        : startIndex + pageSize;

    return `${startIndex + 1} – ${endIndex} / ${length}`;
  };
}
