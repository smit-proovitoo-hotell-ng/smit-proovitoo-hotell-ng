import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { catchError, Observable, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { ApiErrorDto } from '../models/api-error-dto';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private toastr: ToastrService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError((error) => {
        if (error instanceof HttpErrorResponse) {
          if (error.error instanceof ErrorEvent) {
            // a client-side or network error
          } else {
            var apiError = error.error as ApiErrorDto;

            if (apiError.errorMessages != null) {
                apiError.errorMessages.forEach((errorMessage) => this.toastr.error(`${errorMessage}`));  
            }
            if (apiError.fieldErrorMessages != null) {
              Object.keys(apiError.fieldErrorMessages)?.forEach(key => {
                let errors: string[] = apiError.fieldErrorMessages[key];
                errors.forEach((errorMessage) => this.toastr.error(`${errorMessage}`));  
              });
            }
          }
        }
        return throwError(() => error);
    })
    );
  }
}
