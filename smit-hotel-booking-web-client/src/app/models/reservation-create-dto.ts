export interface ReservationCreateDto {
    fullName: string;
    email: string;
    nationalIdentificationNumber: string;
    roomId: number;
    checkInUtc: Date;
    checkOutUtc: Date;
}
