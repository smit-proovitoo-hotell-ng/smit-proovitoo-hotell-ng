export class ApiErrorDto {
  errorMessages?: string[];
  fieldErrorMessages?: { [key: string]: string[] };
}
