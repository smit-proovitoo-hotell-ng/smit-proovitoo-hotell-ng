export interface ReservationUpdateDto {
    bookingCode: string;
    fullName: string;
    email: string;
    nationalIdentificationNumber: string;
    roomId: number;
    checkInUtc: Date;
    checkOutUtc: Date;
}
