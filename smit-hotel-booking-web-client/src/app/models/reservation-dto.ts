import { LocationDto } from "./location-dto";

export interface ReservationDto {
    bookingCode: string;
    fullName: string;
    email: string;
    nationalIdentificationNumber: string;
    price: string;
    room: LocationDto;
    roomId: number;
    checkInStartUtc: Date;
    checkOutUtc: Date;
}
