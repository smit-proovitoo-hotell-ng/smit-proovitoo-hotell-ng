import { ReservationDto } from "./reservation-dto";

export interface LocationDto {
    id: number;
    price: string;
    maxOccupants: string;
    type: string;
    typeId: number;
    roomNumber: string;
    date?: string;
    bookings?: ReservationDto[];
    isAvailable?: boolean;
}
