﻿# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

# Build the application
FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["smit-hotel-booking-server/WebApi/WebApi.csproj", "WebApi/"]
RUN dotnet restore "WebApi/WebApi.csproj"
COPY smit-hotel-booking-server .
WORKDIR "/src/WebApi"
RUN dotnet build "WebApi.csproj" -c Release -o /app/build

# Publish the application
FROM build AS publish
RUN dotnet publish "WebApi.csproj" -c Release -o /app/publish

# Build SQL Server Express image
FROM mcr.microsoft.com/mssql/server:2019-latest AS sql
ENV ACCEPT_EULA=Y
ENV SA_PASSWORD=fA6cBmYex@Jv!BU5f5APfrdSCNQ5waKg_SA
EXPOSE 1433

# Create a SQL Server user
ENV DB_USER=smit-hotel-booking
ENV DB_PASSWORD=fA6cBmYex@Jv!BU5f5APfrdSCNQ5waKg

# Set up SQL Server script to create the user
COPY create-user.sql /docker-entrypoint-initdb.d/create-user.sql

# Final stage / image
FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "WebApi.dll"]
