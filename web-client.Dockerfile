# Build
FROM node:18-alpine as build
WORKDIR /app
COPY smit-hotel-booking-web-client .
RUN npm install
RUN npm run build --prod

# Serve app with nginx
FROM nginx:stable-alpine
COPY --from=build /app/dist /usr/share/nginx/html
COPY --from=build /app/nginx.conf /etc/nginx/conf.d/default.template

EXPOSE 80

CMD /bin/sh -c "envsubst \"`env | awk -F = '{printf \" \\\\$%s\", $1}'`\" < /etc/nginx/conf.d/default.template > /etc/nginx/conf.d/default.conf && exec nginx -g 'daemon off;'"