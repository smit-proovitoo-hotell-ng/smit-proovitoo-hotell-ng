using BLL.Database;
using BLL.Entities.Base;

namespace UnitTests.Helpers;

internal static class Extensions
{
    internal static async Task PopulateDatabaseAsync<T>(this IAppDbContext context, params T[] entities)
        where T : BaseEntity
    {
        context.Set<T>().AddRange(entities);
        await context.SaveChangesAsync();
    }
}