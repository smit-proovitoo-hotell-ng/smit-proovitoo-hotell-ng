using System.Reflection;
using AutoFixture;
using AutoFixture.Kernel;
using BLL.Entities.Base;

namespace UnitTests.Helpers;

internal class IgnoreBaseEntitiesCustomization : ICustomization
{
    public void Customize(IFixture fixture)
    {
        fixture.Customizations.Add(new IgnoreBaseEntitiesSpecimenBuilder());
    }
}

internal class IgnoreBaseEntitiesSpecimenBuilder : ISpecimenBuilder
{
    public object Create(object request, ISpecimenContext context)
    {
        var propertyInfo = request as PropertyInfo;

        if (propertyInfo == null) 
            return new NoSpecimen();

        if (propertyInfo.PropertyType == typeof(BaseEntity) ||
            propertyInfo.PropertyType.IsSubclassOf(typeof(BaseEntity)))
        {
            return new OmitSpecimen();
        }

        if (propertyInfo.PropertyType.IsGenericType)
        {
            var genericTypeDef = propertyInfo.PropertyType.GetGenericTypeDefinition();
            if ((genericTypeDef == typeof(IEnumerable<>) || genericTypeDef == typeof(List<>)) &&
                propertyInfo.PropertyType.GetGenericArguments()[0].IsSubclassOf(typeof(BaseEntity)))
            {
                return new OmitSpecimen();
            }
        }

        return new NoSpecimen();
    }
}