using AutoMapper;
using Common.Utilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace UnitTests;

public class Startup
{
    public static IServiceProvider? ServiceProvider { get; private set; }

    public void ConfigureServices(IServiceCollection services)
    {
        AppData.Configuration = new ConfigurationBuilder()
            .Build();

        services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

        ServiceProvider = services.BuildServiceProvider();
    }
}