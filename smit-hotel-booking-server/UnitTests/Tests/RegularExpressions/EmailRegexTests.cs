using System.Text.RegularExpressions;
using Common.Constants;
using Xunit;

namespace UnitTests.Tests.RegularExpressions;

public class EmailRegexTests
{
    private readonly Regex _emailRegex;

    public EmailRegexTests()
    {
        _emailRegex = ValidationConstants.EmailValidationRegex;
    }

    [Theory]
    [InlineData("user@example.com")]
    [InlineData("user.name@example.com")]
    [InlineData("user_name@example.com")]
    [InlineData("user+name@example.com")]
    [InlineData("user-name@example.com")]
    [InlineData("user@example.c")]
    [InlineData("user@example")]
    public void TestValidEmails(string email)
    {
        var result = _emailRegex.IsMatch(email);
        Assert.True(result);
    }

    [Theory]
    [InlineData("")]
    [InlineData(" ")]
    [InlineData("user@.com")]
    [InlineData("@example.com")]
    [InlineData("userexample.com")]
    [InlineData("user@exa mple.com")]
    [InlineData("user@-example.com")]
    [InlineData("username@exa_mple.com")]
    public void TestInvalidEmails(string email)
    {
        var result = _emailRegex.IsMatch(email);
        Assert.False(result);
    }
}