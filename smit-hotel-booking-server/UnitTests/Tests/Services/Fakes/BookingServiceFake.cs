using AutoMapper;
using BLL.Database.RepositoryBase;
using BLL.Entities;
using BLL.Services;
using Common.Providers;

namespace UnitTests.Tests.Services.Fakes;

public class BookingServiceFake : BookingService
{
    private readonly Queue<string> _bookingNumbers = new();

    public BookingServiceFake(
        IDateTimeProvider dateTimeProvider,
        IRepository<Order> ordersRepository,
        IRepository<Room> roomRepository,
        IMapper mapper,
        IEmailService emailService,
        IRoomService roomService) : base(
        dateTimeProvider, ordersRepository,
        roomRepository, mapper, emailService, roomService) { }

    public override string GetGeneratedBookingNumberWrapper()
    {
        return _bookingNumbers.Count > 0 ? _bookingNumbers.Dequeue() : base.GetGeneratedBookingNumberWrapper();
    }

    /// <summary>
    /// Allows to pre-define an ordered list of booking numbers that will be returned from <see cref="GetGeneratedBookingNumberWrapper"/>
    /// </summary>
    /// <param name="orderedBookingNumbers">Ordered list of booking numbers.</param>
    public void SetupBookingNumberSequence(params string[] orderedBookingNumbers)
    {
        foreach (var bookingNumber in orderedBookingNumbers)
        {
            _bookingNumbers.Enqueue(bookingNumber);
        }
    }
}