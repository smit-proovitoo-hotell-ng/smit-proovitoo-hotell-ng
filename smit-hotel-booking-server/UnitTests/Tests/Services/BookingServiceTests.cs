using AutoFixture;
using AutoMapper;
using BLL.Database;
using BLL.Database.RepositoryBase;
using BLL.Entities;
using BLL.Services;
using Common.Constants;
using Common.Dtos.Booking;
using Common.Dtos.Email;
using Common.Enums;
using Common.Providers;
using Moq;
using UnitTests.Helpers;
using UnitTests.Tests.Services.Fakes;
using Xunit;

namespace UnitTests.Tests.Services;

public class BookingServiceTests : BaseTestService
{
    #region CreateBooking

    [Fact]
    public async Task CreateBooking_ValidBooking_BookingCreated()
    {
        var utcNow = new DateTime(2023, 7, 18, 12, 30, 25);

        var roomId = 1;
        var checkInUtc = utcNow.AddDays(1);
        var checkOutUtc = utcNow.AddDays(2);

        var newBookingInfo = new BookingFlatDto
        {
            FullName = "Full Name",
            NationalIdentificationNumber = "12345",
            Email = "email@email.email",
            RoomId = roomId,
            CheckInUtc = checkInUtc,
            CheckOutUtc = checkOutUtc
        };

        var room = new Room
        {
            Id = roomId
        };

        using var context = CreateTestContext();

        var roomRepositoryMock = new Mock<IRepository<Room>>();
        roomRepositoryMock
            .Setup(rs => rs.GetByIdAsync(
                It.IsAny<int>()))
            .ReturnsAsync(room);

        var roomServiceMock = new Mock<IRoomService>();
        roomServiceMock
            .Setup(rs => rs.IsRoomBookedAsync(
                It.IsAny<int>(),
                It.IsAny<DateTime>(),
                It.IsAny<DateTime>(),
                It.IsAny<string>()))
            .ReturnsAsync(false);

        var emailServiceMock = new Mock<IEmailService>();

        var dateTimeProvider = CreateDateTimeProvider(utcNow: utcNow);

        var bookingService = CreateBookingService<BookingService>(
            appDbContext: context,
            dateTimeProvider: dateTimeProvider,
            roomRepository: roomRepositoryMock.Object,
            roomService: roomServiceMock.Object,
            emailService: emailServiceMock.Object);

        var result = await bookingService.CreateBookingAsync(newBookingInfo);

        emailServiceMock.Verify(m => m.SendEmailAsync(It.IsAny<EmailDto>(), It.IsAny<bool>()), Times.Once());

        Assert.True(result?.IsSuccess);
        Assert.NotNull(result?.Value?.Number);
    }

    [Theory]
    [InlineData("2023-07-18T12:30:25", "2023-07-17T12:30:25", "2023-07-20T12:30:25")] // check-in is in the past
    [InlineData("2023-07-18T12:30:25", "2023-07-19T12:30:25", "2023-07-17T12:30:25")] // check-out is in the past
    [InlineData("2023-07-18T12:30:25", "2023-07-19T12:30:25", "2023-07-19T12:30:25")] // check-out is the same as check-in
    [InlineData("2023-07-18T12:30:25", "2023-07-19T12:30:25", "2023-07-18T12:30:25")] // check-out is before check-in
    public async Task CreateBooking_InvalidTimePeriod_ReturnsError(DateTime utcNow, DateTime checkInUtc, DateTime checkOutUtc)
    {
        var roomId = 1;

        var newBookingInfo = new BookingFlatDto
        {
            RoomId = roomId,
            CheckInUtc = checkInUtc,
            CheckOutUtc = checkOutUtc
        };

        var room = new Room
        {
            Id = roomId
        };

        using var context = CreateTestContext();

        var roomRepositoryMock = new Mock<IRepository<Room>>();
        roomRepositoryMock
            .Setup(rs => rs.GetByIdAsync(
                It.IsAny<int>()))
            .ReturnsAsync(room);

        var roomServiceMock = new Mock<IRoomService>();
        roomServiceMock
            .Setup(rs => rs.IsRoomBookedAsync(
                It.IsAny<int>(),
                It.IsAny<DateTime>(),
                It.IsAny<DateTime>(),
                It.IsAny<string>()))
            .ReturnsAsync(false);

        var dateTimeProvider = CreateDateTimeProvider(utcNow: utcNow);

        var bookingService = CreateBookingService<BookingService>(
            appDbContext: context,
            dateTimeProvider: dateTimeProvider,
            roomRepository: roomRepositoryMock.Object,
            roomService: roomServiceMock.Object);

        var result = await bookingService.CreateBookingAsync(newBookingInfo);

        Assert.False(result?.IsSuccess);
    }

    [Fact]
    public async Task CreateBooking_RoomAlreadyBooked_ReturnsError()
    {
        var utcNow = new DateTime(2023, 7, 18, 12, 30, 25);

        var roomId = 1;
        var checkInUtc = utcNow.AddDays(1);
        var checkOutUtc = utcNow.AddDays(2);

        var newBookingInfo = new BookingFlatDto
        {
            RoomId = roomId,
            CheckInUtc = checkInUtc,
            CheckOutUtc = checkOutUtc
        };

        var room = new Room
        {
            Id = roomId
        };

        using var context = CreateTestContext();

        var roomRepositoryMock = new Mock<IRepository<Room>>();
        roomRepositoryMock
            .Setup(rs => rs.GetByIdAsync(
                It.IsAny<int>()))
            .ReturnsAsync(room);

        var roomServiceMock = new Mock<IRoomService>();
        roomServiceMock
            .Setup(rs => rs.IsRoomBookedAsync(
                It.IsAny<int>(), 
                It.IsAny<DateTime>(), 
                It.IsAny<DateTime>(), 
                It.IsAny<string>()))
            .ReturnsAsync(true);

        var dateTimeProvider = CreateDateTimeProvider(utcNow: utcNow);

        var bookingService = CreateBookingService<BookingService>(
            appDbContext: context,
            dateTimeProvider: dateTimeProvider,
            roomRepository: roomRepositoryMock.Object,
            roomService: roomServiceMock.Object);

        var result = await bookingService.CreateBookingAsync(newBookingInfo);

        Assert.False(result?.IsSuccess);
    }

    [Fact]
    public async Task CreateBooking_InvalidRoom_ReturnsError()
    {
        var utcNow = new DateTime(2023, 7, 18, 12, 30, 25);

        var roomId = 1;
        var checkInUtc = utcNow.AddDays(1);
        var checkOutUtc = utcNow.AddDays(2);

        var newBookingInfo = new BookingFlatDto
        {
            RoomId = roomId,
            CheckInUtc = checkInUtc,
            CheckOutUtc = checkOutUtc
        };

        using var context = CreateTestContext();

        var roomRepositoryMock = new Mock<IRepository<Room>>();
        roomRepositoryMock
            .Setup(rs => rs.GetByIdAsync(
                It.IsAny<int>()))
            .ReturnsAsync((Room?)null);

        var roomServiceMock = new Mock<IRoomService>();
        roomServiceMock
            .Setup(rs => rs.IsRoomBookedAsync(
                It.IsAny<int>(),
                It.IsAny<DateTime>(),
                It.IsAny<DateTime>(),
                It.IsAny<string>()))
            .ReturnsAsync(false);

        var dateTimeProvider = CreateDateTimeProvider(utcNow: utcNow);

        var bookingService = CreateBookingService<BookingService>(
            appDbContext: context,
            dateTimeProvider: dateTimeProvider,
            roomRepository: roomRepositoryMock.Object,
            roomService: roomServiceMock.Object);

        var result = await bookingService.CreateBookingAsync(newBookingInfo);

        Assert.False(result?.IsSuccess);
    }

    #endregion

    #region CancelBooking

    [Fact]
    public void CancelBooking_MinOrderCancellationDaysIsThree()
    {
        Assert.Equal(ValidationConstants.MIN_ORDER_CANCELLATION_DAYS, 3);
    }

    [Fact]
    public async Task CancelBooking_BookingExistsCancellationIsValid_CancelsOrder()
    {
        var utcNow = new DateTime(2023, 7, 18, 12, 30, 25);

        var bookingCode = "BOOKING_CODE";
        var fullName = "Full Name";
        var checkInUtc = utcNow.AddDays(ValidationConstants.MIN_ORDER_CANCELLATION_DAYS + 1);
        var checkOutUtc = checkInUtc.AddDays(1);
        var orderStatus = OrderStatus.Confirmed;
        
        var bookingQueryDto = new BookingQueryDto { BookingCode = bookingCode, FullName = fullName };

        var order = CreateFixture()
            .Build<Order>()
            .With(o => o.OrderNumber, bookingCode)
            .With(o => o.FullName, fullName)
            .With(o => o.CheckInUtc, checkInUtc)
            .With(o => o.CheckOutUtc, checkOutUtc)
            .With(o => o.Status, orderStatus)
            .Without(o => o.Bookings)
            .Create();

        using var context = CreateTestContext();
        await context.PopulateDatabaseAsync(order);

        var dateTimeProvider = CreateDateTimeProvider(utcNow: utcNow);
        var orderRepository = new Repository<Order>(context);
        var bookingService = CreateBookingService<BookingServiceFake>(appDbContext: context, dateTimeProvider: dateTimeProvider, ordersRepository: orderRepository);

        var result = await bookingService.CancelBookingAsync(bookingQueryDto, skipMinimumDayValidation: false);

        order = await orderRepository.GetByIdAsync(order.Id);

        Assert.Equal(OrderStatus.Cancelled, order?.Status);
        Assert.True(result?.IsSuccess);
    }

    [Fact]
    public async Task CancelBooking_BookingExistsCancellationIsNotAllowed_DoesNotCancelOrder()
    {
        var utcNow = new DateTime(2023, 7, 18, 12, 30, 25);

        var bookingCode = "BOOKING_CODE";
        var fullName = "Full Name";
        var checkInUtc = utcNow.AddDays(ValidationConstants.MIN_ORDER_CANCELLATION_DAYS - 1);
        var checkOutUtc = checkInUtc.AddDays(1);
        var orderStatus = OrderStatus.Confirmed;

        var bookingQueryDto = new BookingQueryDto { BookingCode = bookingCode, FullName = fullName };

        var order = CreateFixture()
            .Build<Order>()
            .With(o => o.OrderNumber, bookingCode)
            .With(o => o.FullName, fullName)
            .With(o => o.CheckInUtc, checkInUtc)
            .With(o => o.CheckOutUtc, checkOutUtc)
            .With(o => o.Status, orderStatus)
            .Without(o => o.Bookings)
            .Create();

        using var context = CreateTestContext();
        await context.PopulateDatabaseAsync(order);

        var dateTimeProvider = CreateDateTimeProvider(utcNow: utcNow);
        var orderRepository = new Repository<Order>(context);
        var bookingService = CreateBookingService<BookingServiceFake>(appDbContext: context, dateTimeProvider: dateTimeProvider, ordersRepository: orderRepository);

        var result = await bookingService.CancelBookingAsync(bookingQueryDto, skipMinimumDayValidation: false);

        order = await orderRepository.GetByIdAsync(order.Id);

        Assert.Equal(orderStatus, order.Status);
        Assert.NotEqual(OrderStatus.Cancelled, order?.Status);
        Assert.False(result?.IsSuccess);
    }

    [Fact]
    public async Task CancelBookingAsync_BookingDoesNotExist_ReturnsNull()
    {
        var bookingCode = "BOOKING_CODE";
        var fullName = "Full Name";
        var bookingQueryDto = new BookingQueryDto { BookingCode = bookingCode, FullName = fullName };

        using var context = CreateTestContext();

        var bookingService = CreateBookingService<BookingServiceFake>(appDbContext: context);

        var result = await bookingService.CancelBookingAsync(bookingQueryDto, skipMinimumDayValidation: false);

        Assert.Null(result);
    }

    #endregion

    #region GetUniqueBookingNumber

    [Fact]
    public async Task GetUniqueBookingNumber_RetriesIfBookingNumberExists()
    {
        var utcNow = new DateTime(2023, 7, 18, 12, 30, 25);
        var existingBookingNumber = "EXISTING_NUMBER";

        var fixture = CreateFixture();
        fixture.Customize<Order>(o => o
            .With(x => x.OrderNumber, existingBookingNumber)
            .With(x => x.CheckInUtc, utcNow.AddDays(1))
            .With(x => x.CheckOutUtc, utcNow.AddDays(2))
            .With(x => x.Status, OrderStatus.Confirmed)
            .Without(x => x.Bookings));

        var existingOrder = fixture.Create<Order>();
        using var context = CreateTestContext();
        await context.PopulateDatabaseAsync(existingOrder);

        var dateTimeProvider = CreateDateTimeProvider(utcNow: utcNow);
        var bookingService = CreateBookingService<BookingServiceFake>(appDbContext: context, dateTimeProvider: dateTimeProvider);

        bookingService.SetupBookingNumberSequence(existingBookingNumber);

        var generatedBookingNumber = await bookingService.GetUniqueBookingNumberAsync();

        Assert.NotEqual(existingBookingNumber, generatedBookingNumber);
    }

    [Fact]
    public async Task GetUniqueBookingNumber_IgnoresCancelledOrders()
    {
        var utcNow = new DateTime(2023, 7, 18, 12, 30, 25);
        var validReusableCancelledBookingNumber = "VALID_REUSABLE_NUMBER";

        var fixture = CreateFixture();
        fixture.Customize<Order>(o => o
            .With(x => x.OrderNumber, validReusableCancelledBookingNumber)
            .With(x => x.CheckInUtc, utcNow.AddDays(1))
            .With(x => x.CheckOutUtc, utcNow.AddDays(2))
            .With(x => x.Status, OrderStatus.Cancelled)
            .Without(x => x.Bookings));

        var cancelledOrder = fixture.Create<Order>();
        using var context = CreateTestContext();
        await context.PopulateDatabaseAsync(cancelledOrder);

        var dateTimeProvider = CreateDateTimeProvider(utcNow: utcNow);
        var bookingService = CreateBookingService<BookingServiceFake>(appDbContext: context, dateTimeProvider: dateTimeProvider);

        bookingService.SetupBookingNumberSequence(validReusableCancelledBookingNumber);

        var generatedBookingNumber = await bookingService.GetUniqueBookingNumberAsync();

        Assert.Equal(validReusableCancelledBookingNumber, generatedBookingNumber);
    }

    [Fact]
    public async Task GetUniqueBookingNumber_IgnoresPastOrders()
    {
        var utcNow = new DateTime(2023, 7, 18, 12, 30, 25);
        var validReusablePastBookingNumber = "VALID_REUSABLE_NUMBER";

        var fixture = CreateFixture();
        fixture.Customize<Order>(o => o
            .With(x => x.OrderNumber, validReusablePastBookingNumber)
            .With(x => x.CheckInUtc, DateTime.UtcNow.AddDays(-2))
            .With(x => x.CheckOutUtc, DateTime.UtcNow.AddDays(-1))
            .With(x => x.Status, OrderStatus.Confirmed)
            .Without(x => x.Bookings));

        var pastOrder = fixture.Create<Order>();
        using var context = CreateTestContext();
        await context.PopulateDatabaseAsync(pastOrder);

        var dateTimeProvider = CreateDateTimeProvider(utcNow: utcNow);
        var bookingService = CreateBookingService<BookingServiceFake>(appDbContext:context, dateTimeProvider: dateTimeProvider);

        bookingService.SetupBookingNumberSequence(validReusablePastBookingNumber);

        var generatedBookingNumber = await bookingService.GetUniqueBookingNumberAsync();

        Assert.Equal(validReusablePastBookingNumber, generatedBookingNumber);
    }

    #endregion

    #region Helpers

    private T CreateBookingService<T>(
        IAppDbContext? appDbContext = null,
        IDateTimeProvider? dateTimeProvider = null,
        IRepository<Order>? ordersRepository = null,
        IRepository<Room>? roomRepository = null,
        IMapper? mapper = null,
        IEmailService? emailService = null,
        IRoomService? roomService = null)
        where T : IBookingService
    {
        var dateTimeProviderObj = dateTimeProvider ?? new Mock<IDateTimeProvider>().Object;
        var appDbContextObj = appDbContext ?? CreateTestContext(dateTimeProvider: dateTimeProviderObj);
        var mapperObj = mapper ?? GetResolvedMapper();
        var roomRepositoryObj = roomRepository ?? new Repository<Room>(appDbContextObj);
        var ordersRepositoryObj = ordersRepository ?? new Repository<Order>(appDbContextObj);
        var emailServiceObj = emailService ?? new Mock<IEmailService>().Object;
        var roomServiceObj = roomService ?? new Mock<IRoomService>().Object;

        // do not use Activator, hides constructor change compilation errors
        if (typeof(T) == typeof(BookingServiceFake))
        {
            return (T)(object)new BookingServiceFake(
                dateTimeProviderObj, ordersRepositoryObj, roomRepositoryObj,
                mapperObj, emailServiceObj, roomServiceObj);
        }

        return (T)(object)new BookingService(
            dateTimeProviderObj, ordersRepositoryObj, roomRepositoryObj,
            mapperObj, emailServiceObj, roomServiceObj);
    }

    #endregion
}