using AutoFixture;
using AutoMapper;
using BLL.Database;
using BLL.Database.RepositoryBase;
using BLL.Entities;
using BLL.Services;
using Common.Enums;
using Common.Providers;
using Moq;
using UnitTests.Helpers;
using Xunit;

namespace UnitTests.Tests.Services;

public class RoomTestService : BaseTestService
{
    #region GetAvailableRooms

    [Fact]
    public async Task GetAvailableRooms_AllRoomsAvailable_ReturnsAllRooms()
    {
        var utcNow = new DateTime(2023, 7, 18, 12, 30, 25);

        var checkInUtc = utcNow.AddDays(1);
        var checkOutUtc = utcNow.AddDays(2);

        var fixture = CreateFixture();
        var rooms = fixture.Build<Room>()
            .With(r => r.RoomType, fixture.Create<RoomType>())
            .CreateMany(5)
            .ToArray();

        using var context = CreateTestContext();
        await context.PopulateDatabaseAsync(rooms);

        var dateTimeProvider = CreateDateTimeProvider(utcNow: utcNow);
        var roomService = CreateRoomService(appDbContext: context, dateTimeProvider: dateTimeProvider);

        var availableRooms = await roomService.GetAvailableRoomsAsync(checkInUtc, checkOutUtc);

        Assert.Equal(5, availableRooms.Value?.Count);
    }

    [Fact]
    public async Task GetAvailableRooms_SomeRoomsAvailable_ReturnsAvailableRooms()
    {
        var utcNow = new DateTime(2023, 7, 18, 12, 30, 25);

        var checkInUtc = utcNow.AddDays(3);
        var checkOutUtc = utcNow.AddDays(4);

        var totalRoomsCount = 5;
        var freeRoomCount = 4;

        var fixture = CreateFixture();

        var rooms = fixture.Build<Room>()
            .With(r => r.RoomType, fixture.Create<RoomType>())
            .CreateMany(totalRoomsCount) 
            .ToArray();

        fixture.Customize<OrderBooking>(ob => ob
            .With(b => b.Room, rooms[0]) // first room is booked
            .With(b => b.Order, fixture.Build<Order>()
                .With(o => o.CheckInUtc, utcNow.AddDays(2))
                .With(o => o.CheckOutUtc, utcNow.AddDays(5))
                .With(o => o.Status, OrderStatus.Confirmed)
                .Without(o => o.Bookings)
                .Create()));

        var booking = fixture.Create<OrderBooking>();

        using var context = CreateTestContext();
        await context.PopulateDatabaseAsync(rooms);
        await context.PopulateDatabaseAsync(booking);

        var dateTimeProvider = CreateDateTimeProvider(utcNow: utcNow);
        var roomService = CreateRoomService(appDbContext: context, dateTimeProvider: dateTimeProvider);

        var availableRooms = await roomService.GetAvailableRoomsAsync(checkInUtc, checkOutUtc);

        Assert.Equal(freeRoomCount, availableRooms.Value?.Count);
    }

    [Fact]
    public async Task GetAvailableRooms_NoRoomsAvailable_ReturnsEmptyList()
    {
        var utcNow = new DateTime(2023, 7, 18, 12, 30, 25);

        var checkInUtc = utcNow.AddDays(1);
        var checkOutUtc = utcNow.AddDays(2);

        var fixture = CreateFixture();

        fixture.Customize<OrderBooking>(ob => ob
            .With(b => b.Room, fixture.Build<Room>()
                .With(r => r.RoomType, fixture.Create<RoomType>())
                .Create())
            .With(b => b.Order, fixture.Build<Order>()
                .With(o => o.CheckInUtc, utcNow.AddDays(1))
                .With(o => o.CheckOutUtc, utcNow.AddDays(2))
                .With(o => o.Status, OrderStatus.Confirmed)
                .Without(o => o.Bookings)
                .Create()));

        var bookings = fixture.CreateMany<OrderBooking>(5).ToArray();

        using var context = CreateTestContext();
        await context.PopulateDatabaseAsync(bookings);

        var dateTimeProvider = CreateDateTimeProvider(utcNow: utcNow);
        var roomService = CreateRoomService(appDbContext: context, dateTimeProvider: dateTimeProvider);

        var availableRooms = await roomService.GetAvailableRoomsAsync(checkInUtc, checkOutUtc);

        Assert.NotNull(availableRooms.Value);
        Assert.Empty(availableRooms.Value);
    }

    #endregion

    #region IsRoomBooked

    [Fact]
    public async Task IsRoomBookedAsync_NoOverlap_ReturnsFalse()
    {
        var utcNow = new DateTime(2023, 7, 18, 12, 30, 25);

        var roomId = 1;
        var checkInUtc = utcNow.AddDays(2);
        var checkOutUtc = utcNow.AddDays(3);

        var fixture = CreateFixture();

        fixture.Customize<OrderBooking>(ob => ob
            .With(b => b.RoomId, roomId)
            .With(b => b.Order, fixture.Build<Order>()
                .With(o => o.CheckInUtc, utcNow.AddDays(4))
                .With(o => o.CheckOutUtc, utcNow.AddDays(5))
                .With(o => o.Status, OrderStatus.Confirmed)
                .Without(o => o.Bookings)
                .Create()));

        var booking = fixture.Create<OrderBooking>();

        using var context = CreateTestContext();
        await context.PopulateDatabaseAsync(booking);

        var roomService = CreateRoomService(orderBookingRepository: new Repository<OrderBooking>(context));

        var isRoomBooked = await roomService.IsRoomBookedAsync(roomId, checkInUtc, checkOutUtc);

        Assert.False(isRoomBooked);
    }

    [Fact]
    public async Task IsRoomBooked_DifferentRoom_ReturnsFalse()
    {
        var utcNow = new DateTime(2023, 7, 18, 12, 30, 25);

        var roomIdSaved = 2;
        var roomdIdValidated = 3;
        var checkInUtc = utcNow.AddDays(1);
        var checkOutUtc = utcNow.AddDays(2);

        var fixture = CreateFixture();

        fixture.Customize<OrderBooking>(ob => ob
            .With(b => b.RoomId, roomIdSaved)
            .With(b => b.Order, fixture.Build<Order>()
                .With(o => o.CheckInUtc, utcNow.AddDays(1))
                .With(o => o.CheckOutUtc, utcNow.AddDays(2))
                .With(o => o.Status, OrderStatus.Confirmed)
                .Without(o => o.Bookings)
                .Create()));

        var booking = fixture.Create<OrderBooking>();

        using var context = CreateTestContext();
        await context.PopulateDatabaseAsync(booking);

        var roomService = CreateRoomService(orderBookingRepository: new Repository<OrderBooking>(context));

        var isRoomBooked = await roomService.IsRoomBookedAsync(roomdIdValidated, checkInUtc, checkOutUtc);

        Assert.False(isRoomBooked);
    }

    [Fact]
    public async Task IsRoomBooked_StartOverlap_ReturnsTrue()
    {
        var utcNow = new DateTime(2023, 7, 18, 12, 30, 25);

        var roomId = 1;
        var checkInUtc = utcNow.AddDays(1);
        var checkOutUtc = utcNow.AddDays(2);

        var fixture = CreateFixture();

        fixture.Customize<OrderBooking>(ob => ob
            .With(b => b.RoomId, roomId)
            .With(b => b.Order, fixture.Build<Order>()
                .With(o => o.CheckInUtc, utcNow.AddDays(1))
                .With(o => o.CheckOutUtc, utcNow.AddDays(3))
                .With(o => o.Status, OrderStatus.Confirmed)
                .Without(o => o.Bookings)
                .Create()));

        var booking = fixture.Create<OrderBooking>();

        using var context = CreateTestContext();
        await context.PopulateDatabaseAsync(booking);

        var roomService = CreateRoomService(orderBookingRepository: new Repository<OrderBooking>(context));

        var isRoomBooked = await roomService.IsRoomBookedAsync(roomId, checkInUtc, checkOutUtc);

        Assert.True(isRoomBooked);
    }

    [Fact]
    public async Task IsRoomBooked_EndOverlap_ReturnsTrue()
    {
        var utcNow = new DateTime(2023, 7, 18, 12, 30, 25);

        var roomId = 1;
        var checkInUtc = utcNow.AddDays(3);
        var checkOutUtc = utcNow.AddDays(4);

        var fixture = CreateFixture();

        fixture.Customize<OrderBooking>(ob => ob
            .With(b => b.RoomId, roomId)
            .With(b => b.Order, fixture.Build<Order>()
                .With(o => o.CheckInUtc, utcNow.AddDays(2))
                .With(o => o.CheckOutUtc, utcNow.AddDays(3))
                .With(o => o.Status, OrderStatus.Confirmed)
                .Without(o => o.Bookings)
                .Create()));

        var booking = fixture.Create<OrderBooking>();

        using var context = CreateTestContext();
        await context.PopulateDatabaseAsync(booking);

        var roomService = CreateRoomService(orderBookingRepository: new Repository<OrderBooking>(context));

        var isRoomBooked = await roomService.IsRoomBookedAsync(roomId, checkInUtc, checkOutUtc);

        Assert.True(isRoomBooked);
    }

    [Fact]
    public async Task IsRoomBooked_FullyOverlap_ReturnsTrue()
    {
        var utcNow = new DateTime(2023, 7, 18, 12, 30, 25);

        var roomId = 1;
        var checkInUtc = utcNow.AddDays(1);
        var checkOutUtc = utcNow.AddDays(3);

        var fixture = CreateFixture();

        fixture.Customize<OrderBooking>(ob => ob
            .With(b => b.RoomId, roomId)
            .With(b => b.Order, fixture.Build<Order>()
                .With(o => o.CheckInUtc, utcNow.AddDays(1))
                .With(o => o.CheckOutUtc, utcNow.AddDays(3))
                .With(o => o.Status, OrderStatus.Confirmed)
                .Without(o => o.Bookings)
                .Create()));

        var booking = fixture.Create<OrderBooking>();

        using var context = CreateTestContext();
        await context.PopulateDatabaseAsync(booking);

        var roomService = CreateRoomService(orderBookingRepository: new Repository<OrderBooking>(context));

        var isRoomBooked = await roomService.IsRoomBookedAsync(roomId, checkInUtc, checkOutUtc);

        Assert.True(isRoomBooked);
    }

    #endregion

    #region Helpers

    private RoomService CreateRoomService(
        IAppDbContext? appDbContext = null,
        IMapper? mapper = null,
        IDateTimeProvider? dateTimeProvider = null,
        IRepository<OrderBooking>? orderBookingRepository = null,
        IRepository<Room>? roomRepository = null)
    {
        var dateTimeProviderObj = dateTimeProvider ?? new Mock<IDateTimeProvider>().Object;
        var appDbContextObj = appDbContext ?? CreateTestContext(dateTimeProvider: dateTimeProviderObj);
        var mapperObj = mapper ?? GetResolvedMapper();
        var orderBookingRepositoryObj = orderBookingRepository ?? new Repository<OrderBooking>(appDbContextObj);
        var roomRepositoryObj = roomRepository ?? new Repository<Room>(appDbContextObj);

        return new RoomService(
            mapperObj,
            dateTimeProviderObj,
            orderBookingRepositoryObj,
            roomRepositoryObj);
    }

    #endregion
}