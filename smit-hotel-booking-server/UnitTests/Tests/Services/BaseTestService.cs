using AutoFixture;
using AutoMapper;
using BLL.Database;
using Common.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using UnitTests.Helpers;

namespace UnitTests.Tests.Services;

public abstract class BaseTestService
{
    protected virtual Fixture CreateFixture()
    {
        var fixture = new Fixture();

        // remove circular dependency errors
        fixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList()
            .ForEach(b => fixture.Behaviors.Remove(b));
        fixture.Behaviors.Add(new OmitOnRecursionBehavior());
        
        // skip EF navigational properties entirely
        fixture.Customize(new IgnoreBaseEntitiesCustomization());

        return fixture;
    }
    
    protected virtual IAppDbContext CreateTestContext(IDateTimeProvider? dateTimeProvider = null,
        IActiveUserProvider? activeUserProvider = null,
        string? databaseName = null)
    {
        dateTimeProvider ??= new Mock<IDateTimeProvider>().Object;
        activeUserProvider ??= new Mock<IActiveUserProvider>().Object;
        databaseName ??= Guid.NewGuid().ToString();

        var options = new DbContextOptionsBuilder<AppDbContext>()
            .UseInMemoryDatabase(databaseName: databaseName)
            .Options;

        return new AppDbContext(dateTimeProvider, activeUserProvider, options);
    }

    protected virtual IDateTimeProvider CreateDateTimeProvider(DateTime? now = null, DateTime? utcNow = null)
    {
        var dateTimeProviderMock = new Mock<IDateTimeProvider>();

        if (now is not null)
            dateTimeProviderMock.Setup(dtp => dtp.Now).Returns(now.Value);
        if (utcNow is not null)
            dateTimeProviderMock.Setup(dtp => dtp.UtcNow).Returns(utcNow.Value);

        return dateTimeProviderMock.Object;
    }

    protected virtual IMapper GetResolvedMapper()
    {
        return Startup.ServiceProvider.GetRequiredService<IMapper>();
    }
}