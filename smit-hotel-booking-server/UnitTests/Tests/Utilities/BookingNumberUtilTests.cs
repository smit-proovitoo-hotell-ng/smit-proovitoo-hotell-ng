using Common.Utilities;
using Xunit;

namespace UnitTests.Tests.Utilities;

public class BookingNumberUtilTest
{
    [Fact]
    public void TestGeneratedBookingNumberIsValid()
    {
        var bookingNumber = BookingNumberUtil.GenerateBookingNumber();

        Assert.True(BookingNumberUtil.IsValidBookingNumberFormat(bookingNumber));
    }

    [Theory]
    [InlineData("#ABCDEFG")]
    [InlineData("#1234567")]
    [InlineData("#ABC1234")]
    [InlineData("#abc1234")]
    public void TestValidBookingNumbers(string bookingNumber)
    {
        Assert.True(BookingNumberUtil.IsValidBookingNumberFormat(bookingNumber));
    }

    [Theory]
    [InlineData(null)]
    [InlineData("")]
    [InlineData(" ")]
    [InlineData("ABCDEFGH")]
    [InlineData("#ABCDE1")]
    [InlineData("#ABCDEFGHI")]
    [InlineData("#ABC@345")]
    public void TestInvalidBookingNumbers(string bookingNumber)
    {
        Assert.False(BookingNumberUtil.IsValidBookingNumberFormat(bookingNumber));
    }
}