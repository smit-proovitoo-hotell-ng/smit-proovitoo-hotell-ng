using Common.Utilities;
using Xunit;

namespace UnitTests.Tests.Utilities;

public class HashUtilTests
{
    [Fact]
    public void Hash_ReturnsDifferentResultForSameInput()
    {
        var input = "test_password";

        var hash1 = HashUtil.Hash(input);
        var hash2 = HashUtil.Hash(input);

        Assert.NotEqual(hash1, hash2);
    }

    [Fact]
    public void Hash_ReturnsNotNullOrEmpty()
    {
        var input = "test_password";

        var hash = HashUtil.Hash(input);

        Assert.False(string.IsNullOrEmpty(hash));
    }

    [Fact]
    public void VerifyHash_ReturnsTrueForCorrectInput()
    {
        var input = "test_password";

        var hash = HashUtil.Hash(input);

        var isValid = HashUtil.VerifyHash(input, hash);

        Assert.True(isValid);
    }

    [Fact]
    public void VerifyHash_ReturnsFalseForIncorrectInput()
    {
        var input = "test_password";
        var incorrectInput = "wrong_password";

        var hash = HashUtil.Hash(input);

        var isValid = HashUtil.VerifyHash(incorrectInput, hash);

        Assert.False(isValid);
    }

    [Theory]
    [InlineData(null, "hash")]
    [InlineData("text", null)]
    [InlineData("", "hash")]
    [InlineData("text", "")]
    [InlineData(null, null)]
    [InlineData("", "")]
    public void VerifyHash_ReturnsFalseForNullOrEmptyInput(string text, string correctHash)
    {
        var isValid = HashUtil.VerifyHash(text, correctHash);

        Assert.False(isValid);
    }
}