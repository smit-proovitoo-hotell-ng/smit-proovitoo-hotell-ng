using Common.Utilities;
using Xunit;

namespace UnitTests.Tests.Utilities;

public class SemaphoreLockTests
{
    [Fact]
    public void SemaphoreLock_EnsuresSingleAccessInMultiThreadedScenario()
    {
        var lockKey = 1;
        var sharedResource = 0;
        var semaphoreLock = new SemaphoreLock<int>();

        var tasks = Enumerable.Range(0, 100)
            .Select(_ => Task.Run(() =>
            {
                semaphoreLock.Wait(lockKey);
                try
                {
                    sharedResource++;
                }
                finally
                {
                    semaphoreLock.Release(lockKey);
                }
            }))
            .ToArray();

        Task.WaitAll(tasks);

        Assert.Equal(100, sharedResource);
    }

    [Fact]
    public async Task SemaphoreLock_WaitAsync_WorksCorrectly()
    {
        var lockKey = 1;
        var sharedResource = 0;
        var semaphoreLock = new SemaphoreLock<int>();

        var tasks = Enumerable.Range(0, 100)
            .Select(_ => Task.Run(async () =>
            {
                await semaphoreLock.WaitAsync(lockKey);
                try
                {
                    sharedResource++;
                }
                finally
                {
                    semaphoreLock.Release(lockKey);
                }
            }))
            .ToArray();

        await Task.WhenAll(tasks);

        Assert.Equal(100, sharedResource);
    }

    [Fact]
    public void SemaphoreLock_AllowsConcurrentAccessToDifferentResources()
    {
        var semaphoreLock = new SemaphoreLock<int>();

        var tasks = Enumerable.Range(0, 100)
            .Select(i => Task.Run(() =>
            {
                semaphoreLock.Wait(i);
                try
                {
                    Thread.Sleep(10);
                }
                finally
                {
                    semaphoreLock.Release(i);
                }
            }))
            .ToArray();

        Task.WaitAll(tasks);
    }

    [Fact]
    public async Task SemaphoreLock_HandlesExceptionsCorrectly()
    {
        var lockKey = 1;
        var semaphoreLock = new SemaphoreLock<int>();

        await Assert.ThrowsAsync<InvalidOperationException>(() =>
        {
            semaphoreLock.Wait(lockKey);
            try
            {
                throw new InvalidOperationException();
            }
            finally
            {
                semaphoreLock.Release(lockKey);
            }
        });

        // If the lock is working correctly, it should have been released in the finally block,
        // and we should be able to acquire it again.
        await semaphoreLock.WaitAsync(lockKey);
        semaphoreLock.Release(lockKey);
    }
}