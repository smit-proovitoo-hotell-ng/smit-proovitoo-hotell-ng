using Common.Providers;
using Common.Utilities;
using Moq;
using Xunit;

namespace UnitTests.Tests.Utilities;

public class CheckInCheckOutUtilTests
{
    [Fact]
    public void ValidateDates_CheckInInThePast_ReturnsFalse()
    {
        var dateTimeProvider = new Mock<IDateTimeProvider>();
        dateTimeProvider.Setup(d => d.UtcNow).Returns(new DateTime(2023, 7, 15));

        var checkIn = new DateTime(2023, 7, 14);
        var checkOut = new DateTime(2023, 7, 16);

        var result = CheckInCheckOutUtil.ValidateDateAndAdjustTime(dateTimeProvider.Object, ref checkIn, ref checkOut);

        Assert.False(result);
    }

    [Fact]
    public void ValidateDates_CheckOutInThePast_ReturnsFalse()
    {
        var dateTimeProvider = new Mock<IDateTimeProvider>();
        dateTimeProvider.Setup(d => d.UtcNow).Returns(new DateTime(2023, 7, 15));

        var checkIn = new DateTime(2023, 7, 16);
        var checkOut = new DateTime(2023, 7, 14);

        var result = CheckInCheckOutUtil.ValidateDateAndAdjustTime(dateTimeProvider.Object, ref checkIn, ref checkOut);

        Assert.False(result);
    }

    [Fact]
    public void ValidateDates_CheckOutIsBeforeCheckIn_ReturnsFalse()
    {
        var dateTimeProvider = new Mock<IDateTimeProvider>();
        dateTimeProvider.Setup(d => d.UtcNow).Returns(new DateTime(2023, 7, 15));

        var checkIn = new DateTime(2023, 7, 17);
        var checkOut = new DateTime(2023, 7, 16);

        var result = CheckInCheckOutUtil.ValidateDateAndAdjustTime(dateTimeProvider.Object, ref checkIn, ref checkOut);

        Assert.False(result);
    }

    [Fact]
    public void ValidateDates_MinimumStayLessThanADay_ReturnsFalse()
    {
        var dateTimeProvider = new Mock<IDateTimeProvider>();
        dateTimeProvider.Setup(d => d.UtcNow).Returns(new DateTime(2023, 7, 15));

        var checkIn = new DateTime(2023, 7, 16);
        var checkOut = new DateTime(2023, 7, 16);

        var result = CheckInCheckOutUtil.ValidateDateAndAdjustTime(dateTimeProvider.Object, ref checkIn, ref checkOut);

        Assert.False(result);
    }

    [Fact]
    public void ValidateDates_ValidDates_ReturnsTrueAndAdjustsTimes()
    {
        var dateTimeProvider = new Mock<IDateTimeProvider>();
        dateTimeProvider.Setup(d => d.UtcNow).Returns(new DateTime(2023, 7, 15, 14, 0, 0));

        var checkIn = new DateTime(2023, 7, 16);
        var checkOut = new DateTime(2023, 7, 17);
        var defaultCheckInHour = 14;
        var defaultCheckOutHour = 11;

        var result = CheckInCheckOutUtil.ValidateDateAndAdjustTime(dateTimeProvider.Object, ref checkIn, ref checkOut,
            defaultCheckInHour, defaultCheckOutHour);

        Assert.True(result);
        Assert.Equal(new DateTime(2023, 7, 16, defaultCheckInHour, 0, 0), checkIn);
        Assert.Equal(new DateTime(2023, 7, 17, defaultCheckOutHour, 0, 0), checkOut);
    }

    [Fact]
    public void ValidateDates_CheckInTodayAfterDefaultHour_ReturnsTrueAndAdjustsCheckInTime()
    {
        var defaultCheckInHour = 14;
        var defaultCheckOutHour = 11;
        var now = new DateTime(2023, 7, 15, defaultCheckInHour + 2, 0,
            0); // now is 2 hours after the default check-in hour

        var dateTimeProvider = new Mock<IDateTimeProvider>();
        dateTimeProvider.Setup(d => d.UtcNow).Returns(now);

        var checkIn = new DateTime(2023, 7, 15);
        var checkOut = new DateTime(2023, 7, 16);

        var result = CheckInCheckOutUtil.ValidateDateAndAdjustTime(dateTimeProvider.Object, ref checkIn, ref checkOut,
            defaultCheckInHour, defaultCheckOutHour);

        Assert.True(result);
        Assert.Equal(now.Date + TimeSpan.FromHours(now.Hour), checkIn);
        Assert.Equal(new DateTime(2023, 7, 16, defaultCheckOutHour, 0, 0), checkOut);
    }
}