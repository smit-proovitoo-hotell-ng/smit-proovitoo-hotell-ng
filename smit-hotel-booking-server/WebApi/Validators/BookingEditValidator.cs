using Common.Constants;
using Common.Dtos.Api.Booking;
using FluentValidation;

namespace WebApi.Validators;

public class BookingEditValidator : AbstractValidator<BookingUpdateApiDto>
{
    public BookingEditValidator()
    {
        RuleFor(bc => bc.BookingCode)
            .NotEmpty()
            .MaximumLength(ValidationConstants.DEFAULT_STRING_MAX_LENGTH);

        RuleFor(bc => bc.FullName)
            .NotEmpty()
            .MaximumLength(ValidationConstants.DEFAULT_STRING_MAX_LENGTH);

        RuleFor(bc => bc.NationalIdentificationNumber)
            .NotEmpty()
            .MaximumLength(ValidationConstants.DEFAULT_STRING_MAX_LENGTH);

        RuleFor(bc => bc.Email)
            .NotEmpty()
            .DependentRules(() =>
            {
                RuleFor(ad => ad.Email)
                    .Matches(ValidationConstants.EmailValidationRegex).WithMessage("Vigane e-maili aadress.");
            });

        RuleFor(bc => bc.CheckInUtc)
            .NotEqual(default(DateTime))
            .WithMessage("'{PropertyName}' on kohustuslik.");

        RuleFor(bc => bc.CheckOutUtc)
            .NotEqual(default(DateTime))
            .WithMessage("'{PropertyName}' on kohustuslik.");

        RuleFor(bc => bc.RoomId)
            .NotEqual(default(int))
            .WithMessage("'{PropertyName}' on kohustuslik.");
    }
}
