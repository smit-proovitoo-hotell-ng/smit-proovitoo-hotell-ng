using FluentValidation;
using Common.Dtos.Api.Authentication;

namespace WebApi.Validators;

public class LoginValidator : AbstractValidator<LoginApiDto>
{
    private const int MAX_USERNAME_LENGTH = 256;
    private const int MAX_PASSWORD_LENGTH = 256;

    public LoginValidator()
    {
        RuleFor(x => x.Username)
            .NotEmpty()
            .MaximumLength(MAX_USERNAME_LENGTH);

        RuleFor(x => x.Password)
            .NotEmpty()
            .MaximumLength(MAX_PASSWORD_LENGTH);
    }
}
