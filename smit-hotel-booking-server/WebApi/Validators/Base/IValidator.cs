using FluentValidation.Results;

namespace WebApi.Validators.Base;

public interface IValidator
{
    Task<ValidationResult> ValidateAsync<T>(T instance);
}