using FluentValidation;
using FluentValidation.Results;

namespace WebApi.Validators.Base;

public class Validator : IValidator
{
    private readonly IValidatorFactory _validatorFactory;

    public Validator(IValidatorFactory validatorFactory)
    {
        _validatorFactory = validatorFactory;
    }

    public async Task<ValidationResult> ValidateAsync<T>(T instance)
    {
        var validator = _validatorFactory.GetValidator<T>();

        if (validator == null)
            return new ValidationResult();

        return await validator.ValidateAsync(instance);
    }
}