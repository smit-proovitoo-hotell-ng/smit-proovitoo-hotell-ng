using Common.Constants;
using Common.Dtos.Api.Booking;
using Common.Utilities;
using FluentValidation;

namespace WebApi.Validators;

public class BookingQueryValidator : AbstractValidator<BookingQueryApiDto>
{
    public BookingQueryValidator()
    {
        RuleFor(bq => bq.FullName)
            .NotEmpty()
            .MaximumLength(ValidationConstants.DEFAULT_STRING_MAX_LENGTH);

        RuleFor(ad => ad.BookingCode)
            .NotEmpty()
            .DependentRules(() =>
            {
                RuleFor(ad => ad.BookingCode)
                    .Must(BookingNumberUtil.IsValidBookingNumberFormat).WithMessage("Tundmatu broneeringu number.");
            });
    }
}