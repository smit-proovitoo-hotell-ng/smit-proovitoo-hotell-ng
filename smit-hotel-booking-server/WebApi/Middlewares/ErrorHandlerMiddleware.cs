using System.Net.Mime;
using Common.Dtos.Api.Error;
using Common.Loggers;
using Common.Providers;

namespace WebApi.Middlewares;

public class ErrorHandlerMiddleware
{
    private static readonly IAppLogger s_logger = AppLoggerProvider.GetCurrentClassLogger();

    private readonly RequestDelegate _next;

    public ErrorHandlerMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task Invoke(HttpContext context)
    {
        try
        {
            await _next(context);
        }
        catch (Exception ex)
        {
            s_logger.Error(ex, "Unhandled exception occurred.");
            await HandleExceptionAsync(context);
        }
    }

    private static Task HandleExceptionAsync(HttpContext context)
    {
        context.Response.StatusCode = StatusCodes.Status500InternalServerError;
        context.Response.ContentType = MediaTypeNames.Application.Json;

        var response = ErrorApiDto.New("Tekkis süsteemne viga, palun proovige uuesti.");

        return context.Response.WriteAsJsonAsync(response);
    }
}