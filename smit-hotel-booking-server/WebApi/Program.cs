using System.Globalization;
using AutoMapper;
using BLL.Database;
using BLL.Database.RepositoryBase;
using BLL.Entities;
using BLL.Services;
using Common.Constants;
using Common.Dtos.Authentication;
using Common.Providers;
using Common.Utilities;
using FluentValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using NLog;
using NLog.Extensions.Logging;
using NLog.Web;
using WebApi.Middlewares;
using WebApi.Validators.Base;

var startupLogger = LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();

try
{
    var builder = WebApplication.CreateBuilder(args);

    AppData.Configuration = builder.Configuration;
    builder.Configuration.AddJsonFile("appsettings.json");
    builder.Configuration.AddJsonFile("appsettings.Development.json", optional: true);
    builder.Configuration.AddEnvironmentVariables();

    builder.Logging.ClearProviders();
    builder.Host.UseNLog();

    ConfigSettingLayoutRenderer.DefaultConfiguration = builder.Configuration;
    LogManager.Configuration.Variables[NLogConstants.VariableNames.CONNECTION_STRING] = builder.Configuration.GetConnectionString(CK.CONNECTION_STRING_NAME);

    builder.Services.AddCors(c =>
        c.AddDefaultPolicy(policy => policy
            .SetIsOriginAllowed(_ => true)
            .AllowAnyHeader()
            .AllowAnyMethod()
            .AllowCredentials())
    );

    builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
        .AddCookie(options => options.Cookie.Name = AuthenticationConstants.JWT_COOKIE_NAME)
        .AddJwtBearer(options =>
        {
            options.TokenValidationParameters = JwtUtil.GetTokenValidationParameters(new JwtDto
            {
                Audience = AppData.Configuration[CK.JWT_AUDIENCE],
                Issuer = AppData.Configuration[CK.JWT_ISSUER],
                SecretKey = AppData.Configuration[CK.JWT_KEY]
            });
            options.Events = new JwtBearerEvents
            {
                OnMessageReceived = context =>
                {
                    context.Token = context.Request.Cookies[AuthenticationConstants.JWT_COOKIE_NAME];
                    return Task.CompletedTask;
                }
            };
        });

    builder.Services.AddDbContext<IAppDbContext, AppDbContext>(options =>
    {
        options.UseSqlServer(AppData.Configuration.GetConnectionString(CK.CONNECTION_STRING_NAME));
    });

    builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

    builder.Services.AddControllers();
    builder.Services.AddEndpointsApiExplorer();

    builder.Services.AddValidatorsFromAssembly(typeof(Program).Assembly);
    ValidatorOptions.Global.LanguageManager.Culture = new CultureInfo("et");

    builder.Services.AddSwaggerGen();

    builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
    builder.Services.AddScoped<IActiveUserProvider, ActiveUserProvider>();
    builder.Services.AddScoped<IDateTimeProvider, DateTimeProvider>();
    builder.Services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
    builder.Services.AddScoped<IAuthenticationService, AuthenticationService>();
    builder.Services.AddScoped<IPasswordHasher<User>, PasswordHasher<User>>();
    builder.Services.AddScoped<WebApi.Validators.Base.IValidator, Validator>();
    builder.Services.AddScoped<IValidatorFactory, ServiceProviderValidatorFactory>();
    builder.Services.AddScoped<IEmailService, EmailService>();
    builder.Services.AddScoped<IBookingService, BookingService>();
    builder.Services.AddScoped<IRoomService, RoomService>();


    var app = builder.Build();

    app.UseSwagger();
    app.UseSwaggerUI();

    app.UseHttpsRedirection();

    app.UseMiddleware<ErrorHandlerMiddleware>();

    app.UseAuthentication();
    app.UseAuthorization();

    app.MapControllers();

    app.UseCors();


    await using (var scope = app.Services.CreateAsyncScope())
    {
        using var context = scope.ServiceProvider.GetService<IAppDbContext>();
        await context.RunMigrationsAsync();
    }

    app.Run();
}
catch (Exception ex)
{
    startupLogger.Error(ex, "Error during app startup.");
    throw;
}