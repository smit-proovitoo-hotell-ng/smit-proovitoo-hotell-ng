using AutoMapper;
using BLL.Services;
using Common.Dtos.Api.Admin;
using Common.Dtos.Api.Booking;
using Common.Dtos.Api.Error;
using Common.Dtos.Api.Room;
using Common.Dtos.Booking;
using Common.Dtos.Result;
using Common.Dtos.Room;
using Common.Extensions;
using Common.Utilities;
using Microsoft.AspNetCore.Mvc;
using WebApi.Validators.Base;

namespace WebApi.Controllers.Areas.Admin;

public class HotelManagementController : BaseAdminController
{
    private readonly IMapper _mapper;
    private readonly IValidator _validator;
    private readonly IBookingService _bookingService;
    private readonly IRoomService _roomService;

    public HotelManagementController(IMapper mapper, IValidator validator,
        IBookingService bookingService, IRoomService roomService)
    {
        _mapper = mapper;
        _validator = validator;
        _bookingService = bookingService;
        _roomService = roomService;
    }

    [HttpGet("Booking/{code}")]
    public async Task<IActionResult> GetBooking(string code)
    {
        var booking = await _bookingService.GetBookingByCodeAsync(code);

        if (booking is null)
            return NotFound(ErrorApiDto.New("Broneeringut ei leitud."));

        return Ok(_mapper.Map<BookingApiDto>(booking));
    }

    [HttpGet("Booking/All")]
    public async Task<IActionResult> GetBookings(DateTime? from, DateTime? to)
    {
        var bookings = await _bookingService.GetAllActiveBookings(from, to);

        if (bookings is null)
            return NotFound(ErrorApiDto.New("Broneeringuid ei leitud."));

        return Ok(_mapper.Map<IEnumerable<BookingDto>, IEnumerable<BookingApiDto>>(bookings));
    }

    [HttpPost("Booking/Update")]
    public async Task<IActionResult> UpdateBooking(BookingUpdateApiDto bookingInfo)
    {
        if (bookingInfo is null)
            return BadRequest(ErrorApiDto.New("Tekkis viga."));

        var modelValidationResult = await _validator.ValidateAsync(bookingInfo);

        if (!modelValidationResult.IsValid)
            return BadRequest(new ErrorApiDto(modelValidationResult));

        var updateResult = await _bookingService.UpdateBookingAsync(_mapper.Map<BookingFlatDto>(bookingInfo));

        if (!updateResult.IsSuccess || updateResult.Value is null)
            return BadRequest(new ErrorApiDto(updateResult));

        return Ok(_mapper.Map<BookingApiDto>(updateResult.Value));
    }

    [HttpPost("Booking/Cancel")]
    public async Task<IActionResult> ForceCancelBooking(BookingQueryApiDto bookingInfo)
    {
        if (bookingInfo.BookingCode is null)
            return BadRequest(ErrorApiDto.New("Broneeringu kood ei tohi olla tühi"));

        var result = await _bookingService.CancelBookingAsync(_mapper.Map<BookingQueryDto>(bookingInfo), skipMinimumDayValidation: true);

        if (result is null)
            return NotFound(ErrorApiDto.New("Broneeringut ei leitud."));
        if (!result.IsSuccess)
            return BadRequest(new ErrorApiDto(result));

        return Ok(true);
    }

    [HttpGet("Room/Available")]
    public async Task<IActionResult> GetAvailableRooms(DateTime? from, DateTime? to, string? bookingCode)
    {
        if (from is null || to is null || to.Value.Date <= from.Value.Date)
            return BadRequest(ErrorApiDto.New("Vigane broneerimise periood."));

        var getAvailableRoomsResult = await _roomService.GetAvailableRoomsAsync(from.Value.Date, to.Value.Date, bookingCode);

        if (!getAvailableRoomsResult.IsSuccess || getAvailableRoomsResult.Value is null)
            return BadRequest(new ErrorApiDto(getAvailableRoomsResult));

        return Ok(_mapper.Map<IEnumerable<RoomDto>, IEnumerable<RoomApiDto>>(getAvailableRoomsResult.Value));
    }

    [HttpGet("Room/All")]
    public async Task<IActionResult> GetAllRooms(DateTime? from, DateTime? to)
    {
        var getAllRoomsResult = await _roomService.GetAllRoomsAsync();
        
        var availableRooms = (await _roomService.GetAvailableRoomsAsync(from ?? DateTime.Today, to?.EndOfDay() ?? DateTime.Today.EndOfDay(), ""))
            .Value?
            .Select(x => x.Id).ToHashSet() ?? new HashSet<int>();

        if (!getAllRoomsResult.IsSuccess || getAllRoomsResult.Value is null)
            return BadRequest(new ErrorApiDto(getAllRoomsResult));

        var allRooms = getAllRoomsResult.Value;
        foreach (var room in allRooms)
        {
            room.IsAvailable = availableRooms.Contains(room.Id);
        }

        return Ok(_mapper.Map<IEnumerable<RoomDto>, IEnumerable<RoomApiDto>>(allRooms));
    }
}