using Common.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.Areas.Admin;

[ApiController]
[Route("api/admin/[controller]")]
[Authorize(Roles = UserConstants.Roles.ADMIN)]
public abstract class BaseAdminController : Controller
{

}
