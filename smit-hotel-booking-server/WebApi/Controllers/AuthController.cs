using AutoMapper;
using BLL.Services;
using Common.Constants;
using Common.Dtos.Api.Authentication;
using Common.Dtos.Api.Error;
using Common.Dtos.Api.User;
using Common.Dtos.Authentication;
using Common.Loggers;
using Common.Providers;
using Common.Utilities;
using Microsoft.AspNetCore.Mvc;
using WebApi.Validators.Base;

namespace WebApi.Controllers;

public class AuthController : BaseController
{
    private static readonly IAppLogger s_logger = AppLoggerProvider.GetCurrentClassLogger();

    private readonly IDateTimeProvider _dateTimeProvider;
    private readonly IMapper _mapper;
    private readonly IValidator _validator;
    private readonly IActiveUserProvider _activeUserProvider;
    private readonly IAuthenticationService _authenticationService;

    public AuthController(
        IDateTimeProvider dateTimeProvider,
        IMapper mapper,
        IValidator validator,
        IActiveUserProvider activeUserProvider,
        IAuthenticationService authenticationService)
    {
        _dateTimeProvider = dateTimeProvider;
        _mapper = mapper;
        _validator = validator;
        _activeUserProvider = activeUserProvider;
        _authenticationService = authenticationService;
    }

    [HttpPost("LogIn")]
    public async Task<IActionResult> LogIn(LoginApiDto loginApiDto)
    {
        if (_activeUserProvider.ActiveUser is not null)
            return BadRequest(ErrorApiDto.New("Te olete juba sisseloginud."));

        if (loginApiDto is null)
            return BadRequest(ErrorApiDto.New("Tekkis viga."));

        var modelValidationResult = await _validator.ValidateAsync(loginApiDto);

        if (!modelValidationResult.IsValid)
            return BadRequest(new ErrorApiDto(modelValidationResult));

        var verifyResult = await _authenticationService.VerifyUserPassword(_mapper.Map<LoginDto>(loginApiDto));

        if (!verifyResult.IsSuccess || verifyResult.Value is null)
            return BadRequest();

        var jwtInfo = new JwtDto
        {
            Audience = AppData.Configuration[CK.JWT_AUDIENCE],
            Issuer = AppData.Configuration[CK.JWT_ISSUER],
            SecretKey = AppData.Configuration[CK.JWT_KEY]
        };
        var tokenLifetimeSeconds = AppData.Configuration.GetValue<int>(CK.JWT_TOKEN_LIFETIME);

        var jwtToken =
            JwtUtil.CreateJwtToken(_dateTimeProvider, jwtInfo, verifyResult.Value, tokenLifetimeSeconds);

        Response.Cookies.Append(AuthenticationConstants.JWT_COOKIE_NAME, jwtToken,
            new CookieOptions
            {
                HttpOnly = true,
                Secure = true,
                IsEssential = true,
                SameSite = SameSiteMode.None,
                Expires = _dateTimeProvider.UtcNow.AddSeconds(tokenLifetimeSeconds)
            });

        s_logger.Info($"User ID: {verifyResult?.Value?.UserId} logged in.");

        return Ok();
    }

    [HttpPost("LogOut")]
    public ActionResult LogOut()
    {
        Response.Cookies.Delete(AuthenticationConstants.JWT_COOKIE_NAME,
            new CookieOptions { Secure = true, SameSite = SameSiteMode.None });

        s_logger.Info($"User ID: {_activeUserProvider.ActiveUser?.UserId} logged out.");

        return Ok();
    }

    [HttpGet("UserData")]
    public UserApiDto? GetUserData()
    {
        return _mapper.Map<UserApiDto>(_activeUserProvider.ActiveUser);
    }

    [HttpGet("IsAuthenticated")]
    public bool IsAuthenticated() => _activeUserProvider.ActiveUser is not null;
}