using AutoMapper;
using BLL.Services;
using Common.Dtos.Api.Error;
using Common.Dtos.Api.Room;
using Common.Dtos.Room;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers;

public class RoomController : BaseController
{
    private readonly IMapper _mapper;
    private readonly IRoomService _roomService;

    public RoomController(IMapper mapper, IRoomService roomService)
    {
        _mapper = mapper;
        _roomService = roomService;
    }
    
    [HttpGet("Available")]
    public async Task<IActionResult> Rooms([FromQuery] DateTime? from, [FromQuery] DateTime? to)
    {
        if (from is null || to is null || to.Value.Date <= from.Value.Date)
            return BadRequest(ErrorApiDto.New("Vigane broneerimise periood."));

        var getAvailableRoomsResult = await _roomService.GetAvailableRoomsAsync(from.Value.Date, to.Value.Date);

        if (!getAvailableRoomsResult.IsSuccess || getAvailableRoomsResult.Value is null)
            return BadRequest(new ErrorApiDto(getAvailableRoomsResult));

        return Ok(_mapper.Map<IEnumerable<RoomDto>, IEnumerable<RoomApiDto>>(getAvailableRoomsResult.Value));

      
    }
}
