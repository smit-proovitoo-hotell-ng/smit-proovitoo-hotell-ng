using AutoMapper;
using BLL.Services;
using Common.Dtos.Api.Booking;
using Common.Dtos.Api.Error;
using Common.Dtos.Booking;
using Microsoft.AspNetCore.Mvc;
using WebApi.Validators.Base;

namespace WebApi.Controllers;

public class BookingController : BaseController
{
    private readonly IMapper _mapper;
    private readonly IValidator _validator;
    private readonly IBookingService _bookingService;

    public BookingController(IMapper mapper, IValidator validator,
        IBookingService bookingService)
    {
        _mapper = mapper;
        _validator = validator;
        _bookingService = bookingService;
    }

    [HttpPost("Create")]
    public async Task<IActionResult> CreateBooking(BookingCreateApiDto bookingInfo)
    {
        if (bookingInfo is null)
            return BadRequest(ErrorApiDto.New("Tekkis viga."));

        var modelValidationResult = await _validator.ValidateAsync(bookingInfo);

        if (!modelValidationResult.IsValid)
            return BadRequest(new ErrorApiDto(modelValidationResult));

        var createResult = await _bookingService.CreateBookingAsync(_mapper.Map<BookingFlatDto>(bookingInfo));

        if (!createResult.IsSuccess || createResult.Value is null)
            return BadRequest(new ErrorApiDto(createResult));

        return Ok(_mapper.Map<BookingApiDto>(createResult.Value));
    }

    [HttpPost("Search")]
    public async Task<IActionResult> SearchBookings(BookingQueryApiDto bookingInfo)
    {
        var errorResult = await ValidateBookingQuery(bookingInfo);

        if (errorResult is not null)
            return BadRequest(errorResult);

        var booking = await _bookingService.GetBookingByCodeAndNameAsync(_mapper.Map<BookingQueryDto>(bookingInfo));

        if (booking is null)
            return NotFound(ErrorApiDto.New("Broneeringuid ei leitud."));

        return Ok(_mapper.Map<BookingApiDto>(booking));
    }

    [HttpPost("Cancel")]
    public async Task<IActionResult> CancelBooking(BookingQueryApiDto bookingInfo)
    {
        var errorResult = await ValidateBookingQuery(bookingInfo);

        if (errorResult is not null)
            return BadRequest(errorResult);

        var result = await _bookingService.CancelBookingAsync(_mapper.Map<BookingQueryDto>(bookingInfo));

        if (result is null)
            return NotFound(ErrorApiDto.New("Broneeringut ei leitud."));
        if (!result.IsSuccess)
            return BadRequest(new ErrorApiDto(result));

        return Ok(true);
    }

    private async Task<ErrorApiDto?> ValidateBookingQuery(BookingQueryApiDto? bookingInfo)
    {
        if (bookingInfo is null)
            return ErrorApiDto.New("Tekkis viga.");

        var modelValidationResult = await _validator.ValidateAsync(bookingInfo);

        return !modelValidationResult.IsValid 
            ? new ErrorApiDto(modelValidationResult) 
            : null;
    }
}