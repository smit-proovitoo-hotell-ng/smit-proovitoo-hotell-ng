using AutoMapper;
using BLL.Entities;
using Common.Dtos.Booking;
using Common.Dtos.Room;
using Common.Dtos.User;

namespace BLL.Mappers;

public class EntityMappingProfile : Profile
{
    public EntityMappingProfile()
    {
        CreateMap<Role, RoleDto>()
            .ForMember(dest => dest.RoleId, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.SystemName, opt => opt.MapFrom(src => src.SystemName))
            .ForAllOtherMembers(opt => opt.Ignore());

        CreateMap<User, UserDto>()
            .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.EmailAddress))
            .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.Roles))
            .ForAllOtherMembers(opt => opt.Ignore());

        CreateMap<Room, RoomDto>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.PriceEur))
            .ForMember(dest => dest.TypeId, opt => opt.MapFrom(src => src.RoomTypeId))
            .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.RoomType.Name))
            .ForMember(dest => dest.MaxOccupants, opt => opt.MapFrom(src => src.RoomType.MaxCapacity))
            .ForMember(dest => dest.RoomNumber, opt => opt.MapFrom(src => src.RoomNumber))
            .ForMember(dest => dest.Bookings, opt => opt.MapFrom(src => src.OrderBookings));

        CreateMap<OrderBooking, RoomDto>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.RoomId))
            .ForMember(dest => dest.TypeId, opt => opt.MapFrom(src => src.Room.RoomTypeId))
            .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Room.RoomType.Name))
            .ForMember(dest => dest.MaxOccupants, opt => opt.MapFrom(src => src.Room.RoomType.MaxCapacity))
            .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.RoomPriceEur))
            .ForMember(dest => dest.RoomNumber, opt => opt.MapFrom(src => src.Room.RoomNumber));

        CreateMap<OrderBooking, BookingFlatDto>()
            .ForMember(dest => dest.BookingCode, opt => opt.MapFrom(src => src.Order.OrderNumber))
            .ForMember(dest => dest.CheckInUtc, opt => opt.MapFrom(src => src.Order.CheckInUtc))
            .ForMember(dest => dest.CheckOutUtc, opt => opt.MapFrom(src => src.Order.CheckOutUtc))
            .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Order.Email))
            .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.Order.FullName))
            .ForMember(dest => dest.NationalIdentificationNumber,
                opt => opt.MapFrom(src => src.Order.NationalIdentificationNumber));

        CreateMap<Order, BookingDto>()
            .ForMember(dest => dest.Number, opt => opt.MapFrom(src => src.OrderNumber))
            .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FullName))
            .ForMember(dest => dest.NationalIdentificationNumber,
                opt => opt.MapFrom(src => src.NationalIdentificationNumber))
            .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
            .ForMember(dest => dest.CheckInStartUtc, opt => opt.MapFrom(src => src.CheckInUtc))
            .ForMember(dest => dest.CheckOutUtc, opt => opt.MapFrom(src => src.CheckOutUtc))
            .ForMember(dest => dest.BookingRooms, opt => opt.MapFrom(src => src.Bookings))
            .ForAllOtherMembers(opt => opt.Ignore());
    }
}