using AutoMapper;
using BLL.Database.RepositoryBase;
using BLL.Entities;
using Common.Constants;
using Common.Dtos.Booking;
using Common.Dtos.Email;
using Common.Dtos.Result;
using Common.Enums;
using Common.Extensions;
using Common.Loggers;
using Common.Providers;
using Common.Utilities;
using Microsoft.EntityFrameworkCore;

namespace BLL.Services;

public class BookingService : IBookingService
{
    private static readonly IAppLogger s_logger = AppLoggerProvider.GetCurrentClassLogger();

    private readonly IDateTimeProvider _dateTimeProvider;
    private readonly IRepository<Order> _ordersRepository;
    private readonly IRepository<Room> _roomRepository;
    private readonly IMapper _mapper;
    private readonly IEmailService _emailService;
    private readonly IRoomService _roomService;

    public BookingService(
        IDateTimeProvider dateTimeProvider,
        IRepository<Order> ordersRepository,
        IRepository<Room> roomRepository,
        IMapper mapper,
        IEmailService emailService,
        IRoomService roomService)
    {
        _dateTimeProvider = dateTimeProvider;
        _ordersRepository = ordersRepository;
        _roomRepository = roomRepository;
        _mapper = mapper;
        _emailService = emailService;
        _roomService = roomService;
    }

    public virtual async Task<ResultDto<BookingDto?>> CreateBookingAsync(BookingFlatDto booking)
    {
        await Locks.RoomLock.WaitAsync(booking.RoomId);
        try
        {
            var room = await _roomRepository.GetByIdAsync(booking.RoomId);
            if (room is null)
                return ResultDto<BookingDto?>.ErrorResult(null);

            var checkInUtc = booking.CheckInUtc;
            var checkOutUtc = booking.CheckOutUtc;

            var isValidCheckInCheckOut = CheckInCheckOutUtil.ValidateDateAndAdjustTime(_dateTimeProvider, ref checkInUtc, ref checkOutUtc);
            if (!isValidCheckInCheckOut)
                return ResultDto<BookingDto?>.ErrorResult(null, "Broneeringu algus või lõpp ei ole valiidsed.");

            var isRoomAlreadyBooked = await _roomService.IsRoomBookedAsync(room.Id, checkInUtc, checkOutUtc);
            if (isRoomAlreadyBooked)
                return ResultDto<BookingDto?>.ErrorResult(null, "Tuba on juba broneeritud.");

            var bookingNumber = await GetUniqueBookingNumberAsync();

            var order = new Order
            {
                OrderNumber = bookingNumber,
                CheckInUtc = checkInUtc,
                CheckOutUtc = checkOutUtc,
                Email = booking.Email,
                FullName = booking.FullName,
                NationalIdentificationNumber = booking.NationalIdentificationNumber,
                Status = OrderStatus.Confirmed,
                Bookings = new List<OrderBooking>
                {
                    new()
                    {
                        RoomId = room.Id,
                        RoomPriceEur = room.PriceEur
                    }
                }
            };

            await _ordersRepository.InsertAsync(order);

            await SendBookingEmailAsync(order);

            s_logger.Info($"Created new order: ID:{order.Id}, OrderNumber:{order.OrderNumber}.");

            return new ResultDto<BookingDto?>(new BookingDto
            {
                Number = order.OrderNumber,
                FullName = booking.FullName
            });
        }
        finally
        {
            Locks.RoomLock.Release(booking.RoomId);
        }
    }

    public virtual async Task<ResultDto<BookingDto?>> UpdateBookingAsync(BookingFlatDto booking)
    {
        await Locks.RoomLock.WaitAsync(booking.RoomId);
        try
        {
            var order = await _ordersRepository.Table
                .Include(x => x.Bookings)
                .ThenInclude(x => x.Room)
                .ThenInclude(x => x.RoomType)
                .FirstOrDefaultAsync(x => x.OrderNumber == booking.BookingCode);

            if (order is null)
                return ResultDto<BookingDto?>.ErrorResult(null);

            var room = await _roomRepository.GetByIdAsync(booking.RoomId);
            if (room is null)
                return ResultDto<BookingDto?>.ErrorResult(null);

            var checkInUtc = booking.CheckInUtc.Date == order.CheckInUtc.Date ? order.CheckInUtc : booking.CheckInUtc.Date;
            var checkOutUtc = booking.CheckOutUtc.Date == order.CheckOutUtc.Date ? order.CheckOutUtc : booking.CheckOutUtc.Date;

            var isValidCheckInCheckOut =
                CheckInCheckOutUtil.ValidateDateAndAdjustTime(_dateTimeProvider, ref checkInUtc, ref checkOutUtc, skipBusinessLogic: true);
            if (!isValidCheckInCheckOut)
                return ResultDto<BookingDto?>.ErrorResult(null, "Broneeringu algus või lõpp ei ole valiidsed.");

            var isRoomAlreadyBooked = await _roomService.IsRoomBookedAsync(room.Id, checkInUtc, checkOutUtc, booking.BookingCode);
            if (isRoomAlreadyBooked)
                return ResultDto<BookingDto?>.ErrorResult(null, "Tuba on juba broneeritud.");

            order.FullName = booking.FullName;
            order.NationalIdentificationNumber = booking.NationalIdentificationNumber;
            order.Email = booking.Email;
            order.CheckInUtc = checkInUtc;
            order.CheckOutUtc = checkOutUtc;
            order.Bookings = new List<OrderBooking>
            {
                new()
                {
                    RoomId = room.Id,
                    RoomPriceEur = room.PriceEur,
                    Room = room
                }
            };

            await _ordersRepository.UpdateAsync(order);

            s_logger.Info($"Modified order: ID:{order.Id}, OrderNumber:{order.OrderNumber}.");

            return new ResultDto<BookingDto?>(_mapper.Map<BookingDto>(order));
        }
        finally
        {
            Locks.RoomLock.Release(booking.RoomId);
        }
    }

    public virtual async Task<IEnumerable<BookingDto>?> GetAllActiveBookings(DateTime? from, DateTime? to)
    {
        var utcNow = _dateTimeProvider.UtcNow;

        var bookingsQuery = _ordersRepository.Table
            .Include(x => x.Bookings)
            .ThenInclude(x => x.Room)
            .ThenInclude(x => x.RoomType)
            .Where(o => o.Status == OrderStatus.Confirmed  && o.CheckOutUtc > utcNow);

        if (from.HasValue)
            bookingsQuery = bookingsQuery.Where(o => o.CheckInUtc >= from.Value);
        if (to.HasValue)
            bookingsQuery = bookingsQuery.Where(o => o.CheckOutUtc <= to.Value.EndOfDay());

        var bookings = await bookingsQuery.ToListAsync();

        return _mapper.Map<List<Order>, IEnumerable<BookingDto>>(bookings);
    }

    public virtual async Task<BookingDto?> GetBookingByCodeAsync(string code)
    {
        if (string.IsNullOrEmpty(code))
            return null;

        var ongoingBookingOrder = await GetBookingAsync(new() { BookingCode = code }, OrderStatus.Confirmed, false);

        if (ongoingBookingOrder is null)
            return null;

        return _mapper.Map<BookingDto>(ongoingBookingOrder);
    }

    public virtual async Task<BookingDto?> GetBookingByCodeAndNameAsync(BookingQueryDto bookingQuery)
    {
        if (string.IsNullOrEmpty(bookingQuery.BookingCode) || string.IsNullOrEmpty(bookingQuery.FullName))
            return null;

        var ongoingBookingOrder = await GetBookingAsync(bookingQuery, OrderStatus.Confirmed, false);

        return _mapper.Map<BookingDto>(ongoingBookingOrder);
    }

    public virtual async Task<ResultDto?> CancelBookingAsync(BookingQueryDto bookingQuery, bool skipMinimumDayValidation = false)
    {
        var ongoingBookingOrder = await GetBookingAsync(bookingQuery);

        if (ongoingBookingOrder is null)
            return null;

        var utcNow = _dateTimeProvider.UtcNow;
        var isOrderCancelAllowed = skipMinimumDayValidation ||
            utcNow <= ongoingBookingOrder.CheckInUtc.AddDays(-ValidationConstants.MIN_ORDER_CANCELLATION_DAYS);

        if (!isOrderCancelAllowed)
            return ResultDto.ErrorResult("Lubatud broneeringu tühistamise aeg on möödunud.");

        ongoingBookingOrder.Status = OrderStatus.Cancelled;

        await _ordersRepository.UpdateAsync(ongoingBookingOrder);

        s_logger.Info($"Cancelled order: ID:{ongoingBookingOrder.Id}, OrderNumber:{ongoingBookingOrder.OrderNumber}.");

        return ResultDto.SuccessResult("Broneering tühistati.");
    }

    public virtual async Task<string> GetUniqueBookingNumberAsync()
    {
        var utcNow = _dateTimeProvider.UtcNow;
        string bookingNumber;

        async Task<bool> IsBookingNumberExistsLocal(string number)
        {
            return await _ordersRepository.Table
                .AnyAsync(x => x.OrderNumber == number &&
                               x.CheckOutUtc >= utcNow &&
                               x.Status == OrderStatus.Confirmed);
        }

        do
            bookingNumber = GetGeneratedBookingNumberWrapper();
        while (await IsBookingNumberExistsLocal(bookingNumber));

        return bookingNumber;
    }

    public virtual string GetGeneratedBookingNumberWrapper()
    {
        return BookingNumberUtil.GenerateBookingNumber();
    }

    /// <summary>
    /// Returns booking by booking code or full name.
    /// </summary>
    /// <param name="bookingQuery">Booking information.</param>
    /// <param name="status">Booking status filter.</param>
    /// <param name="includeHistorical">Set to True to search both, historical and active orders.</param>
    /// <returns>Order if found, otherwise null.</returns>
    private async Task<Order?> GetBookingAsync(BookingQueryDto bookingQuery, OrderStatus status = OrderStatus.Confirmed, bool includeHistorical = false)
    {
        if (string.IsNullOrEmpty(bookingQuery.BookingCode) && string.IsNullOrEmpty(bookingQuery.FullName))
            return null;

        var query = _ordersRepository.Table;

        if (!string.IsNullOrEmpty(bookingQuery.BookingCode))
            query = query.Where(o => o.OrderNumber == bookingQuery.BookingCode.Trim());

        if (!string.IsNullOrEmpty(bookingQuery.FullName))
            query = query.Where(o => o.FullName == bookingQuery.FullName.Trim());

        var utcNow = _dateTimeProvider.UtcNow;

        query = query.Where(o => o.Status == status && (includeHistorical || o.CheckOutUtc > utcNow));

        var booking = await query
            .Include(x => x.Bookings)
            .ThenInclude(x => x.Room)
            .ThenInclude(x => x.RoomType)
            .SingleOrDefaultAsync();

        return booking;
    }

    /// <summary>
    /// Sends booking confirmation email to the customer.
    /// </summary>
    /// <param name="order">Booking order.</param>
    /// <returns>True if email was sent successfully, otherwise False.</returns>
    private async Task<bool> SendBookingEmailAsync(Order order)
    {
        var email = new EmailDto
        {
            ToAddresses = new[] { order.Email },
            Subject = "Hotelli broneering",
            Body = $"Teie broneering on kinnitatud. Broneeringu number: {order.OrderNumber}."
        };

        try
        {
            await _emailService.SendEmailAsync(email);
        }
        catch (Exception ex)
        {
            s_logger.Error(ex, $"Could not send booking code email to customer. Order: ID:{order.Id}, OrderNumber:{order.OrderNumber}.");
            return false;
        }

        return true;
    }
}
