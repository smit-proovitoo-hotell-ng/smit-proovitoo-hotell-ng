using Common.Dtos.Email;

namespace BLL.Services;

public interface IEmailService
{
    /// <summary>
    /// Send email.
    /// </summary>
    /// <param name="email">Email to be sent.</param>
    /// <param name="logEmails">Set to true to log sent emails.</param>
    /// <returns>True if email was successfully sent.</returns>
    Task<bool> SendEmailAsync(EmailDto email, bool logEmails = false);

    /// <summary>
    /// Send emails.
    /// </summary>
    /// <param name="emails">Emails to be sent.</param>
    /// <param name="logEmails">Set to true to log sent emails.</param>
    /// <returns>True if all emails were successfully sent.</returns>
    Task<bool> SendEmailAsync(List<EmailDto> emails, bool logEmails = false);
}