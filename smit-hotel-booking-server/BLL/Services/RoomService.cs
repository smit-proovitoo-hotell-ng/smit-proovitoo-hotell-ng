using AutoMapper;
using BLL.Database.RepositoryBase;
using BLL.Entities;
using Common.Dtos.Result;
using Common.Dtos.Room;
using Common.Enums;
using Common.Providers;
using Common.Utilities;
using Microsoft.EntityFrameworkCore;

namespace BLL.Services;

public class RoomService : IRoomService
{
    private readonly IMapper _mapper;
    private readonly IDateTimeProvider _dateTimeProvider;
    private readonly IRepository<OrderBooking> _orderBookingRepository;
    private readonly IRepository<Room> _roomRepository;

    public RoomService(
        IMapper mapper,
        IDateTimeProvider dateTimeProvider,
        IRepository<OrderBooking> orderBookingRepository,
        IRepository<Room> roomRepository)
    {
        _mapper = mapper;
        _dateTimeProvider = dateTimeProvider;
        _orderBookingRepository = orderBookingRepository;
        _roomRepository = roomRepository;
    }

    public virtual async Task<bool> IsRoomBookedAsync(int roomId, DateTime checkInUtc, DateTime checkOutUtc, string? skipOverBookingCode = null)
    {
        var isRoomAlreadyBooked = await _orderBookingRepository.Table
            .AnyAsync(x =>
                x.RoomId == roomId
                && x.Order.Status == OrderStatus.Confirmed
                && x.Order.CheckInUtc <= checkOutUtc
                && x.Order.CheckOutUtc >= checkInUtc
                && (skipOverBookingCode == null || x.Order.OrderNumber != skipOverBookingCode));

        return isRoomAlreadyBooked;
    }

    public virtual async Task<ResultDto<List<RoomDto>>> GetAvailableRoomsAsync(DateTime checkInUtc, DateTime checkOutUtc, string? skipOverBookingCode = null)
    {
        var isValid = CheckInCheckOutUtil.ValidateDateAndAdjustTime(_dateTimeProvider, ref checkInUtc, ref checkOutUtc, skipBusinessLogic: skipOverBookingCode != null);

        if (!isValid) 
            return ResultDto<List<RoomDto>>.ErrorResult(new List<RoomDto>(), "Vigane broneerimise periood.")!;

        var availableRooms = await _roomRepository.Table
            .Include(r => r.RoomType)
            .Where(r =>
                !r.OrderBookings.Any(ob => ob.Order.Status == OrderStatus.Confirmed
                                         && ob.Order.CheckOutUtc >= checkInUtc
                                         && ob.Order.CheckInUtc <= checkOutUtc
                                         && (skipOverBookingCode == null || ob.Order.OrderNumber != skipOverBookingCode)))
            .ToListAsync();

        return new ResultDto<List<RoomDto>>(_mapper.Map<List<RoomDto>>(availableRooms));
    }

    public virtual async Task<ResultDto<List<RoomDto>>> GetAllRoomsAsync()
    {
        var utcNow = _dateTimeProvider.UtcNow;

        var rooms = await _roomRepository.Table
            .Include(r => r.OrderBookings.Where(ob => ob.Order.Status == OrderStatus.Confirmed && ob.Order.CheckOutUtc > utcNow))
            .ThenInclude(ob => ob.Order)
            .ToListAsync();

        return new ResultDto<List<RoomDto>>(_mapper.Map<List<RoomDto>>(rooms));
    }
}