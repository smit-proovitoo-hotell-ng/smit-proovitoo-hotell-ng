using Common.Dtos.Result;
using Common.Dtos.Room;

namespace BLL.Services;

public interface IRoomService
{
    /// <summary>
    /// Validates whether a room is booked for a given time period.
    /// </summary>
    /// <param name="roomId">Room ID.</param>
    /// <param name="checkInUtc">Check-in datetime in UTC.</param>
    /// <param name="checkOutUtc">Check-out datetime in UTC.</param>
    /// <param name="skipOverBookingCode">Optional, a booking code to be skipped from the check.</param>
    /// <returns>True if room is booked, otherwise false.</returns>
    Task<bool> IsRoomBookedAsync(int roomId, DateTime checkInUtc, DateTime checkOutUtc, string? skipOverBookingCode = null);
    /// <summary>
    /// Get all available rooms for a given time period.
    /// </summary>
    /// <param name="checkInUtc">Check-in datetime in UTC.</param>
    /// <param name="checkOutUtc">Check-out datetime in UTC.</param>
    /// <param name="skipOverBookingCode">Optional, a booking code to be skipped from the check.</param>
    /// <returns>All available rooms for given time period.</returns>
    Task<ResultDto<List<RoomDto>>> GetAvailableRoomsAsync(DateTime checkInUtc, DateTime checkOutUtc, string? skipOverBookingCode = null);
    /// <summary>
    /// Get every room.
    /// </summary>
    /// <returns>Every room, booked or otherwise</returns>
    Task<ResultDto<List<RoomDto>>> GetAllRoomsAsync();
}