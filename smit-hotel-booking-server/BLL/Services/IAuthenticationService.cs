using Common.Dtos.Authentication;
using Common.Dtos.Result;
using Common.Dtos.User;

namespace BLL.Services;

public interface IAuthenticationService
{
    /// <summary>
    /// Verifies the user's password.
    /// </summary>
    /// <param name="loginInfo">User login info.</param>
    /// <returns>Verification result. User information is included in case of successful validation.</returns>
    Task<ResultDto<UserDto?>> VerifyUserPassword(LoginDto loginInfo);
}