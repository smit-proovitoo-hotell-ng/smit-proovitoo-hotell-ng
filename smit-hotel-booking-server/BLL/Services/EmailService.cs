using Common.Constants;
using Common.Dtos.Email;
using Common.Loggers;
using Common.Providers;
using Common.Utilities;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;

namespace BLL.Services;

public class EmailService : IEmailService
{
    private static readonly IAppLogger s_logger = AppLoggerProvider.GetCurrentClassLogger();

    public virtual async Task<bool> SendEmailAsync(EmailDto email, bool logEmails = false)
    {
        return await SendEmailAsync(new List<EmailDto>() { email }, logEmails);
    }

    public virtual async Task<bool> SendEmailAsync(List<EmailDto> emails, bool logEmails = false)
    {
        var isSuccess = true;
        var smtpConfig = GetSmtpConfig();

        try
        {
            var mailkitMessages = PrepareEmails(emails, smtpConfig);

            using var client = new SmtpClient();

            await client.ConnectAsync(
                smtpConfig.ServerAddress,
                smtpConfig.ServerPort!.Value,
                SecureSocketOptions.Auto);

            await client.AuthenticateAsync(smtpConfig.Username, smtpConfig.Password);

            foreach (var message in mailkitMessages)
            {
                try
                {
                    var res = await client.SendAsync(message.MimeMessage);

                    if (logEmails)
                        s_logger.Info("Email successfully sent out.", message.EmailDto);
                }
                catch (Exception ex)
                {
                    s_logger.Error(ex, "Failed to send out an email.", logEmails ? message.EmailDto : null!);
                    isSuccess = false;
                }
            }

            await client.DisconnectAsync(true);
        }
        catch (Exception ex)
        {
            s_logger.Error(ex, $"Exception during {nameof(SendEmailAsync)}.");
            isSuccess = false;
        }

        return isSuccess;
    }

    private IEnumerable<MailkitMessageDto> PrepareEmails(IEnumerable<EmailDto> emails, SmtpConfig smtpConfig)
    {
        var mimeMessages = new List<MailkitMessageDto>();

        foreach (var email in emails)
        {
            if (email.ToAddresses is null || !email.ToAddresses.Any())
                continue;

            var fromAddresses =  new[] { new MailboxAddress(smtpConfig.DefaultFromAddressName, smtpConfig.DefaultFromAddress) };
            var toAddresses = email.ToAddresses.Select(e => new MailboxAddress(e,e));

            var mimeMessage = new MimeMessage();
            mimeMessage.From.AddRange(fromAddresses);
            mimeMessage.To.AddRange(toAddresses);
            mimeMessage.Subject = email.Subject;
            mimeMessage.Body = new TextPart("plain") { Text = email.Body };

            mimeMessages.Add(new MailkitMessageDto(email, mimeMessage));
        }

        return mimeMessages;
    }

    private SmtpConfig GetSmtpConfig()
    {
        return new SmtpConfig
        {
            ServerAddress = AppData.Configuration[CK.SMTP_SERVER_ADDRESS],
            ServerPort = int.Parse(AppData.Configuration[CK.SMTP_SERVER_PORT]),
            DefaultFromAddress = AppData.Configuration[CK.SMTP_DEFAULT_FROM_ADDRESS],
            DefaultFromAddressName = AppData.Configuration[CK.SMTP_DEFAULT_FROM_ADDRESS_NAME],
            Username = AppData.Configuration[CK.SMTP_USERNAME],
            Password = AppData.Configuration[CK.SMTP_PASSWORD]
        };
    }

    private class MailkitMessageDto
    {
        public MimeMessage MimeMessage { get; }
        public EmailDto EmailDto { get; }

        public MailkitMessageDto(EmailDto emailDto, MimeMessage mimeMessage)
        {
            EmailDto = emailDto;
            MimeMessage = mimeMessage;
        }
    }

    private class SmtpConfig
    {
        public string? ServerAddress { get; init; }
        public int? ServerPort { get; init; }
        public string? DefaultFromAddressName { get; init; }
        public string? DefaultFromAddress { get; init; }
        public string? Username { get; set; }
        public string? Password { get; set; }
    }
}