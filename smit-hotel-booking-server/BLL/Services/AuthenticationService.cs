using AutoMapper;
using BLL.Database.RepositoryBase;
using BLL.Entities;
using Common.Dtos.Authentication;
using Common.Dtos.Result;
using Common.Dtos.User;
using Common.Utilities;
using Microsoft.EntityFrameworkCore;

namespace BLL.Services;

public class AuthenticationService : IAuthenticationService
{
    private readonly IMapper _mapper;
    private readonly IRepository<User> _userRepository;

    public AuthenticationService(IMapper mapper, IRepository<User> userRepository)
    {
        _mapper = mapper;
        _userRepository = userRepository;
    }

    public virtual async Task<ResultDto<UserDto?>> VerifyUserPassword(LoginDto loginInfo)
    {
        var errorResult = ResultDto<UserDto>.ErrorResult(null, "Kasutaja parooli valideerimine ebaõnnestus, palun kontrollige väljad ja proovige uuesti..");

        if (loginInfo.Username is null || loginInfo.Password is null)
            return errorResult;

        var user = await _userRepository.Table
            .Include(u => u.Roles)
            .SingleOrDefaultAsync(u => u.EmailAddress == loginInfo.Username);

        if (user is null)
            return errorResult;

        var isValidHash = HashUtil.VerifyHash(loginInfo.Password, user.PasswordHash);

        return isValidHash
            ? ResultDto<UserDto>.SuccessResult(_mapper.Map<UserDto>(user))
            : errorResult;
    }
}