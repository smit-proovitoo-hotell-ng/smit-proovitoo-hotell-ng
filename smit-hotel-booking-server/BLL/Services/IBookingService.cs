using Common.Dtos.Booking;
using Common.Dtos.Result;
using Common.Utilities;

namespace BLL.Services;

public interface IBookingService
{
    Task<BookingDto?> GetBookingByCodeAndNameAsync(BookingQueryDto bookingQuery);
    /// <summary>
    /// Get booking by booking code.
    /// </summary>
    /// <param name="code">Booking code.</param>
    /// <returns>Booking if it is found, otherwise null.</returns>
    Task<BookingDto?> GetBookingByCodeAsync(string code);
    /// <summary>
    /// Cancel a booking.
    /// </summary>
    /// <param name="bookingQuery">Booking information.</param>
    /// <param name="skipMinimumDayValidation">Set to true to skip validating the minimum cancellation period.</param>
    /// <returns>Cancellation result.</returns>
    Task<ResultDto?> CancelBookingAsync(BookingQueryDto bookingQuery, bool skipMinimumDayValidation = false);
    Task<ResultDto<BookingDto?>> CreateBookingAsync(BookingFlatDto booking);
    Task<ResultDto<BookingDto?>> UpdateBookingAsync(BookingFlatDto booking);
    Task<IEnumerable<BookingDto>?> GetAllActiveBookings(DateTime? from, DateTime? to);
    /// <summary>
    /// Get a unique booking number. Booking number is unique if no active booking has the same number.
    /// </summary>
    /// <returns>A unique booking number.</returns>
    Task<string> GetUniqueBookingNumberAsync();
    /// <summary>
    /// Internally wraps <see cref="BookingNumberUtil"/>.
    /// </summary>
    /// <returns>Generated booking number.</returns>
    string GetGeneratedBookingNumberWrapper();
}