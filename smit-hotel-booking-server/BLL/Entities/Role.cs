using BLL.Entities.Base;

namespace BLL.Entities;

public class Role : TrackedEntity
{
    public string SystemName { get; set; } = default!;

    #region Relationships

    public virtual List<User> Users { get; set; } = new();

    #endregion
}