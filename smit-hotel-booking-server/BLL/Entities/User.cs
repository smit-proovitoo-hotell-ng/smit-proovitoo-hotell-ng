using BLL.Entities.Base;

namespace BLL.Entities;

public class User : TrackedEntity
{
    public string EmailAddress { get; set; } = default!;
    public string? PasswordHash { get; set; }
    public string? FullName { get; set; }

    #region Relationships

    public virtual List<Role> Roles { get; set; } = new();

    #endregion
}