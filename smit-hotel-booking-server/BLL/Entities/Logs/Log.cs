using BLL.Entities.Base;

namespace BLL.Entities.Logs;

/// <summary>
/// System log entity.
/// </summary>
public class Log : BaseEntity
{
    public DateTime CreatedAtUtc { get; set; }

    public string? Level { get; set; }

    public string? LoggerName { get; set; }

    public string? Message { get; set; }

    public string? Exception { get; set; }

    public int? UserId { get; set; }

    public string? ObjectContextJson { get; set; }
}
