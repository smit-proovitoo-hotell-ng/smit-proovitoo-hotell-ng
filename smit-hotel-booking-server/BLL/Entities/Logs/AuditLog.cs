using BLL.Entities.Base;

namespace BLL.Entities.Logs;

public class AuditLog : BaseEntity
{
    public string TableName { get; set; } = default!;

    public int EntityId { get; set; }

    public string? OldValueJson { get; set; }

    public string? NewValueJson { get; set; }

    public string Action { get; set; } = default!;

    public DateTime CreatedAtUtc { get; set; }

    public int? UserId { get; set; }
}