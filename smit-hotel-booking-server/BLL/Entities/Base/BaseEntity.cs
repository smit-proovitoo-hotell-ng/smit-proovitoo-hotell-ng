namespace BLL.Entities.Base;

/// <summary>
/// Base entity class for all domain models.
/// </summary>
public abstract class BaseEntity
{
    /// <summary>
    /// Primary key.
    /// </summary>
    public int Id { get; set; }
}