namespace BLL.Entities.Base;

/// <summary>
/// Entity whose creation and modification actions are tracked.
/// </summary>
public abstract class TrackedEntity : BaseEntity
{
    public DateTime CreatedAtUtc { get; set; }
    public DateTime? ModifiedAtUtc { get; set; }

    #region Relationships

    public int? CreatedById { get; set; }
    public virtual User? CreatedBy { get; set; }

    public virtual int? ModifiedById { get; set; }
    public virtual User? ModifiedBy { get; set; }

    #endregion
}