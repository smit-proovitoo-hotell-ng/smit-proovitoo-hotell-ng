using BLL.Entities.Base;

namespace BLL.Entities;

public class Hotel : TrackedEntity
{
    public string Name { get; set; } = default!;

    #region Relationships

    public virtual List<Room> Rooms { get; set; } = new();

    #endregion
}