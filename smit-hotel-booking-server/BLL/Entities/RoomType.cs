using BLL.Entities.Base;

namespace BLL.Entities;

public class RoomType : TrackedEntity
{
    public string Name { get; set; } = default!;
    public int MaxCapacity { get; set; }

    #region Relationships

    public virtual List<Room> Rooms { get; set; } = new();

    #endregion
}