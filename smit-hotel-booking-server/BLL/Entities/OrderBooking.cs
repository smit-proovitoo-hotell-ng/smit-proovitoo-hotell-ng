using System.ComponentModel.DataAnnotations.Schema;
using BLL.Entities.Base;

namespace BLL.Entities;

public class OrderBooking : TrackedEntity
{
    public decimal RoomPriceEur { get; set; }

    #region Relationships

    public int RoomId { get; set; }
    public virtual Room Room { get; set; } = default!;

    public int OrderId { get; set; }
    public virtual Order Order { get; set; } = default!;

    #endregion
}