using BLL.Entities.Base;
using Common.Enums;
using Microsoft.EntityFrameworkCore;

namespace BLL.Entities;

[Index(nameof(OrderNumber), IsUnique = true)]
[Index(nameof(Status))]
public class Order : TrackedEntity
{
    public OrderStatus Status { get; set; }
    public DateTime CheckInUtc { get; set; }
    public DateTime CheckOutUtc { get; set; }
    public string OrderNumber { get; set; } = default!;

    public string Email { get; set; } = default!;
    public string FullName { get; set; } = default!;
    public string NationalIdentificationNumber { get; set; } = default!;

    #region Relationships

    public virtual List<OrderBooking> Bookings { get; set; } = new();

    #endregion
}