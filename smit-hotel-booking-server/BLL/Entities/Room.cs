using System.ComponentModel.DataAnnotations.Schema;
using BLL.Entities.Base;

namespace BLL.Entities;

public class Room : TrackedEntity
{
    public string RoomNumber { get; set; } = default!;
    public decimal PriceEur { get; set; }
    public string? Description { get; set; }

    #region Relationships

    public int HotelId { get; set; }
    public virtual Hotel Hotel { get; set; } = default!;

    public int RoomTypeId { get; set; }

    public virtual RoomType RoomType { get; set; } = default!;

    public virtual List<OrderBooking> OrderBookings { get; set; } = new();

    #endregion
}