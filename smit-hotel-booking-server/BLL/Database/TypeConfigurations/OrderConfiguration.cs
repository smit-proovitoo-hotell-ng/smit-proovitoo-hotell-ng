using BLL.Entities;
using Common.Enums;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace BLL.Database.TypeConfigurations;

public class OrderConfiguration : IEntityTypeConfiguration<Order>
{
    public void Configure(EntityTypeBuilder<Order> builder)
    {
        builder.Property(o => o.Status)
            .IsRequired();

        builder.Property(o => o.OrderNumber)
            .IsRequired();

        builder.Property(o => o.CheckInUtc)
            .IsRequired();

        builder.Property(o => o.CheckOutUtc)
            .IsRequired();

        builder.HasMany(o => o.Bookings)
            .WithOne(ob => ob.Order)
            .HasForeignKey(ob => ob.OrderId)
            .OnDelete(DeleteBehavior.Cascade);

        builder
            .Property(e => e.Status)
            .HasConversion(
                v => v.ToString().ToUpper(),
                v => (OrderStatus)Enum.Parse(typeof(OrderStatus), v, true));
    }
}