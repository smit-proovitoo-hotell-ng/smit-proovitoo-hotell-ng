using BLL.Entities.Logs;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace BLL.Database.TypeConfigurations;

internal class AuditLogConfiguration : IEntityTypeConfiguration<AuditLog>
{
    public void Configure(EntityTypeBuilder<AuditLog> builder)
    {
        builder.Property(l => l.CreatedAtUtc)
            .IsRequired();

        builder.Property(l => l.TableName)
            .IsRequired()
            .HasMaxLength(128);

        builder.Property(l => l.OldValueJson)
            .HasMaxLength(int.MaxValue);

        builder.Property(l => l.NewValueJson)
            .HasMaxLength(int.MaxValue);
    }
}