using BLL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace BLL.Database.TypeConfigurations;

public class OrderBookingConfiguration : IEntityTypeConfiguration<OrderBooking>
{
    public void Configure(EntityTypeBuilder<OrderBooking> builder)
    {
        builder.Property(ob => ob.RoomPriceEur)
            .HasColumnType("decimal(10,2)")
            .IsRequired();

        builder.HasOne(ob => ob.Room)
            .WithMany(ob => ob.OrderBookings)
            .HasForeignKey(ob => ob.RoomId);

        builder.HasOne(ob => ob.Order)
            .WithMany(o => o.Bookings)
            .HasForeignKey(ob => ob.OrderId);
    }
}