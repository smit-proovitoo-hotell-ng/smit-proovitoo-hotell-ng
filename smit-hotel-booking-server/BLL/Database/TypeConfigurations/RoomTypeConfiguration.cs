using BLL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace BLL.Database.TypeConfigurations;

internal class RoomTypeConfiguration : IEntityTypeConfiguration<RoomType>
{
    public void Configure(EntityTypeBuilder<RoomType> builder)
    {
        builder.Property(r => r.Name)
            .IsRequired();

        builder.Property(r => r.MaxCapacity)
            .IsRequired();

        builder.HasMany(h => h.Rooms)
            .WithOne(r => r.RoomType)
            .HasForeignKey(r => r.RoomTypeId);
    }
}