using BLL.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace BLL.Database.TypeConfigurations;

public class RoomConfiguration : IEntityTypeConfiguration<Room>
{
    public void Configure(EntityTypeBuilder<Room> builder)
    {
        builder.Property(r => r.PriceEur)
            .HasColumnType("decimal(10,2)")
            .IsRequired();

        builder.Property(r => r.RoomNumber)
            .IsRequired();

        builder.HasMany(o => o.OrderBookings)
            .WithOne(ob => ob.Room)
            .HasForeignKey(ob => ob.RoomId);

        builder.HasOne(r => r.RoomType).WithMany(rt => rt.Rooms);

        builder.Navigation(r => r.RoomType).AutoInclude();
    }
}