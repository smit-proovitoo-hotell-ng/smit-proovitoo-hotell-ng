using BLL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BLL.Database.TypeConfigurations;

internal class RoleConfiguration : IEntityTypeConfiguration<Role>
{
    public void Configure(EntityTypeBuilder<Role> builder)
    {
        builder.Property(r => r.SystemName)
            .HasMaxLength(100);

        builder
            .HasIndex(r => r.SystemName)
            .HasDatabaseName("RoleSystemNameIndex")
            .IsUnique();
    }
}