using BLL.Entities.Logs;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;

namespace BLL.Database.TypeConfigurations;

internal class LogConfiguration : IEntityTypeConfiguration<Log>
{
    public void Configure(EntityTypeBuilder<Log> builder)
    {
        builder.Property(l => l.CreatedAtUtc)
            .IsRequired();

        builder.Property(l => l.Exception)
            .HasMaxLength(int.MaxValue);

        builder.Property(l => l.ObjectContextJson)
            .HasMaxLength(int.MaxValue);
    }
}