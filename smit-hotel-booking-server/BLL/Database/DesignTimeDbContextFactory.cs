using Common.Constants;
using Common.Utilities;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BLL.Database;

/// <summary>
/// Design-time factory for creating an <see cref="AppDbContext"/> instance. This will only be used by command line tools such as EF Core's migrations.
/// </summary>
internal class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
{
    public AppDbContext CreateDbContext(string[] args)
    {
        var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
        optionsBuilder.UseSqlServer(AppData.Configuration.GetConnectionString(CK.CONNECTION_STRING_NAME));

        return new AppDbContext(null!, null!, optionsBuilder.Options);
    }
}