using BLL.Entities;
using BLL.Entities.Base;
using BLL.Entities.Logs;
using Common.Providers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Newtonsoft.Json;

namespace BLL.Database;

public class AppDbContext : DbContext, IAppDbContext
{
    private readonly IDateTimeProvider _dateTimeProvider;
    private readonly IActiveUserProvider _activeUserProvider;

    public AppDbContext(IDateTimeProvider dateTimeProvider, IActiveUserProvider activeUserProvider,
        DbContextOptions options) : base(options)
    {
        _dateTimeProvider = dateTimeProvider;
        _activeUserProvider = activeUserProvider;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        DataContextModelBuilder.OnModelCreating(modelBuilder);
    }

    public async Task RunMigrationsAsync(CancellationToken cancellationToken = default)
    {
        await Database.MigrateAsync(cancellationToken);
    }

    public new DbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity => base.Set<TEntity>();

    public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
    {
        SetTrackedFields();

        var auditLogs = GetAuditEntities();

        var result = await base.SaveChangesAsync(cancellationToken);

        await InsertAuditLogs(auditLogs);

        return result;
    }

    private void SetTrackedFields()
    {
        var utcNow = _dateTimeProvider.UtcNow;
        var loggedInUserId = _activeUserProvider.ActiveUser?.UserId;

        foreach (var entityEntry in ChangeTracker.Entries<TrackedEntity>())
        {
            if (entityEntry.State is EntityState.Detached or EntityState.Unchanged)
                continue;

            var entityState = entityEntry.State;
            var trackedEntity = entityEntry.Entity;

            switch (entityState)
            {
                case EntityState.Added:
                {
                    if (trackedEntity.CreatedAtUtc == default)
                        trackedEntity.CreatedAtUtc = utcNow;

                    trackedEntity.CreatedById = loggedInUserId;
                    break;
                }
                case EntityState.Modified:
                {
                    trackedEntity.ModifiedAtUtc = utcNow;
                    trackedEntity.ModifiedById = loggedInUserId;
                    break;
                }
            }
        }
    }

    private List<(AuditLog, EntityEntry, EntityState, Dictionary<string, object?>? newValue)> GetAuditEntities()
    {
        var utcNow = _dateTimeProvider.UtcNow;
        var loggedInUserId = _activeUserProvider.ActiveUser?.UserId;

        var result = new List<(AuditLog, EntityEntry, EntityState, Dictionary<string, object?>? newValue)>();
        foreach (var e in ChangeTracker.Entries())
        {
            if (e.State is EntityState.Detached or EntityState.Unchanged)
                continue;

            var action = e.State switch
            {
                EntityState.Added => "Insert",
                EntityState.Modified => "Update",
                EntityState.Deleted => "Delete",
                _ => throw new ArgumentOutOfRangeException()
            };

            var table = e.Entity.GetType().Name;

            var oldValue = new Dictionary<string, object?>();
            var newValue = new Dictionary<string, object?>();

            foreach (var propertyEntry in e.Properties)
            {
                switch (e.State)
                {
                    case EntityState.Added:
                        newValue[propertyEntry.Metadata.Name] = propertyEntry.CurrentValue;
                        break;
                    case EntityState.Modified:
                        oldValue[propertyEntry.Metadata.Name] = propertyEntry.OriginalValue;
                        newValue[propertyEntry.Metadata.Name] = propertyEntry.CurrentValue;
                        break;
                    case EntityState.Deleted:
                        oldValue[propertyEntry.Metadata.Name] = propertyEntry.OriginalValue;
                        break;
                }
            }

            switch (e.State)
            {
                case EntityState.Added:
                    oldValue = null;
                    break;
                case EntityState.Deleted:
                    newValue = null;
                    break;
            }

            result.Add((new AuditLog
            {
                CreatedAtUtc = utcNow,
                UserId = loggedInUserId,
                TableName = table,
                Action = action,
                OldValueJson = JsonConvert.SerializeObject(oldValue,
                    new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore })
            }, e, e.State, newValue));
        }

        return result;
    }
    
    private async Task InsertAuditLogs(List<(AuditLog, EntityEntry, EntityState, Dictionary<string, object?>?)> auditLogs)
    {
        foreach (var (auditLog, e, state, newValue) in auditLogs)
        {
            if (e.Entity is BaseEntity entity)
            {
                auditLog.EntityId = entity.Id;

                if (state is EntityState.Added or EntityState.Modified && newValue != null)
                {
                    newValue["Id"] = entity.Id;
                    auditLog.NewValueJson = JsonConvert.SerializeObject(newValue,
                        new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                }
            }

            AuditLogs.Add(auditLog);
        }
        
        await base.SaveChangesAsync();
    }

    #region DbSets

    public DbSet<Log> Logs { get; set; } = default!;
    public DbSet<AuditLog> AuditLogs { get; set; } = default!;
    public DbSet<User> Users { get; set; } = default!;
    public DbSet<Role> Roles { get; set; } = default!;
    public DbSet<UserRole> UserRoles { get; set; } = default!;
    public DbSet<Hotel> Hotels { get; set; } = default!;
    public DbSet<RoomType> RoomTypes { get; set; } = default!;
    public DbSet<Room> Rooms { get; set; } = default!;
    public DbSet<Order> Orders { get; set; } = default!;
    public DbSet<OrderBooking> OrderBookings { get; set; } = default!;

    #endregion
}