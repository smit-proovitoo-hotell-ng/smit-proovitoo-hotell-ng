using BLL.Entities.Base;
using Microsoft.EntityFrameworkCore;

namespace BLL.Database.RepositoryBase;

public class Repository<T> : IRepository<T> where T : BaseEntity
{
    private readonly IAppDbContext _context;
    private DbSet<T>? _entities;

    public Repository(IAppDbContext context)
    {
        _context = context;
    }

    public async Task<T?> GetByIdAsync(int id)
    {
        return await Entities.SingleOrDefaultAsync(x => x.Id == id);
    }

    public async Task InsertAsync(T entity)
    {
        if (entity == null)
            throw new ArgumentNullException(nameof(entity));

        await Entities.AddAsync(entity);

        await _context.SaveChangesAsync();
    }

    public async Task UpdateAsync(T entity)
    {
        if (entity == null)
            throw new ArgumentNullException(nameof(entity));

        await _context.SaveChangesAsync();
    }

    public async Task DeleteAsync(T entity)
    {
        if (entity == null)
            throw new ArgumentNullException(nameof(entity));

        Entities.Remove(entity);

        await _context.SaveChangesAsync();
    }

    public IQueryable<T> Table => Entities;

    protected virtual DbSet<T> Entities => _entities ??= _context.Set<T>();
}