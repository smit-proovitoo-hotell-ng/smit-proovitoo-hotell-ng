using BLL.Entities.Base;

namespace BLL.Database.RepositoryBase;

/// <summary>
/// Repository
/// </summary>
public interface IRepository<T> where T : BaseEntity
{
    /// <summary>
    /// Get entity by identifier.
    /// </summary>
    /// <param name="id">Identifier</param>
    /// <returns>Entity</returns>
    Task<T?> GetByIdAsync(int id);

    /// <summary>
    /// Insert entity.
    /// </summary>
    /// <param name="entity">Entity</param>
    Task InsertAsync(T entity);

    /// <summary>
    /// Update entity.
    /// </summary>
    /// <param name="entity">Entity</param>
    Task UpdateAsync(T entity);

    /// <summary>
    /// Delete entity.
    /// </summary>
    /// <param name="entity">Entity</param>
    Task DeleteAsync(T entity);

    /// <summary>
    /// Gets a table.
    /// </summary>
    IQueryable<T> Table { get; }
}