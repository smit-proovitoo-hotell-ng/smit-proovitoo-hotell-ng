﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BLL.Database.Migrations
{
    /// <inheritdoc />
    public partial class Alter_AuditLogs_EntityId : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EntityIdJson",
                table: "AuditLogs");

            migrationBuilder.AddColumn<int>(
                name: "EntityId",
                table: "AuditLogs",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EntityId",
                table: "AuditLogs");

            migrationBuilder.AddColumn<string>(
                name: "EntityIdJson",
                table: "AuditLogs",
                type: "nvarchar(max)",
                maxLength: 2147483647,
                nullable: false,
                defaultValue: "");
        }
    }
}
