﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BLL.Database.Migrations
{
    /// <inheritdoc />
    public partial class Remove_customer_role : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 2);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "CreatedAtUtc", "CreatedById", "ModifiedAtUtc", "ModifiedById", "SystemName" },
                values: new object[] { 2, new DateTime(2020, 11, 21, 20, 17, 53, 0, DateTimeKind.Unspecified), null, null, null, "Customer" });
        }
    }
}
