﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BLL.Database.Migrations
{
    /// <inheritdoc />
    public partial class OrderBooking_RoomId1 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderBookings_Rooms_RoomId1",
                table: "OrderBookings");

            migrationBuilder.DropIndex(
                name: "IX_OrderBookings_RoomId1",
                table: "OrderBookings");

            migrationBuilder.DropColumn(
                name: "RoomId1",
                table: "OrderBookings");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RoomId1",
                table: "OrderBookings",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OrderBookings_RoomId1",
                table: "OrderBookings",
                column: "RoomId1");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderBookings_Rooms_RoomId1",
                table: "OrderBookings",
                column: "RoomId1",
                principalTable: "Rooms",
                principalColumn: "Id");
        }
    }
}
