﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BLL.Database.Migrations
{
    /// <inheritdoc />
    public partial class OrderBooking_Cascade : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderBookings_Orders_OrderId",
                table: "OrderBookings");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderBookings_Orders_OrderId",
                table: "OrderBookings",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderBookings_Orders_OrderId",
                table: "OrderBookings");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderBookings_Orders_OrderId",
                table: "OrderBookings",
                column: "OrderId",
                principalTable: "Orders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
