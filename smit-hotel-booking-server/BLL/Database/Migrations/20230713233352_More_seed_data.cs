using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace BLL.Database.Migrations
{
    /// <inheritdoc />
    public partial class More_seed_data : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Rooms");
            migrationBuilder.Sql("DELETE FROM RoomTypes");
            migrationBuilder.Sql("DELETE FROM Hotels");
            migrationBuilder.Sql("DELETE FROM UserRoles");
            migrationBuilder.Sql("DELETE FROM Users");
            migrationBuilder.Sql("DELETE FROM Roles");

            migrationBuilder.InsertData(
                table: "Hotels",
                columns: new[] { "Id", "CreatedAtUtc", "CreatedById", "ModifiedAtUtc", "ModifiedById", "Name" },
                values: new object[] { 1, new DateTime(2020, 11, 21, 20, 17, 53, 0, DateTimeKind.Unspecified), null, null, null, "Hotell" });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "CreatedAtUtc", "CreatedById", "ModifiedAtUtc", "ModifiedById", "SystemName" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 11, 21, 20, 17, 53, 0, DateTimeKind.Unspecified), null, null, null, "Admin" },
                    { 2, new DateTime(2020, 11, 21, 20, 17, 53, 0, DateTimeKind.Unspecified), null, null, null, "Customer" }
                });

            migrationBuilder.InsertData(
                table: "RoomTypes",
                columns: new[] { "Id", "CreatedAtUtc", "CreatedById", "MaxCapacity", "ModifiedAtUtc", "ModifiedById", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 11, 21, 20, 17, 53, 0, DateTimeKind.Unspecified), null, 1, null, null, "Ühekohaline" },
                    { 2, new DateTime(2020, 11, 21, 20, 17, 53, 0, DateTimeKind.Unspecified), null, 2, null, null, "Kahekohaline" },
                    { 3, new DateTime(2020, 11, 21, 20, 17, 53, 0, DateTimeKind.Unspecified), null, 3, null, null, "Kolmekohaline" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "CreatedAtUtc", "CreatedById", "EmailAddress", "FirstName", "LastName", "ModifiedAtUtc", "ModifiedById", "NationalIdentificationNumber", "PasswordHash" },
                values: new object[] { 1, new DateTime(2020, 11, 21, 20, 17, 53, 0, DateTimeKind.Unspecified), null, "test-admin@netgroup.com", "Test", "Admin", null, null, null, "AQAAAAEAAYagAAAAEHA8hcAua28vz5A+Qkqs1KNaRiJJiaKWIf7yWBOazfBRbBuKM56O24NelpuCkGXVJQ==" });

            migrationBuilder.InsertData(
                table: "Rooms",
                columns: new[] { "Id", "CreatedAtUtc", "CreatedById", "Description", "HotelId", "ModifiedAtUtc", "ModifiedById", "PriceEur", "RoomTypeId" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 11, 21, 20, 17, 53, 0, DateTimeKind.Unspecified), 1, "Rõduga tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 150m, 1 },
                    { 2, new DateTime(2022, 1, 14, 22, 5, 21, 0, DateTimeKind.Unspecified), 1, "Diivanvoodiga tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 150m, 2 },
                    { 3, new DateTime(2022, 7, 3, 13, 41, 26, 0, DateTimeKind.Unspecified), 1, "Eraldi köögiga", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 150m, 3 },
                    { 4, new DateTime(2020, 8, 31, 23, 49, 59, 0, DateTimeKind.Unspecified), 1, "Luksuslik sviit", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 450m, 3 },
                    { 5, new DateTime(2022, 6, 22, 11, 24, 12, 0, DateTimeKind.Unspecified), 1, "Diivanvoodiga tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 300m, 1 },
                    { 6, new DateTime(2022, 6, 29, 11, 37, 21, 0, DateTimeKind.Unspecified), 1, "Lemmiklooma sõbralik", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 50m, 1 },
                    { 7, new DateTime(2021, 9, 5, 20, 10, 5, 0, DateTimeKind.Unspecified), 1, "Avar tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 50m, 1 },
                    { 8, new DateTime(2021, 4, 11, 9, 54, 28, 0, DateTimeKind.Unspecified), 1, "Merevaatega tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 150m, 2 },
                    { 9, new DateTime(2021, 5, 24, 14, 13, 7, 0, DateTimeKind.Unspecified), 1, "Merevaatega tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 150m, 3 },
                    { 10, new DateTime(2021, 10, 29, 1, 9, 45, 0, DateTimeKind.Unspecified), 1, "Diivanvoodiga tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 300m, 2 },
                    { 11, new DateTime(2021, 2, 28, 6, 48, 26, 0, DateTimeKind.Unspecified), 1, "Rõduga tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 150m, 1 },
                    { 12, new DateTime(2020, 8, 1, 1, 45, 24, 0, DateTimeKind.Unspecified), 1, "Merevaatega tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 450m, 3 },
                    { 13, new DateTime(2021, 3, 28, 10, 19, 33, 0, DateTimeKind.Unspecified), 1, "Luksuslik sviit", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 225m, 3 },
                    { 14, new DateTime(2021, 3, 19, 18, 41, 55, 0, DateTimeKind.Unspecified), 1, "Eraldi köögiga", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 150m, 1 },
                    { 15, new DateTime(2021, 11, 18, 6, 3, 8, 0, DateTimeKind.Unspecified), 1, "Lemmiklooma sõbralik", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 300m, 2 },
                    { 16, new DateTime(2021, 11, 29, 19, 30, 31, 0, DateTimeKind.Unspecified), 1, "Eraldi köögiga", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 225m, 3 },
                    { 17, new DateTime(2021, 11, 20, 23, 13, 19, 0, DateTimeKind.Unspecified), 1, "Lemmiklooma sõbralik", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 100m, 2 },
                    { 18, new DateTime(2021, 6, 26, 8, 37, 29, 0, DateTimeKind.Unspecified), 1, "Luksuslik sviit", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 75m, 1 },
                    { 19, new DateTime(2022, 6, 13, 7, 13, 17, 0, DateTimeKind.Unspecified), 1, "Rõduga tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 225m, 3 },
                    { 20, new DateTime(2021, 4, 12, 18, 54, 0, 0, DateTimeKind.Unspecified), 1, "Avar tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 100m, 2 },
                    { 21, new DateTime(2021, 7, 15, 3, 44, 32, 0, DateTimeKind.Unspecified), 1, "Lemmiklooma sõbralik", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 75m, 1 },
                    { 22, new DateTime(2022, 4, 5, 11, 32, 54, 0, DateTimeKind.Unspecified), 1, "Rõduga tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 100m, 2 },
                    { 23, new DateTime(2021, 6, 27, 5, 36, 34, 0, DateTimeKind.Unspecified), 1, "Diivanvoodiga tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 75m, 1 },
                    { 24, new DateTime(2021, 9, 22, 10, 35, 27, 0, DateTimeKind.Unspecified), 1, "Diivanvoodiga tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 225m, 3 },
                    { 25, new DateTime(2020, 10, 5, 22, 38, 19, 0, DateTimeKind.Unspecified), 1, "Rõduga tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 100m, 2 },
                    { 26, new DateTime(2020, 7, 28, 23, 58, 59, 0, DateTimeKind.Unspecified), 1, "Luksuslik sviit", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 150m, 1 },
                    { 27, new DateTime(2022, 5, 4, 22, 57, 53, 0, DateTimeKind.Unspecified), 1, "Rõduga tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 225m, 3 },
                    { 28, new DateTime(2021, 2, 9, 9, 45, 54, 0, DateTimeKind.Unspecified), 1, "Rõduga tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 300m, 2 },
                    { 29, new DateTime(2022, 5, 16, 15, 31, 1, 0, DateTimeKind.Unspecified), 1, "Merevaatega tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 150m, 3 },
                    { 30, new DateTime(2022, 4, 22, 12, 57, 11, 0, DateTimeKind.Unspecified), 1, "Eraldi köögiga", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 50m, 1 },
                    { 31, new DateTime(2021, 12, 16, 6, 15, 38, 0, DateTimeKind.Unspecified), 1, "Eraldi köögiga", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 150m, 2 },
                    { 32, new DateTime(2020, 10, 3, 12, 38, 43, 0, DateTimeKind.Unspecified), 1, "Avar tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 225m, 3 },
                    { 33, new DateTime(2020, 8, 29, 7, 17, 32, 0, DateTimeKind.Unspecified), 1, "Luksuslik sviit", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 50m, 1 },
                    { 34, new DateTime(2021, 1, 10, 11, 58, 59, 0, DateTimeKind.Unspecified), 1, "Lemmiklooma sõbralik", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 450m, 3 },
                    { 35, new DateTime(2021, 3, 12, 7, 53, 37, 0, DateTimeKind.Unspecified), 1, "Lemmiklooma sõbralik", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 150m, 2 },
                    { 36, new DateTime(2022, 2, 9, 2, 33, 19, 0, DateTimeKind.Unspecified), 1, "Avar tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 75m, 1 },
                    { 37, new DateTime(2021, 7, 11, 23, 9, 3, 0, DateTimeKind.Unspecified), 1, "Eraldi köögiga", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 225m, 3 },
                    { 38, new DateTime(2021, 11, 6, 16, 6, 25, 0, DateTimeKind.Unspecified), 1, "Avar tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 150m, 1 },
                    { 39, new DateTime(2022, 7, 3, 19, 55, 54, 0, DateTimeKind.Unspecified), 1, "Merevaatega tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 100m, 2 },
                    { 40, new DateTime(2021, 3, 13, 6, 11, 46, 0, DateTimeKind.Unspecified), 1, "Eraldi köögiga", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 100m, 2 },
                    { 41, new DateTime(2022, 3, 22, 4, 41, 59, 0, DateTimeKind.Unspecified), 1, "Rõduga tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 50m, 1 },
                    { 42, new DateTime(2021, 3, 20, 18, 16, 56, 0, DateTimeKind.Unspecified), 1, "Rõduga tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 450m, 3 },
                    { 43, new DateTime(2021, 11, 26, 7, 38, 26, 0, DateTimeKind.Unspecified), 1, "Lemmiklooma sõbralik", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 300m, 2 },
                    { 44, new DateTime(2021, 9, 20, 1, 59, 24, 0, DateTimeKind.Unspecified), 1, "Lemmiklooma sõbralik", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 150m, 1 },
                    { 45, new DateTime(2021, 11, 20, 2, 29, 22, 0, DateTimeKind.Unspecified), 1, "Lemmiklooma sõbralik", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 150m, 3 },
                    { 46, new DateTime(2021, 11, 26, 13, 44, 58, 0, DateTimeKind.Unspecified), 1, "Diivanvoodiga tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 150m, 3 },
                    { 47, new DateTime(2022, 7, 2, 12, 52, 18, 0, DateTimeKind.Unspecified), 1, "Avar tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 100m, 2 },
                    { 48, new DateTime(2022, 6, 25, 20, 4, 41, 0, DateTimeKind.Unspecified), 1, "Rõduga tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 150m, 1 },
                    { 49, new DateTime(2021, 6, 29, 5, 12, 22, 0, DateTimeKind.Unspecified), 1, "Merevaatega tuba", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 300m, 2 },
                    { 50, new DateTime(2020, 7, 16, 20, 41, 18, 0, DateTimeKind.Unspecified), 1, "Eraldi köögiga", 1, new DateTime(2023, 7, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), 1, 50m, 1 }
                });

            migrationBuilder.InsertData(
                table: "UserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { 1, 1 });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 28);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 29);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 30);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 31);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 32);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 33);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 34);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 35);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 36);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 37);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 38);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 39);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 40);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 41);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 42);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 43);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 44);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 45);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 46);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 47);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 48);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 49);

            migrationBuilder.DeleteData(
                table: "Rooms",
                keyColumn: "Id",
                keyValue: 50);

            migrationBuilder.DeleteData(
                table: "UserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "Hotels",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "RoomTypes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
