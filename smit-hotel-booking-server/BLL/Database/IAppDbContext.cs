using BLL.Entities.Base;
using Microsoft.EntityFrameworkCore;

namespace BLL.Database;

public interface IAppDbContext : IDisposable
{
    /// <summary>
    /// Get <see cref="DbSet{TEntity}"/>.
    /// </summary>
    /// <typeparam name="TEntity">Entity type.</typeparam>
    /// <returns>A <see cref="DbSet{TEntity}"/>.</returns>
    DbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity;

    /// <summary>
    /// Saves all changes made in this context to the database.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>Number of state entries written to the database.</returns>
    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

    /// <summary>
    /// Execute database migrations.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns></returns>
    Task RunMigrationsAsync(CancellationToken cancellationToken = default);
}