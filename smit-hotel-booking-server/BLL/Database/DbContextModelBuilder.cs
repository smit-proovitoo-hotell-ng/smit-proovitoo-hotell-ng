using BLL.Database.TypeConfigurations;
using BLL.Entities;
using BLL.Entities.Base;
using Common.Constants;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BLL.Database;

/// <summary>
/// Data model builder for common and entity specific configurations.
/// </summary>
internal static class DataContextModelBuilder
{
    public static void OnModelCreating(ModelBuilder modelBuilder)
    {
        SetCommonConfigurations(modelBuilder);

        #region EntityConfigurations

        modelBuilder.ApplyConfiguration(new AuditLogConfiguration());
        modelBuilder.ApplyConfiguration(new LogConfiguration());
        modelBuilder.ApplyConfiguration(new RoleConfiguration());
        modelBuilder.ApplyConfiguration(new UserConfiguration());
        modelBuilder.ApplyConfiguration(new HotelConfiguration());
        modelBuilder.ApplyConfiguration(new RoomTypeConfiguration());
        modelBuilder.ApplyConfiguration(new RoomConfiguration());
        modelBuilder.ApplyConfiguration(new OrderConfiguration());
        modelBuilder.ApplyConfiguration(new OrderBookingConfiguration());

        #endregion

        GenerateSeedData(modelBuilder);
    }

    private static void SetCommonConfigurations(ModelBuilder modelBuilder)
    {
        var entityTypes = modelBuilder.Model.GetEntityTypes().ToList();

        foreach (var entityType in entityTypes)
        {
            SetBaseClassConfigurations(modelBuilder, entityType);

            foreach (var property in entityType.GetProperties())
            {
                // add a default max length of 1000 to all strings
                if (property.ClrType == typeof(string))
                {
                    property.SetMaxLength(1000);
                }

                foreach (var foreignKey in entityType.GetForeignKeys())
                {
                    // disable cascade delete
                    foreignKey.DeleteBehavior = DeleteBehavior.Restrict;
                }
            }
        }
    }

    private static void SetBaseClassConfigurations(ModelBuilder modelBuilder, IMutableEntityType entityType)
    {
        var entityClrType = entityType.ClrType;

        if (typeof(BaseEntity).IsAssignableFrom(entityClrType))
        {
            modelBuilder.Entity(entityClrType, entity => { entity.HasKey(nameof(BaseEntity.Id)); });
        }

        if (typeof(TrackedEntity).IsAssignableFrom(entityType.ClrType))
        {
            modelBuilder.Entity(entityType.ClrType, entity =>
            {
                entity.Property(nameof(TrackedEntity.CreatedAtUtc))
                    .IsRequired();

                entity.HasOne(nameof(TrackedEntity.CreatedBy))
                    .WithMany()
                    .HasForeignKey(nameof(TrackedEntity.CreatedById))
                    .IsRequired(false);

                entity.HasOne(nameof(TrackedEntity.ModifiedBy))
                    .WithMany()
                    .HasForeignKey(nameof(TrackedEntity.ModifiedById))
                    .IsRequired(false);
            });
        }
    }

    private static void GenerateSeedData(ModelBuilder modelBuilder)
    {
        #region Roles, Administrators

        var adminRole = new Role
        {
            Id = 1,
            SystemName = UserConstants.Roles.ADMIN,
            CreatedAtUtc = DateTime.Parse("2020-11-21 20:17:53")
        };

        modelBuilder.Entity<Role>().HasData(
            adminRole
        );

        var adminUser = new User
        {
            Id = 1,
            EmailAddress = "test-admin@netgroup.com",
            PasswordHash = "AQAAAAEAAYagAAAAEHA8hcAua28vz5A+Qkqs1KNaRiJJiaKWIf7yWBOazfBRbBuKM56O24NelpuCkGXVJQ==",
            FullName = "Test Admin",
            CreatedAtUtc = DateTime.Parse("2020-11-21 20:17:53"),
        };
        modelBuilder.Entity<User>().HasData(adminUser);

        modelBuilder.Entity<UserRole>().HasData(new UserRole { UserId = adminUser.Id, RoleId = adminRole.Id });

        #endregion

        #region Hotels, RoomTypes, Rooms 

        var hotel = new Hotel
        {
            Id = 1,
            Name = "Hotell",
            CreatedAtUtc = DateTime.Parse("2020-11-21 20:17:53")
        };
        modelBuilder.Entity<Hotel>().HasData(hotel);

        var roomType1 = new RoomType
        {
            Id = 1,
            Name = "Väike (ühekohaline)",
            MaxCapacity = 1,
            CreatedAtUtc = DateTime.Parse("2020-11-21 20:17:53")
        };
        var roomType2 = new RoomType
        {
            Id = 2,
            Name = "Keskmine (kahekohaline)",
            MaxCapacity = 2,
            CreatedAtUtc = DateTime.Parse("2020-11-21 20:17:53")
        };
        var roomType3 = new RoomType
        {
            Id = 3,
            Name = "Suur (kolmekohaline)",
            MaxCapacity = 3,
            CreatedAtUtc = DateTime.Parse("2020-11-21 20:17:53")
        };
        modelBuilder.Entity<RoomType>().HasData(roomType1, roomType2, roomType3);

        modelBuilder.Entity<Room>().HasData(
            new Room { Id = 1,  RoomNumber = "1", Description = "Rõduga tuba", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 150, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2020-11-21 20:17:53"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 2,  RoomNumber = "2", Description = "Diivanvoodiga tuba", HotelId = hotel.Id, RoomTypeId = roomType2.Id, PriceEur = 150, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2022-01-14 22:05:21"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 3,  RoomNumber = "3", Description = "Eraldi köögiga", HotelId = hotel.Id, RoomTypeId = roomType3.Id, PriceEur = 150, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2022-07-03 13:41:26"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 4,  RoomNumber = "4", Description = "Luksuslik sviit", HotelId = hotel.Id, RoomTypeId = roomType3.Id, PriceEur = 450, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2020-08-31 23:49:59"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 5,  RoomNumber = "5", Description = "Diivanvoodiga tuba", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 300, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2022-06-22 11:24:12"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 6,  RoomNumber = "6", Description = "Lemmiklooma sõbralik", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 50, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2022-06-29 11:37:21"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 7,  RoomNumber = "7", Description = "Avar tuba", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 50, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-09-05 20:10:05"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 8,  RoomNumber = "8", Description = "Merevaatega tuba", HotelId = hotel.Id, RoomTypeId = roomType2.Id, PriceEur = 150, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-04-11 09:54:28"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 9,  RoomNumber = "9", Description = "Merevaatega tuba", HotelId = hotel.Id, RoomTypeId = roomType3.Id, PriceEur = 150, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-05-24 14:13:07"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 10, RoomNumber = "10", Description = "Diivanvoodiga tuba", HotelId = hotel.Id, RoomTypeId = roomType2.Id, PriceEur = 300, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-10-29 01:09:45"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 11, RoomNumber = "11", Description = "Rõduga tuba", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 150, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-02-28 06:48:26"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 12, RoomNumber = "12", Description = "Merevaatega tuba", HotelId = hotel.Id, RoomTypeId = roomType3.Id, PriceEur = 450, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2020-08-01 01:45:24"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 13, RoomNumber = "13", Description = "Luksuslik sviit", HotelId = hotel.Id, RoomTypeId = roomType3.Id, PriceEur = 225, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-03-28 10:19:33"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 14, RoomNumber = "14", Description = "Eraldi köögiga", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 150, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-03-19 18:41:55"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 15, RoomNumber = "15", Description = "Lemmiklooma sõbralik", HotelId = hotel.Id, RoomTypeId = roomType2.Id, PriceEur = 300, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-11-18 06:03:08"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 16, RoomNumber = "16", Description = "Eraldi köögiga", HotelId = hotel.Id, RoomTypeId = roomType3.Id, PriceEur = 225, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-11-29 19:30:31"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 17, RoomNumber = "17", Description = "Lemmiklooma sõbralik", HotelId = hotel.Id, RoomTypeId = roomType2.Id, PriceEur = 100, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-11-20 23:13:19"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 18, RoomNumber = "18", Description = "Luksuslik sviit", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 75, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-06-26 08:37:29"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 19, RoomNumber = "19", Description = "Rõduga tuba", HotelId = hotel.Id, RoomTypeId = roomType3.Id, PriceEur = 225, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2022-06-13 07:13:17"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 20, RoomNumber = "20", Description = "Avar tuba", HotelId = hotel.Id, RoomTypeId = roomType2.Id, PriceEur = 100, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-04-12 18:54:00"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 21, RoomNumber = "21", Description = "Lemmiklooma sõbralik", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 75, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-07-15 03:44:32"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 22, RoomNumber = "22", Description = "Rõduga tuba", HotelId = hotel.Id, RoomTypeId = roomType2.Id, PriceEur = 100, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2022-04-05 11:32:54"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 23, RoomNumber = "23", Description = "Diivanvoodiga tuba", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 75, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-06-27 05:36:34"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 24, RoomNumber = "24", Description = "Diivanvoodiga tuba", HotelId = hotel.Id, RoomTypeId = roomType3.Id, PriceEur = 225, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-09-22 10:35:27"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 25, RoomNumber = "25", Description = "Rõduga tuba", HotelId = hotel.Id, RoomTypeId = roomType2.Id, PriceEur = 100, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2020-10-05 22:38:19"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 26, RoomNumber = "26", Description = "Luksuslik sviit", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 150, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2020-07-28 23:58:59"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 27, RoomNumber = "27", Description = "Rõduga tuba", HotelId = hotel.Id, RoomTypeId = roomType3.Id, PriceEur = 225, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2022-05-04 22:57:53"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 28, RoomNumber = "28", Description = "Rõduga tuba", HotelId = hotel.Id, RoomTypeId = roomType2.Id, PriceEur = 300, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-02-09 09:45:54"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 29, RoomNumber = "29", Description = "Merevaatega tuba", HotelId = hotel.Id, RoomTypeId = roomType3.Id, PriceEur = 150, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2022-05-16 15:31:01"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 30, RoomNumber = "30", Description = "Eraldi köögiga", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 50, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2022-04-22 12:57:11"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 31, RoomNumber = "31", Description = "Eraldi köögiga", HotelId = hotel.Id, RoomTypeId = roomType2.Id, PriceEur = 150, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-12-16 06:15:38"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 32, RoomNumber = "32", Description = "Avar tuba", HotelId = hotel.Id, RoomTypeId = roomType3.Id, PriceEur = 225, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2020-10-03 12:38:43"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 33, RoomNumber = "33", Description = "Luksuslik sviit", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 50, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2020-08-29 07:17:32"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 34, RoomNumber = "34", Description = "Lemmiklooma sõbralik", HotelId = hotel.Id, RoomTypeId = roomType3.Id, PriceEur = 450, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-01-10 11:58:59"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 35, RoomNumber = "35", Description = "Lemmiklooma sõbralik", HotelId = hotel.Id, RoomTypeId = roomType2.Id, PriceEur = 150, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-03-12 07:53:37"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 36, RoomNumber = "36", Description = "Avar tuba", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 75, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2022-02-09 02:33:19"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 37, RoomNumber = "37", Description = "Eraldi köögiga", HotelId = hotel.Id, RoomTypeId = roomType3.Id, PriceEur = 225, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-07-11 23:09:03"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 38, RoomNumber = "38", Description = "Avar tuba", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 150, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-11-06 16:06:25"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 39, RoomNumber = "39", Description = "Merevaatega tuba", HotelId = hotel.Id, RoomTypeId = roomType2.Id, PriceEur = 100, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2022-07-03 19:55:54"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 40, RoomNumber = "40", Description = "Eraldi köögiga", HotelId = hotel.Id, RoomTypeId = roomType2.Id, PriceEur = 100, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-03-13 06:11:46"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 41, RoomNumber = "41", Description = "Rõduga tuba", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 50, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2022-03-22 04:41:59"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 42, RoomNumber = "42", Description = "Rõduga tuba", HotelId = hotel.Id, RoomTypeId = roomType3.Id, PriceEur = 450, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-03-20 18:16:56"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 43, RoomNumber = "43", Description = "Lemmiklooma sõbralik", HotelId = hotel.Id, RoomTypeId = roomType2.Id, PriceEur = 300, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-11-26 07:38:26"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 44, RoomNumber = "44", Description = "Lemmiklooma sõbralik", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 150, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-09-20 01:59:24"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 45, RoomNumber = "45", Description = "Lemmiklooma sõbralik", HotelId = hotel.Id, RoomTypeId = roomType3.Id, PriceEur = 150, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-11-20 02:29:22"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 46, RoomNumber = "46", Description = "Diivanvoodiga tuba", HotelId = hotel.Id, RoomTypeId = roomType3.Id, PriceEur = 150, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-11-26 13:44:58"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 47, RoomNumber = "47", Description = "Avar tuba", HotelId = hotel.Id, RoomTypeId = roomType2.Id, PriceEur = 100, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2022-07-02 12:52:18"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 48, RoomNumber = "48", Description = "Rõduga tuba", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 150, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2022-06-25 20:04:41"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 49, RoomNumber = "49", Description = "Merevaatega tuba", HotelId = hotel.Id, RoomTypeId = roomType2.Id, PriceEur = 300, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2021-06-29 05:12:22"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00") },
            new Room { Id = 50, RoomNumber = "50", Description = "Eraldi köögiga", HotelId = hotel.Id, RoomTypeId = roomType1.Id, PriceEur = 50, CreatedById = adminUser.Id, CreatedAtUtc =  DateTime.Parse("2020-07-16 20:41:18"), ModifiedById = adminUser.Id, ModifiedAtUtc =DateTime.Parse( "2023-07-13 00:00:00")}
        );

        #endregion
    }
}