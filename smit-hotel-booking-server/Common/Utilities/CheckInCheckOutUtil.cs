using Common.Constants;
using Common.Providers;

namespace Common.Utilities;

public static class CheckInCheckOutUtil
{
    /// <summary>
    /// Validates and adjusts the check in and check out datetimes.
    /// </summary>
    /// <returns>True, if validation has passed with the given check in and check out values being adjusted.</returns>
    public static bool ValidateDateAndAdjustTime(IDateTimeProvider dateTimeProvider, ref DateTime checkInUtc, ref DateTime checkOutUtc,
        int defaultCheckInHour = BookingConstants.CHECK_IN_HOUR, int defaultCheckOutHour = BookingConstants.CHECK_OUT_HOUR, bool skipBusinessLogic = false)
    {
        var nowUtc = dateTimeProvider.UtcNow;

        if (!skipBusinessLogic)
        {
            if (checkInUtc.Date < nowUtc.Date) // check in must be in the future or today
                return false;

            if (checkOutUtc.Date <= nowUtc.Date) // check out must be in the future
                return false;
        }

        if (checkOutUtc <= checkInUtc) // check out must be after check in, minimum stay is a day
            return false;

        AdjustTimes(nowUtc, ref checkInUtc, ref checkOutUtc, defaultCheckInHour, defaultCheckOutHour);

        return true;
    }

    private static void AdjustTimes(DateTime utcNow, ref DateTime checkIn, ref DateTime checkOut, int defaultCheckInHour, int defaultCheckOutHour)
    {
        var checkInTime = new TimeSpan(defaultCheckInHour, 0, 0);
        var checkOutTime = new TimeSpan(defaultCheckOutHour, 0, 0);

        if (utcNow.TimeOfDay <= checkInTime)
            checkIn = checkIn.Date + checkInTime;
        else
            checkIn = checkIn.Date + TimeSpan.FromHours(utcNow.Hour); // check in is today, but after the default check in hour, adjust to current hour.

        checkOut = checkOut.Date + checkOutTime;
    }
}
