using System.Collections.Concurrent;

namespace Common.Utilities;

public class SemaphoreLock<T> where T : notnull
{
    private readonly ConcurrentDictionary<T, SemaphoreSlim> _lockDictionary = new ();
    private readonly int initialCount;
    public SemaphoreLock(int initialCount = 1)
    {
        this.initialCount = initialCount;
    }

    public void Wait(T lockKey)
    {
        _lockDictionary.GetOrAdd(lockKey, new SemaphoreSlim(initialCount)).Wait();
    }
 
    public async Task WaitAsync(T lockKey)
    {
        await _lockDictionary.GetOrAdd(lockKey, new SemaphoreSlim(initialCount)).WaitAsync();
    }
 
    public void Release(T lockKey)
    {
        if (_lockDictionary.TryGetValue(lockKey, out var semaphore))
            semaphore.Release();
    }
}