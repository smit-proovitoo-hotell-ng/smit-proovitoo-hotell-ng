using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Common.Dtos.Authentication;
using Common.Dtos.User;
using Common.Providers;
using Microsoft.IdentityModel.Tokens;

namespace Common.Utilities;

public class JwtUtil
{
    /// <summary>
    /// Creates JWT token.
    /// </summary>
    /// <param name="dateTimeProvider">DateTime provider.</param>
    /// <param name="jwtInfo">JWT information.</param>
    /// <param name="userId">User ID</param>
    /// <param name="expiresSeconds">Token expiration in seconds.</param>
    /// <returns>JWT token</returns>
    public static string CreateJwtToken(IDateTimeProvider dateTimeProvider, JwtDto jwtInfo, UserDto userInfo,
        int expiresSeconds)
    {
        var utcNow = dateTimeProvider.UtcNow;

        var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtInfo.SecretKey!));
        var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha512);

        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Issuer = jwtInfo.Issuer,
            Audience = jwtInfo.Audience,
            Claims = new Dictionary<string, object>
            {
                {ClaimTypes.Sid, userInfo.UserId},
                {ClaimTypes.Name, userInfo.Email},
            },
            NotBefore = utcNow,
            Expires = utcNow.AddSeconds(expiresSeconds),
            SigningCredentials = signingCredentials
        };

        userInfo.Roles?.ForEach(r => tokenDescriptor.Claims.Add(ClaimTypes.Role, r.SystemName));

        var tokenHandler = new JwtSecurityTokenHandler();
        var securityToken = tokenHandler.CreateToken(tokenDescriptor);

        return tokenHandler.WriteToken(securityToken);
    }

    /// <summary>
    /// Validates JWT token based on given key.
    /// </summary>
    /// <param name="token">Token to be validated.</param>
    /// <param name="jwtInfo">JWT information.</param>
    /// <returns>JwtSecurityToken when valid. Invalid case returns null.</returns>
    public static JwtSecurityToken? ValidateJwtToken(string token, JwtDto jwtInfo)
    {
        var tokenHandler = new JwtSecurityTokenHandler();
        try
        {
            tokenHandler.ValidateToken(token, GetTokenValidationParameters(jwtInfo),
                out SecurityToken validatedToken);

            return (JwtSecurityToken)validatedToken;
        }
        catch
        {
            return null;
        }
    }

    /// <summary>
    /// Return common TokenValidationParameters for application
    /// </summary>
    /// <param name="jwtInfo">JWT information.</param>
    public static TokenValidationParameters GetTokenValidationParameters(JwtDto jwtInfo)
    {
        return new TokenValidationParameters
        {
            ValidateAudience = true,
            ValidateIssuer = true,
            ValidateIssuerSigningKey = true,
            ValidateLifetime = true,
            ValidAudience = jwtInfo.Audience,
            ValidIssuer = jwtInfo.Issuer,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtInfo.SecretKey)),
            ClockSkew = TimeSpan.Zero
        };
    }
}