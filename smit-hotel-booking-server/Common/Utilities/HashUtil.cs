using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;

namespace Common.Utilities;

public static class HashUtil
{
    private static readonly PasswordHasherOptions s_passwordHasherOptions = new()
    {
        CompatibilityMode = PasswordHasherCompatibilityMode.IdentityV3,
        IterationCount = 100000
    }; 
    private static readonly IPasswordHasher<object> s_passwordHasher = new PasswordHasher<object>(Options.Create(s_passwordHasherOptions));

    /// <summary>
    /// Returns a salted and hashed string using the OpenBSD bcrypt scheme.
    /// </summary>
    public static string Hash(string input)
    {
        return s_passwordHasher.HashPassword(null, input);

    }

    /// <summary>
    /// Verifies that the hash of the given text matches the provided hash.
    /// </summary>
    /// <param name="text">Plain text to be validated.</param>
    /// <param name="correctHash">Hash used for validation.</param>
    /// <returns>True if given text matches provided hash.</returns>
    public static bool VerifyHash(string? text, string? correctHash)
    {
        if (string.IsNullOrEmpty(text) || string.IsNullOrEmpty(correctHash))
            return false;

        var verificationResult = s_passwordHasher.VerifyHashedPassword(null, correctHash, text);
        return verificationResult == PasswordVerificationResult.Success;
    }
}
