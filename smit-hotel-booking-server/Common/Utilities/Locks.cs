namespace Common.Utilities;

public static class Locks
{
    public static readonly SemaphoreLock<int> RoomLock = new();
}