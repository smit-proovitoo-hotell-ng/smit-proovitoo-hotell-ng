using System.Text.RegularExpressions;

namespace Common.Utilities;

public static class BookingNumberUtil
{
    private const string BOOKING_NUMBER_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private const int BOOKING_NUMBER_LENGTH = 7;

    private static readonly Random s_random = new();

    private static readonly Regex s_bookingNumberRegex =
        new(@"^#[A-Z0-9]{" + BOOKING_NUMBER_LENGTH + "}$", RegexOptions.Compiled);

    public static string GenerateBookingNumber()
    {
        var randomChars = new string(Enumerable.Repeat(BOOKING_NUMBER_CHARS, BOOKING_NUMBER_LENGTH)
            .Select(s => s[s_random.Next(s.Length)]).ToArray());

        return $"#{randomChars}";
    }

    public static bool IsValidBookingNumberFormat(string? bookingNumber)
    {
        if (string.IsNullOrEmpty(bookingNumber))
            return false;

        return s_bookingNumberRegex.IsMatch(bookingNumber.ToUpperInvariant());
    }
}