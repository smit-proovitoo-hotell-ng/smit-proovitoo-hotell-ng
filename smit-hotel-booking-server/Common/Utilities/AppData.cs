using Microsoft.Extensions.Configuration;

namespace Common.Utilities;

public static class AppData
{
    public static IConfiguration Configuration = default!;
}