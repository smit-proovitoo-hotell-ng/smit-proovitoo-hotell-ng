namespace Common.Enums;

public enum OrderStatus
{
    Confirmed = 0,
    Cancelled = 1,
}