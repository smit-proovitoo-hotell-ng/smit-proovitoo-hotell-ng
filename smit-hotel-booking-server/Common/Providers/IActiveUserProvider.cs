using Common.Dtos.User;

namespace Common.Providers;

public interface IActiveUserProvider
{
    /// <summary>
    /// Currently active user (is logged in).
    /// </summary>
    UserDto? ActiveUser { get; }
}
