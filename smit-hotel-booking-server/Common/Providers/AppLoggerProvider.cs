using System.Diagnostics;
using Common.Loggers;
using NLog;

namespace Common.Providers;

public static class AppLoggerProvider
{
    public static IAppLogger GetCurrentClassLogger()
    {
        return LogManager.LogFactory.GetLogger<AppLogger>(GetCallingClassName());
    }

    private static string? GetCallingClassName()
    {
        var skippedFramesCount = 2;
        var stackFrame = new StackFrame(skippedFramesCount);

        return stackFrame.GetMethod()?.DeclaringType?.FullName;
    }
}