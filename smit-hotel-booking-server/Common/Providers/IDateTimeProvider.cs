namespace Common.Providers;

/// <summary>
/// Provider for current date and time in a testable way.
/// </summary>
public interface IDateTimeProvider
{
    DateTime Now { get; }
    DateTime UtcNow { get; }
}