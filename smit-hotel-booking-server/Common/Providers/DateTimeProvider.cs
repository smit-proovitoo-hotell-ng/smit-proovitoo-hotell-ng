namespace Common.Providers;

public class DateTimeProvider : IDateTimeProvider
{
    /// <summary>
    /// Internally uses NET's <see cref="DateTime.Now"/>.
    /// </summary>
    public DateTime Now => DateTime.Now;
    /// <summary>
    /// Internally uses NET's <see cref="DateTime.UtcNow"/>.
    /// </summary>
    public DateTime UtcNow => DateTime.UtcNow;
}