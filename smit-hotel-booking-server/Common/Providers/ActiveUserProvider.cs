using System.Security.Claims;
using Common.Dtos.User;
using Microsoft.AspNetCore.Http;

namespace Common.Providers;

public class ActiveUserProvider : IActiveUserProvider
{
    private readonly HttpContext _httpContext;

    public ActiveUserProvider(IHttpContextAccessor httpContextAceAccessor)
    {
        _httpContext = httpContextAceAccessor.HttpContext;
    }

    public UserDto? ActiveUser => GetLoggedInUser();

    private UserDto? GetLoggedInUser()
    {
        var httpUser = _httpContext.User;

        if (httpUser.Identity is { IsAuthenticated: false })
            return null;

        var userId = httpUser.Claims.Single(c => c.Type == ClaimTypes.Sid).Value;
        var email = httpUser.Claims.Single(c => c.Type == ClaimTypes.Name)?.Value;
        var roleSystemNames = httpUser.FindAll(ClaimTypes.Role).Select(c => c.Value).ToList();

        return new UserDto
        {
            UserId = int.Parse(userId),
            Email = email,
            Roles = roleSystemNames.Select(rn => new RoleDto { SystemName = rn }).ToList()
        };
    }
}