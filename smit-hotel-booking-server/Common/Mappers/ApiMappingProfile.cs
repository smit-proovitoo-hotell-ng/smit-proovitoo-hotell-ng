using AutoMapper;
using Common.Dtos.Api.Authentication;
using Common.Dtos.Api.Booking;
using Common.Dtos.Api.Room;
using Common.Dtos.Api.User;
using Common.Dtos.Authentication;
using Common.Dtos.Booking;
using Common.Dtos.Room;
using Common.Dtos.User;

namespace Common.Mappers;

public class ApiMappingProfile : Profile
{
    public ApiMappingProfile()
    {
        CreateMap<UserDto, UserApiDto>()
            .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
            .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Email))
            .ForMember(dest => dest.Roles, opt => opt.MapFrom(src => src.Roles.SelectMany(r => r.SystemName)))
            .ForAllOtherMembers(opt => opt.Ignore());
        CreateMap<RoomDto, RoomApiDto>()
            .ForMember(dest => dest.Bookings, opt => opt.MapFrom(src => src.Bookings));
        CreateMap<BookingDto, BookingApiDto>()
            .ForMember(dest => dest.BookingCode, opt => opt.MapFrom(src => src.Number))
            .ForMember(dest => dest.BookingRoom, opt => opt.MapFrom(src => src.BookingRooms.FirstOrDefault()));
        CreateMap<BookingFlatDto, BookingApiDto>()
            .ForMember(dest => dest.CheckInStartUtc, opt => opt.MapFrom(src => src.CheckInUtc));

        CreateMap<LoginApiDto, LoginDto>()
            .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.Username))
            .ForMember(dest => dest.Password, opt => opt.MapFrom(src => src.Password))
            .ForAllOtherMembers(opt => opt.Ignore());

        CreateMap<BookingCreateApiDto, BookingFlatDto>()
            .ForMember(dest => dest.CheckInUtc, opt => opt.MapFrom(src => src.CheckInUtc));
        CreateMap<BookingUpdateApiDto, BookingFlatDto>()
            .ForMember(dest => dest.CheckInUtc, opt => opt.MapFrom(src => src.CheckInUtc));

        CreateMap<BookingQueryApiDto, BookingQueryDto>();
    }
}