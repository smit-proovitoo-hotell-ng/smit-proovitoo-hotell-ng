using NLog;

namespace Common.Loggers;

public interface IAppLogger
{
    /// <summary>
    /// Writes the diagnostic message at the <c>Debug</c> level.
    /// </summary>
    /// <param name="message">Log message.</param>
    void Debug(string message);

    /// <summary>
    /// Writes the diagnostic message at the <c>Debug</c> level.
    /// </summary>
    /// <param name="message">Log message.</param>
    /// <param name="objectContext">Optional object context. The object will be serialized as JSON and appended to the message.</param>
    void Debug<T>(string message, T objectContext) where T : class;

    /// <summary>
    /// Writes the diagnostic message at the <c>Info</c> level.
    /// </summary>
    /// <param name="message">Log message.</param>
    void Info(string message);

    /// <summary>
    /// Writes the diagnostic message at the <c>Info</c> level.
    /// </summary>
    /// <param name="message">Log message.</param>
    /// <param name="objectContext">Optional object context. The object will be serialized as JSON and appended to the message.</param>
    void Info<T>(string message, T objectContext) where T : class;

    /// <summary>
    /// Writes the diagnostic message at the <c>Error</c> level.
    /// </summary>
    /// <param name="message">Log message.</param>
    /// <param name="exception">The logged exception.</param>
    void Error(Exception exception, string message);

    /// <summary>
    /// Writes the diagnostic message at the <c>Error</c> level.
    /// </summary>
    /// <param name="message">Log message.</param>
    /// <param name="exception">The logged exception.</param>
    /// <param name="objectContext">Optional object context. The object will be serialized as JSON and appended to the message.</param>
    void Error<T>(Exception exception, string message, T objectContext) where T : class;

    /// <summary>
    /// Writes the diagnostic message at the specified level.
    /// </summary>
    /// <param name="logLevel">Log level.</param>
    /// <param name="message">Log message.</param>
    void Log(LogLevel logLevel, string message);
}