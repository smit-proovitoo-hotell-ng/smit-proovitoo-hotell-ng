using System.Globalization;
using Common.Constants;
using Common.Providers;
using Newtonsoft.Json;
using NLog;

namespace Common.Loggers;

public class AppLogger : Logger, IAppLogger
{
    public new void Debug(string message)
    {
        var logEventInfo = new LogEventInfo(LogLevel.Debug, Name, message);

        BaseLog(logEventInfo);
    }

    public new void Debug<T>(string message, T objectContext) where T : class
    {
        var logEventInfo = new LogEventInfo(LogLevel.Debug, Name, message);

        BaseObjectContextLog(logEventInfo, objectContext);
    }

    public new void Info(string message)
    {
        var logEventInfo = new LogEventInfo(LogLevel.Info, Name, message);

        BaseLog(logEventInfo);
    }

    public new void Info<T>(string message, T objectContext) where T : class
    {
        var logEventInfo = new LogEventInfo(LogLevel.Info, Name, message);

        BaseObjectContextLog(logEventInfo, objectContext);
    }

    public new void Error(Exception exception, string message)
    {
        var logEventInfo = new LogEventInfo(LogLevel.Error, Name, CultureInfo.InvariantCulture, message, null,
            exception);

        BaseLog(logEventInfo);
    }

    public void Error<T>(Exception exception, string message, T objectContext) where T : class
    {
        var logEventInfo = new LogEventInfo(LogLevel.Error, Name, CultureInfo.InvariantCulture, message, null,
            exception);

        BaseObjectContextLog(logEventInfo, objectContext);
    }

    public new void Log(LogLevel logLevel, string message)
    {
        var logEventInfo = new LogEventInfo(logLevel, Name, message);

        BaseLog(logEventInfo);
    }

    private void BaseObjectContextLog<T>(LogEventInfo logEventInfo, T? objectContext)
        where T : class
    {
        if (objectContext != null)
        {
            var objectJson = JsonConvert.SerializeObject(objectContext,
                new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

            logEventInfo.Properties[NLogConstants.VariableNames.OBJECT_CONTEXT_JSON] = objectJson;
        }

        BaseLog(logEventInfo);
    }

    private void BaseLog(LogEventInfo logEventInfo)
    {
        Log(typeof(AppLogger), logEventInfo);
    }
}