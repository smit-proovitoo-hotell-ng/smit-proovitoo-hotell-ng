namespace Common.Dtos.Booking;

public class NewBookingDto
{
    public string Name { get; set; } = default!;
    public string PersonalCode { get; set; } = default!;
    public string Email { get; set; } = default!;
    public int RoomId { get; set; }
    public DateTime From { get; set; }
    public DateTime To { get; set; }
}