using Common.Dtos.Room;

namespace Common.Dtos.Booking;

public class BookingDto
{
    public string Number { get; set; } = default!;
    public string FullName { get; set; } = default!;
    public string NationalIdentificationNumber { get; set; } = default!;
    public string Email { get; set; } = default!;
    public DateTime CheckInStartUtc { get; set; } = default!;
    public DateTime CheckOutUtc { get; set; } = default!;
    public List<RoomDto> BookingRooms { get; set; } = new();
}