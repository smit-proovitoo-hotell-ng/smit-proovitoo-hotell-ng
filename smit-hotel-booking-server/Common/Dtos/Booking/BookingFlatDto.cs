namespace Common.Dtos.Booking;
public class BookingFlatDto
{
    public string BookingCode { get; set; } = default!;
    public string FullName { get; set; } = default!;
    public string NationalIdentificationNumber { get; set; } = default!;
    public string Email { get; set; } = default!;
    public DateTime CheckInUtc { get; set; }
    public DateTime CheckOutUtc { get; set; }
    public int RoomId { get; set; }
}
