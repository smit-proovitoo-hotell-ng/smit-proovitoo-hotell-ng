namespace Common.Dtos.Booking;
public class BookingQueryDto
{
    public string? BookingCode { get; set; }
    public string? FullName { get; set; }
}
