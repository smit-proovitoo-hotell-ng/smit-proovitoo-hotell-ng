namespace Common.Dtos.Booking;

public class BookingInfoDto
{
    public string Number { get; set; } = default!;
    public string FullName { get; set; } = default!;
    public DateTime From { get; set; }
    public DateTime To { get; set; }
}