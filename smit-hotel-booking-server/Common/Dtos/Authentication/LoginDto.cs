using System.Text.Json.Serialization;

namespace Common.Dtos.Authentication;

public class LoginDto
{
    public string? Username { get; set; }

    [JsonIgnore]
    public string? Password { get; set; }
}