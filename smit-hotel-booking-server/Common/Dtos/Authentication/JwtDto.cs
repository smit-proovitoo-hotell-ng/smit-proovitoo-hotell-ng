using System.IdentityModel.Tokens.Jwt;

namespace Common.Dtos.Authentication;

public class JwtDto
{
    /// <summary>
    /// Token's audience.
    /// </summary>
    public string? Audience { get; set; }

    /// <summary>
    /// Token's issuer.
    /// </summary>
    public string? Issuer { get; set; }

    /// <summary>
    /// Token's signing secret key.
    /// </summary>
    public string? SecretKey { get; set; }
}