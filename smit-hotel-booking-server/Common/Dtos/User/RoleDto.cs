namespace Common.Dtos.User;
public class RoleDto
{
    public int RoleId { get; set; }
    public string? SystemName { get; set; }
}
