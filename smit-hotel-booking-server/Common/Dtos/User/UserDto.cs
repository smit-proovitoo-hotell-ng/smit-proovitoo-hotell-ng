namespace Common.Dtos.User;

public class UserDto
{
    public int UserId { get; set; }
    public string? Email { get; set; }
    public List<RoleDto>? Roles { get; set; } = new();
}