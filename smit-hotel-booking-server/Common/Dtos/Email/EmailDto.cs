namespace Common.Dtos.Email;

public class EmailDto
{
    public IEnumerable<string>? ToAddresses { get; set; }
    public string? Subject { get; set; }
    public string? Body { get; set; }
}
