namespace Common.Dtos.Room;

public class RoomTypeDto
{
    public int Id { get; set; }
    public string Name { get; set; } = default!;
    public int MaxCapacity { get; set; }
}