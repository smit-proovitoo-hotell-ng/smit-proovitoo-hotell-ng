namespace Common.Dtos.Result;

public class ResultDto
{
    private readonly List<string> _errorMessages;
    private readonly List<string> _successMessages;

    protected const string DEFAULT_SUCCESS_MESSAGE = "Toiming teostati edukalt.";
    protected const string DEFAULT_ERROR_MESSAGE = "Tekkis viga.";

    public bool IsSuccess => !ErrorMessages.Any();
    public IEnumerable<string> ErrorMessages => _errorMessages;
    public IEnumerable<string> SuccessMessages => _successMessages;

    public ResultDto()
    {
        _errorMessages = new List<string>();
        _successMessages = new List<string>();
    }

    public void AddError(string message)
    {
        _errorMessages.Add(message);
    }

    public void AddSuccess(string message)
    {
        _successMessages.Add(message);
    }

    public static ResultDto SuccessResult(string successMessage = DEFAULT_SUCCESS_MESSAGE)
    {
        var result = new ResultDto();
        result.AddSuccess(successMessage);
        return result;
    }

    public static ResultDto ErrorResult(string errorMessage = DEFAULT_ERROR_MESSAGE)
    {
        var result = new ResultDto();
        result.AddError(errorMessage);
        return result;
    }
}

public class ResultDto<TValue> : ResultDto
{
    public TValue? Value { get; set; }

    public ResultDto(TValue? value)
    {
        Value = value;
    }

    public static ResultDto<TValue?> SuccessResult(TValue? value,
        string successMessage = DEFAULT_SUCCESS_MESSAGE)
    {
        var result = new ResultDto<TValue?>(value);
        result.AddSuccess(successMessage);
        return result;
    }

    public static ResultDto<TValue?> ErrorResult(TValue? value, string errorMessage = DEFAULT_ERROR_MESSAGE)
    {
        var result = new ResultDto<TValue?>(value);
        result.AddError(errorMessage);
        return result;
    }
}