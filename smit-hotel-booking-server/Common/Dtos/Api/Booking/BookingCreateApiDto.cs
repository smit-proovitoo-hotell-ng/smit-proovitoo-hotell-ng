namespace Common.Dtos.Api.Booking;

public class BookingCreateApiDto
{
    public string FullName { get; set; } = default!;
    public string NationalIdentificationNumber { get; set; } = default!;
    public string Email { get; set; } = default!;
    public DateTime CheckInUtc { get; set; }
    public DateTime CheckOutUtc { get; set; }
    public int RoomId { get; set; }
}