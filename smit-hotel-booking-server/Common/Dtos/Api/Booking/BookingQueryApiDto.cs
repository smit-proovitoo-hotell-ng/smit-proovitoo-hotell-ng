namespace Common.Dtos.Api.Booking;

public class BookingQueryApiDto
{
    public string? BookingCode { get; set; }
    public string? FullName { get; set; }
}