using Common.Dtos.Api.Room;
using Common.Dtos.Room;

namespace Common.Dtos.Api.Booking;

public class BookingApiDto
{
    public string BookingCode { get; set; } = default!;
    public string FullName { get; set; } = default!;
    public string NationalIdentificationNumber { get; set; } = default!;
    public string Email { get; set; } = default!;
    public DateTime CheckInStartUtc { get; set; } = default!;
    public DateTime CheckOutUtc { get; set; } = default!;
    public RoomApiDto BookingRoom { get; set; } = new();
}