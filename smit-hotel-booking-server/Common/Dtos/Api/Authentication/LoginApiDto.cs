namespace Common.Dtos.Api.Authentication;

public class LoginApiDto
{
    public string Username { get; set; } = default!;
    public string Password { get; set; } = default!;
}