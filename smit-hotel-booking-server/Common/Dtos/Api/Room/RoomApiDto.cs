using Common.Dtos.Api.Booking;

namespace Common.Dtos.Api.Room;

public class RoomApiDto
{
    public int Id { get; set; }
    public string RoomNumber { get; set; } = default!;
    public decimal Price { get; set; }
    public int MaxOccupants { get; set; }
    public int TypeId { get; set; }
    public string Type { get; set; } = default!;
    public List<BookingApiDto> Bookings { get; set; } = default!;
    public bool? IsAvailable { get; set; }
}