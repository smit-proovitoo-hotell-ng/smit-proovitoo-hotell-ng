namespace Common.Dtos.Api.User;

public class UserApiDto
{
    public int UserId { get; set; }
    public string? Username { get; set; }
    public List<string>? Roles { get; set; }
}
