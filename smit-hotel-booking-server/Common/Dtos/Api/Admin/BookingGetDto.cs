using System.ComponentModel.DataAnnotations;

namespace Common.Dtos.Api.Admin;
public class BookingGetDto
{
    [Required]
    public string Code { get; set; } = default!;
}
