using Common.Dtos.Result;
using FluentValidation.Results;

namespace Common.Dtos.Api.Error;

public class ErrorApiDto
{
    public List<string>? ErrorMessages { get; set; }
    public Dictionary<string, List<string>>? FieldErrorMessages { get; set; }

    public ErrorApiDto()
        : this(new List<string>()) { }

    public ErrorApiDto(List<string>? errorMessages)
        : this(errorMessages, new Dictionary<string, List<string>> ()) { }

    public ErrorApiDto(List<string>? errorMessages,
        Dictionary<string, List<string>>? fieldErrorMessages)
    {
        ErrorMessages = errorMessages ?? new List<string>();
        FieldErrorMessages = fieldErrorMessages ?? new Dictionary<string, List<string>> ();
    }

    public ErrorApiDto(ResultDto result)
    {
        if (result.IsSuccess)
        {
            ErrorMessages = new List<string>();
            FieldErrorMessages = new Dictionary<string, List<string>>();
        }
        else
        {
            ErrorMessages = result.ErrorMessages.ToList();
        }
    }

    public ErrorApiDto(ValidationResult validationResult)
    {
        if (validationResult.IsValid)
        {
            ErrorMessages = new List<string>();
            FieldErrorMessages = new Dictionary<string, List<string>>();
        }
        else
        {
            ErrorMessages = validationResult.Errors
                .Where(error => string.IsNullOrEmpty(error.PropertyName))
                .Select(error => error.ErrorMessage)
                .ToList();
            FieldErrorMessages = validationResult.Errors
                .Where(error => !string.IsNullOrEmpty(error.PropertyName))
                .GroupBy(error => error.PropertyName)
                .ToDictionary(group => group.Key, group => group.Select(error => error.ErrorMessage).ToList());
        }
    }

    public static ErrorApiDto New(string errorMessage)
    {
        return new ErrorApiDto(new List<string> { errorMessage });
    }

    public static ErrorApiDto New(List<string>? errorMessages)
    {
        return new ErrorApiDto(errorMessages);
    }

    public static ErrorApiDto New(Dictionary<string, List<string>>? fieldErrorMessages)
    {
        return new ErrorApiDto(null, fieldErrorMessages);
    }

    public static ErrorApiDto New(List<string>? errorMessages,
        Dictionary<string, List<string>>? fieldErrorMessages)
    {
        return new ErrorApiDto(errorMessages, fieldErrorMessages);
    }
}