using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Extensions;
public static class DateTimeExtensions
{
    public static DateTime EndOfDay(this DateTime dateTime) => dateTime.Date.AddHours(23).AddMinutes(59).AddSeconds(59);
}
