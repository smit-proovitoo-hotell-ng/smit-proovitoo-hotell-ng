namespace Common.Constants;

public static class NLogConstants
{
    public static class VariableNames
    {
        public const string USER_ID = "UserId";
        public const string CONNECTION_STRING = "ConnectionString";
        public const string OBJECT_CONTEXT_JSON = "ObjectContextJson";
    }
}