namespace Common.Constants;
public static class BookingConstants
{
    /// <summary>
    /// Default check-in hour for a booking in 24h format
    /// </summary>
    public const int CHECK_IN_HOUR = 13;
    /// <summary>
    /// Default check-out hour for a booking in 24h format
    /// </summary>
    public const int CHECK_OUT_HOUR = 11;
}
