namespace Common.Constants;

/// <summary>
/// Configuration keys.
/// </summary>
public static class CK
{
    public const string JWT_ISSUER = "JWT_ISSUER";
    public const string JWT_AUDIENCE = "JWT_AUDIENCE";
    public const string JWT_KEY = "JWT_KEY";
    public const string JWT_TOKEN_LIFETIME = "JWT_TOKEN_LIFETIME";
    public const string CONNECTION_STRING_NAME = "DefaultConnection";
    public const string SMTP_SERVER_ADDRESS = "SmtpServerAddress";
    public const string SMTP_SERVER_PORT = "SmtpServerPort";
    public const string SMTP_DEFAULT_FROM_ADDRESS = "SmtpDefaultFromAddress";
    public const string SMTP_DEFAULT_FROM_ADDRESS_NAME = "SmtpDefaultFromAddressName";
    public const string SMTP_USERNAME = "SmtpUsername";
    public const string SMTP_PASSWORD = "SmtpPassword";
}