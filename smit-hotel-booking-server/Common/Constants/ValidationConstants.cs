using System.Text.RegularExpressions;

namespace Common.Constants;

public class ValidationConstants
{
    /// <summary>
    /// Default maximum length for a string.
    /// </summary>
    public const int DEFAULT_STRING_MAX_LENGTH = 30;

    /// <summary>
    /// Email validation regex.
    /// </summary>
    /// <remarks>
    /// Regex taken from https://html.spec.whatwg.org/multipage/input.html#valid-e-mail-address.
    /// </remarks>
    public static Regex EmailValidationRegex = new(@"^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", RegexOptions.Compiled);

    /// <summary>
    /// Minimum number of days before an order cannot be cancelled.
    /// </summary>
    public const int MIN_ORDER_CANCELLATION_DAYS = 3;
}