USE [master]
GO

-- Create the login
CREATE LOGIN [$(DB_USER)] WITH PASSWORD='$(DB_PASSWORD)';
GO

-- Create a user mapped to the login
USE [YourDatabaseName]
GO
CREATE USER [$(DB_USER)] FOR LOGIN [$(DB_USER)];
GO

-- Set appropriate permissions for the user
ALTER ROLE [db_owner] ADD MEMBER [$(DB_USER)];
GO
