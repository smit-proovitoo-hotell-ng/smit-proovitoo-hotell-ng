# SMIT proovitoo: hotelli broneerimissüsteem

## Sisukord
- [Sissejuhatus](#sissejuhatus)
- [Eeldused](#eeldused)
- [Käivitamine](#käivitamine)
- [Testid](#testid)
- [Rakendused](#rakendused)
  - [Klientrakendus](#klientrakendus)
  - [Rakendusserver](#rakekendusserver)
  - [Andmebaas](#andmebaas)


Käesoleva projekt on loodud SMIT-i hanke proovitööna. Proovitöö loojaks on Net Group OÜ.

Proovitöö raames on loodud veebiteenus, mille peamisteks komponentideks on veebirakendus, rakendusserver ning relatsiooniline andmebaas. Veebiteenuse eesmärk on võimaldada luua ning hallata hotelli broneeringuid.

Mugavamaks testimiseks on demorakendus paigaldatud ka Azure'i. Demorakendus asub aadressil: https://smit-hotel-booking-web-client.azurewebsites.net


## Eeldused
Järgnevalt on toodud välja tehnoloogilised eeldused, mis on vajalikud lokaalses masinas rakenduse käivitamiseks ja/või arendamiseks:
- [Docker](https://docs.docker.com/get-docker) (käivitamiseks, arendamiseks)
- [Docker Compose](https://docs.docker.com/compose/install) (käivitamiseks, arendamiseks)
- [.NET 6.0](https://dotnet.microsoft.com/en-us/download) (arendamiseks)
- [Entity Framework tools](https://docs.microsoft.com/en-us/ef/core/cli/dotnet) (arendamiseks)
- [Node.js 18](https://nodejs.org/en) (arendamiseks)


## Käivitamine
Ehita ning käivita rakendused kasutades Docker Compose-i.

1. Valideeri, et asud juurkaustas. Kausta nimi "smit-proovitoo-hotell" ja kaustas asub "docker-compose.yml".

2. Käivita Docker Compose
```shell
docker-compose up
```
Loodud rakendused on lokaalses masinas saadaval järgnevatel aadressidel:
- Klientrakendus: http://localhost:4200
- Rakendusserver: http://localhost:5196

NB! Tegemist on HTTP URL-idega. Juhul kui teie brauser suunab automaatselt HTTPS peale võib aidata brauseri privaatses režiimis avamine (Incognito või private browsing).

Konteinerite peatamiseks ning kustutamiseks kasuta käsku:
```shell
docker-compose down
```

## Testid
Ühiktestid asuvad rakendusserveri projekti kataloogis "smit-hotel-booking-server/UnitTests". Ühiktestide käivitamine eeldab .NET 6 SDK olemasolu.

Testide käivitamiseks:
1. Ava käsurida ning liigu "UnitTests" kataloogile:
```shell
cd smit-hotel-booking-server/UnitTests
```
2. Käivita testid järgneva käsuga:
```shell
dotnet test
```

## Rakendused
Lahendus koosneb kolmest peamisest komponendist:
1. Klientrakendus
2. Rakendusserver
3. Andmebaas

Järgnevalt tutvustatakse iga rakenduse komponenti eraldi ning kirjeldatakse arenduskeskkonna ülesseadmist.

### Klientrakendus
Klientrakenduse loomiseks on kasutatud TypeScript-il põhinevat Angular raamistikku.
Stiilide jaoks on kasutatud Sass CSS-i laiendust ja Angular Material komponente.

Klientrakendusega võimaldab:
- Otsida vabasid hotelli tubasid kuupäeva alusel.
- Hotelli toa broneerimist.
- Vaadata aktiivse broneeringu andmeid.
- Broneeringuid muuta ja tühistada.

Demo haldusliidese rakendus asub aadressil:
https://smit-hotel-booking-web-client.azurewebsites.net

Demo hotelli töötaja vaatest asub aadressil:
https://smit-hotel-booking-web-client.azurewebsites.net/admin

Selleks, et käivitada arenduskeskkonnas klientrakenduse server:
1. Installeeri Angulari käsurea tööriist. Seda saab teha NPM paketihalduriga:
```shell
npm install -g @angular/cli
```
1. Liigu klientrakenduse kataloogi:
```shell
cd smit-hotel-booking-web-client
```
1. Installeeri npm paketid:
```shell
npm install
```
1. Käivita klientrakenduse server:
```shell
ng server
```
Arenduskeskkonnas käivitatud klientrakendus asub lokaalses masinas aadressil: http://localhost:4200

### Rakendusserver
Rakendusserver põhineb .NET 6.0 raamistikul. Rakendusserveri eesmärk on realiseerida andmete talletamine andmebaasis, äriloogika realiseerimine ning võimaldada andmete manipuleerimist klientrakendusel.

Rakendusserver koosneb kolmest peamisest kihist:
- Esitluskiht. Koondatud projekti "WebApi" alla.
- Äriloogika. Koondatud projekti "BLL" alla.
- Common. Koondatud projekti "Common" alla.

Selleks, et käivitada arenduskeskkonnas klientrakenduse server:
1. Liigu rakendusserveri WebApi kataloogi:
```shell
cd smit-hotel-booking-server/WebApi
```
1. Käivita server:
```shell
dotnet run
```
Arenduskeskkonnas käivitatud rakendusserver asub lokaalses masinas aadressil: https://localhost:7294

API dokumentatsioon on nähtav lokaalses masinas aadressil:
https://localhost:7294/swagger/index.html

### Andmebaas
Rakendus kasutab Microsoft SQL Server andmebaasi. Microsoft SQL Server on objekt-relatsiooniline andmebaasisüsteem (ORDBMS), mis põhineb SQL keelel. Microsoft SQL Server on tuntud oma jõudluse, skaleeritavuse, töökindluse ja robustsuse poolest. 

Demo andmebaas:
- Serveri aadress: smit-hotel-booking.database.windows.net
- Kasutajanimi: smit-hotel-booking-admin
- Parool: fA6cBmYex@Jv!BU5f5APfrdSCNQ5waKg
